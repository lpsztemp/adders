. setdef.sh
$PY $SCRIPT_PATH/collect_performance_data.py -N $E1_N -s $E1_START -e $E1_END --gmp -c $RESULTS_PATH/cycles-$(size_string $E1_START)-$(size_string $E1_END).csv -t $RESULTS_PATH/ms-$(size_string $E1_START)-$(size_string $E1_END).csv
$PY $SCRIPT_PATH/collect_performance_data_hi_res.py -N $REL_SMALL_N -s $REL_SMALL_START -e $REL_SMALL_END -r $REL_SMALL_RES --smp=-1 -c $RESULTS_PATH/cycles-$(size_string $REL_SMALL_START)-$(size_string $REL_SMALL_END).csv -t $RESULTS_PATH/ms-$(size_string $REL_SMALL_START)-$(size_string $REL_SMALL_END).csv
$PY $SCRIPT_PATH/collect_performance_data_hi_res.py -N $REL_BIG_N -s $REL_BIG_START -e $REL_BIG_END -r $REL_BIG_RES --smp=-1 -c $RESULTS_PATH/cycles-$(size_string $REL_BIG_START)-$(size_string $REL_BIG_END).csv -t $RESULTS_PATH/ms-$(size_string $REL_BIG_START)-$(size_string $REL_BIG_END).csv
$PY $SCRIPT_PATH/collect_scalability_data.py -N $SCAL_SMALL_N -s $SCAL_SMALL_SIZE -i SMP AVX512 -o $RESULTS_PATH/scalability_smp_avx512-$(size_string $SCAL_SMALL_SIZE).csv
$PY $SCRIPT_PATH/collect_scalability_data.py -N $SCAL_BIG_N -s $SCAL_BIG_SIZE -i SMP AVX512 -o $RESULTS_PATH/scalability_smp_avx512-$(size_string $SCAL_BIG_SIZE).csv
if [ $CUDA_DEVICE -ge 0 ]; then
	$PY $SCRIPT_PATH/collect_performance_data_hi_res.py -N $CUDA_N -s $CUDA_START -e $CUDA_END -r $CUDA_RES --cuda-device=$CUDA_DEVICE -c $RESULTS_PATH/cycles-cuda-$(size_string $CUDA_START)-$(size_string $CUDA_END).csv -t $RESULTS_PATH/ms-cuda-$(size_string $CUDA_START)-$(size_string $CUDA_END).csv
else
	echo Skipping CUDA experiments.
fi;
