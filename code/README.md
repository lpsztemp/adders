# Artifact Identification

The repository contains implementations of adders described in the article by Andrey Chusov (assoc. professor of the Far-Eastern Federal University, Russia):

 A. Chusov, "Outperforming Sequential Full-Word Long Addition With Parallelization and Vectorization," in IEEE Transactions on Parallel and Distributed Systems, vol. 33, no. 12, pp. 4974-4985, 1 Dec. 2022, doi: 10.1109/TPDS.2022.3211937.

as well as native (x86-64) programs, Python and shell scripts to automatically conduct all of the experiments described in the article to measure performance of the adders and represent it visually.

## Abstract

The article and the artifact present algorithms for parallel and vectorized full-word addition of big unsigned integers with carry propagation.
Because of the propagation, software parallelization and vectorization of non-polynomial addition of big integers have long been considered impractical due to data dependencies between digits of the operands.
The presented algorithms are based upon parallel and vectorized detection of carry origins within elements of vector operands, evaluation of bit masks corresponding to those elements and subsequent addition of the resulting integers.
The acquired bits are consequently used to adjust the sum using the same method, which, in essence, is a generalization of the Kogge-Stone method.

Essentially, the paper formalizes and experimentally verifies, using the code of this artifact, the proposed parallel and vectorized implementations of carry-lookahead adders applied with arbitrary granularity of data.
This approach is noticeably beneficial for manycore, CUDA and vectorized implementation using AVX-512 with masked instructions.

The experiments implemented with the code of this artifact show that the parallel and vectorized implementations of the proposed algorithms can be many times faster compared to a sequential ripple-carry adder or adders based on redundant number systems such as one used in the GNU Multiple Precision library.

The code in the artifact implements every experiment described in the paper.
One can reproduce the experiments following the instructions given in the "Reproducibility of Experiments" section below or, in Linux, simply by setting the ``FOR_PAPER`` environment variable to 1 (e.g. ``export FOR_PAPER=1``) and running the ``run.sh`` script, provided that the requirements described below are met.

By default, the results of the experiments, which include the plots published in the paper and the corresponding CSV tables, are placed into the ``results`` subdirectory of the repository root directory.

# Artifact Dependencies and Requirements

Availability of most software and hardware dependencies (CUDA, GMP, SSE4.2, AVX2 and AVX-512) is determined at runtime as well as compilation time.
If the requirements are not met, the respective experiments are skipped with warning messages.

Some results, particularly relative performance of vectorized adders with respect to the sequential ripple-carry adder (cf. fig. 7 in the article), are sensitive to CPU dynamic clock frequency scaling which should be disabled in BIOS prior to collecting the respective performance data in order to obtain accurate results.

## Hardware requirements

The adders of the Code Artifact are implemented for the x86-64 (AMD64) target architecture.

The programs automatically detect available vector extensions of the CPU at runtime, and if some are unsupported, the respective experiments are skipped.

In order to run experiments upon AVX512 adders, the CPU must implement AVX512F, AVX512DQ, AVX512BW and AVX512VL instructions (which is the case for most of AVX512 CPUs).

Presence of a CUDA card with a supported compute capability and valid [NVIDIA drivers] is also detected at runtime, such that the CUDA experiments are skipped, if the requirements are not met.
To run the CUDA experiments, the CUDA device must have a compute capability of at least 3.0.

## Requirements for Compilation

* To build the programs either GCC (``g++`` of version 10 or later) or Visual Studio 2019 (or later) are required.

* On Linux, the ``make`` utility and the GNU assembler are required (included in the GNU ``binutils`` package).

* _Optionally_, to build and run CUDA experiments, the supported CUDA compiler ``nvcc`` of version 10.1 (or later) is required.
	* On Linux the specific version of ``nvcc`` required for compilation depends on a Linux distribution and a version of ``g++`` compiler.
	Please refer to [NVIDIA CUDA System Requirements] for details.
	Please also refer to [NVIDIA drivers] for information on NVIDIA drivers compatibility.
	* On Windows, compilation with ``nvcc`` requires Visual Studio 2019 or Visual Studio 2022.
	The latter is supported by ``nvcc`` of version 11.6 or later.
	Note that the Visual Studio projects in this code artifact were created for Visual Studio 2019 and CUDA 11.4, therefore use of other versions will require retargeting of the projects (see examples in the "Artifact Installation and Deployment Process" section).


* _Optionally_, to run the experiment upon the GNU Multiple Precision Arithmetic Library (GMP), it must be installed such that the "gmp.h" file is available in a default system include directory.
On Linux, the library and its headers are provided via the ``libgmp-dev`` package.
Versions 6.2.0 and later of the library are supported by the code artifact.

## Requirements for Running the Experiments

### Dependencies of the Basic Programs

The experiments can run on both Linux and Windows.
There are no dependencies of basic programs which implement the experiments other than the standard C and C++ libraries, which normally are already installed on the machine used for compilation.
Otherwise, depending on the platform and the compiler used to build the programs, the corresponding standard C and C++ libraries are required.
On Linux, please confer [GCC ABI Policy and Guideliness].
On Windows, see [Visual Studio redistributables].

Additionally, to run the CUDA experiments, a corresponding NVIDIA driver must be installed as specified in [NVIDIA drivers].
The CUDA runtime library is linked statically to create the Artifact programs, therefore its separate installation is not required.
If the Artifact programs are built without CUDA support, or if CUDA driver and runtime library fail to load, the CUDA experiments are omitted at runtime.

Also, to run the GMP experiments, the GNU Multiple Precision Arithmetic Library (``libgmp10``) must be installed.
If the library fails to load, the GMP experiments are omitted at runtime.

### Dependencies of the Scripts

The code artifact also includes Python scripts to simplify data gathering and visualization.
To use them, the Python interpreter of at least version 3.8 is needed together with ``numpy``, ``matplotlib`` and ``pandas`` python libraries.

The artifact also provides shell scripts to simplify conduction of the experiments parameterizing them for CodeOcean Reproducible Run or to replicate the results published in the paper.
To use this script a shell script interpreter, such as ``bash`` or ``sh``, are used.
The scripts are implemented on top of the Python scripts.

# Artifact Installation and Deployment Process

If all of the requirements are met, then in order to automatically prepare and run the experiments, one can simply use the ``run.sh`` script (see the "Shell scripts" section below).

Otherwise, the following steps should be taken.
1. Build basic programs. By default, the compiled programs reside in the ``code/bin`` directory.  
Normally, compilation takes less than a minute to complete.
2. _Optionally_, the Python scripts (in ``code/scripts``), or the shell scripts (in ``code/``) can be used to automate data gathering and visualization.
See the "Reproducibility of Experiments" section below for details on parameterizing the experiments.

This should be enough to run the experiments in default configuration.
However, deployment can be customized before compilation as follows.

*In Visual Studio*, this is achieved by setting Property Pages and Build Customizations dialogs of projects or by parameterizing the ``MSBuild`` program.
For example, to build basic programs in a custom output directory ``..\custom-bin``, one can invoke ``MSBuild`` the following way:

	MSBuild adders.sln /t:Build /p:OutDir=..\custom-bin\;Configuration=Release;Platform=x64

To customize dependency on a CUDA compiler to build the ``performance`` program (i.e. the ``performance.vcxproj`` Visual Studio project) using the default output path but changing path to the CUDA compiler to ``C:\CUDA\v10.2\bin``, path to the CUDA library to ``C:\CUDA\v10.2\lib\x64`` and path to the CUDA include files to ``C:\CUDA\v10.2\include``, one can build the programs as follows:

	MSBuild performance.vcxproj /t:Build /p:Configuration=Release;Platform=x64;^
		CudaToolkitBinDir=C:\CUDA\v10.2\bin;^
		CudaToolkitIncludeDir=C:\CUDA\v10.2\include;^
		CudaToolkitLibDir=C:\CUDA\v10.2\lib\x64

*On Linux* the programs are built using ``make``.
By default, all of the basic programs are built.
However, a selective build is conducted by specifying a build target (see the "Basic programs" subsection of "Reproducibility of Experiments" below).

* ``make all`` (the default) - build every basic program.
* ``make performance`` - build the ``code/bin/performance`` program.
* ``make performance_hi_res`` - build the ``code/bin/performance_hi_res`` program.
* ``make scalability`` - build the ``code/bin/scalability`` program.
* ``make tests`` - build the ``code/bin/tests`` program.
* ``make clean`` - clean up: delete the `code/bin` directory as well as `code/obj` with intermediate files.

The build can be customized by setting the following environment variables prior to invoking ``make``.

* ``CXX`` - path to the C++ compiler. Defaults to ``g++``.
* ``AS`` - path to the GNU Assembler. Defaults to ``as``.
* ``NVCC`` - path to the CUDA compiler. Defaults to ``nvcc``.
* ``BIN`` - output path for the built programs to reside in. Defaults to ``bin`` subdirectory of the working directory.
* ``CXXFLAGS`` - parameters for the C++ compiler. Note that the Makefile appends target machine (``-m``) parameters to these in order to compile adders.
* ``NVCCFLAGS`` - parameters for the CUDA compiler. Note that ``--machine``, ``-cudart``, ``--generate-code`` and ``-ccbin`` are always set in the Makefile.

Note that the Python scripts expect a directory with the programs to be ``../bin`` with respect to the scripts location.
Likewise, the shell scripts of this code artifact expect the programs to be in ``./bin``, and the scripts to be in ``./scripts`` with respect to their location.

# Reproducibility of Experiments

After compilation of the code artifact, the following programs implement the experiments:
 * _basic programs_ in ``code/bin`` are basic implementations of the adders discussed in the article as well as measurements of their performance which are printed out to the standard output device (``stdout``) and, optionally, to CSV files;
 * _Python scripts_ in ``code/scripts`` defined on top of the basic programs to perform experiments multiple times, collect data in the form of CSV files and visualize it as plots as presented in the article;
 * _shell scripts_ in the ``code`` directory whose purpose is to automatically build the programs and parameterize the Python invocations in order to replicate the experiments described in the paper or to implement the Reproducible Run of CodeOcean with reduced time consumption.

*On Linux, the simplest way to reproduce the experiments published in the paper is to use the ``run.sh`` shell script as described in the "Shell scripts" section below.*

Particularly, if the requirements to the platform are met, the following command invoked from ``code`` subdirectory of the repository builds the programs and runs the experiments of the Code Artifact exactly as specified in the article:

	export FOR_PAPER=1 && ./run.sh

This will produce the plots presented in the article as well as the corresponding CSV tables in the ``../results`` directory.
Note that if not all of the requirements to the platform are met, or if the default parameters, such as a CUDA device, are not appropriate, some of the experiments may be skipped with a warning message.

The table below shows correspondence between the results of such invocation with plots (identified with figure references) in the article, provided that all of the requirements (see the "Artifact Dependencies and Requirements" section above) are met.

Figure  | Image file                                                                                           | Data table
--------|------------------------------------------------------------------------------------------------------|---------------------------------------------
fig. 5  | performance-adc-smp-smpavx512-8GiB.pdf                                                               | cycles-64B-8GiB.csv and ms-64B-8GiB.csv
fig. 6  | scalability_smp_avx512-8GiB.pdf and scalability_smp_avx512-16MiB.pdf                                 | scalability_smp_avx512-8GiB.csv and scalability_smp_avx512-16MiB.csv
fig. 7  | relative-performance-adc-avx512-128KiB-128MiB.pdf and relative-performance-adc-avx512-32KiB-8MiB.pdf | cycles-128KiB-128MiB.csv, ms-128KiB-128MiB.csv, cycles-32KiB-8MiB.csv and ms-32KiB-8MiB.csv
fig. 8  | performance-sse4.2nolut-sse4.2lut-adc-64B-8GiB.pdf                                                   | cycles-64B-8GiB.csv and ms-64B-8GiB.csv
fig. 9  | performance-cudakernel-cuda-adc-64B-4GiB.pdf and performance-cudaled-adc-64B-4GiB.pdf                | cycles-cuda-64B-4GiB.csv and ms-cuda-64B-4GiB.csv
fig. 10 | performance-adc-smp-avx2-sse4.2lut-gmp-64B-8GiB.pdf                                                  | cycles-64B-8GiB.csv and ms-64B-8GiB.csv

Also, note a possible compilation error in ``host_config.h`` which is a part of CUDA library distribution:

	error: #error -- unsupported GNU version!

It originates from the NVIDIA CUDA compiler ``nvcc`` and tells about a mismatch of the C++ compiler and the CUDA compiler.
To resolve it, use a compiler specified in the [NVIDIA CUDA System Requirements] (cf. the Requirements for Compilation above) as described in the "Shell scripts" section below.

## Basic programs

Basic programs are native (x86-64) implementation of adders.
They measure the performance of each adder once using low-level methods described in the paper.
Afterward they print the results as tables to ``stdout`` and, if defined by options, write them to CSV files.

By default, after compilation all of the basic programs reside in the ``code/bin`` directory with respect to the root directory of the repository.

The list of basic programs is comprised of the following.

### Measure performance with respect to data size

	performance [-h] [-s START_SIZE] [-e END_SIZE]
		[--smp T] [--cuda-device CUDA_DEVICE] [--gmp]
		[-c CYCLES_FILE] [-t TIMES_FILE]

Measures performance of long addition of different sizes implemented with every adder provided as a result of compilation and supported by the platform (see the "Artifact Dependencies and Requirements" above).
CUDA, SMP (CPU-parallel) and GMP-based adders are optional and are evaluated, if the program options allow that, as specified below.

The sizes, which are expressed as powers of two, are changed exponentially, i.e. by incrementing the exponent from a given starting value to the given
(inclusively) ending value.
To measure performance with higher custom resolution, use the ``performance_hi_res`` program.

The ``performance`` program was used via the Python scripts (see the section below) to obtain figures 5, 8 and 10 in the article.

Parameter                 | Description
--------------------------|-------------
``-h``, ``--help``        | Display help.
``-s``, ``--start-size``  | Starting size of addends. The value is expressed as base-2 logarithm of a number of 64-bit words. The experiments are invoked for sizes of operands changing exponentially as powers of 2 ranging from the value of ``-s`` to the value of ``-e`` (inclusive). The value must be at least 3 (the default).
``-e``, ``--end-size``    | Inclusive final size of addends. The value is expressed as a base-2 logarithm of a number of 64-bit words. The default value is 24.
``--smp``                 | A number of CPU threads to run the parallel adders. Negative values turn off SMP runs. Zero (the default) or values greater than the number of logical CPU cores are internally changed to the actual number of the cores.
``--cuda-device``         | Perform CUDA experiments using the specified CUDA device. The device is specified by an integral 0-based index passed to [cudaSetDevice]. If the parameter is omitted, experiments on the CUDA adders are skipped.
``--gmp``                 | Perform computations using the GNU MP library, if one is available.
``-c``, ``--cycles-file`` | A name of a file to accept a CSV table with performance of the adders expressed as a difference between values of the CPU cycles counter returned by the RDTSC instruction after and before each invocation of each adder. The file must not be the same as one of ``--times-file``, if the latter is specified. If the file exists, it will be silently overwritten. By default no such file is created.
``-t``, ``--times-file``  | A name of a file to accept a CSV table with performance of the adders expressed in milliseconds passed between time points before and after each invocation of each adder. The file must not be the same as one of ``--cycles-file``, if it is specified. If the file exists, it will be silently overwritten. By default no such file is created.

**Example**

	performance -s 3 -e 20 --smp 18 --cuda-device 0 --gmp -c cycles.csv -t times.csv

Measure performance of adding up random integer values of exponentially changed sizes starting from $2^3 \cdot 8 = 64 \text{ bytes}$ (``-s 3``) and ending after performance of $2^{20} \cdot 8 = 8\text{ MiB}$ addition (``-e 20``) has been measured.

Perform the measurements of performance of every available single-threaded CPU adder as well as manycore implementation using 18 threads (``--smp 18``), GPGPU CUDA adder on device #0 (``--cuda-device 0``) and The GNU Multiple Precision Arithmetic Library (``--gmp``).

In addition to printing a table with the results to ``stdout``,  also export the results in the CSV format: write the measured values of the CPU counter to ``cycles.csv`` (``-c cycles.csv``), write time values to ``times.csv`` (``-t times.csv``), such that both files will reside in the current directory.

### Measure performance depending on data size with increased resolution

	performance_hi_res [-h] [-s START_SIZE] [-e END_SIZE] [-r resolution]
		[--smp T] [--cuda-device CUDA_DEVICE] [--gmp]
		[-c CYCLES_FILE] [-t TIMES_FILE]

Measures performance of long addition of different sizes implemented with available adders.
Unlike the ``performance`` program, the sizes are changed parabolically, rather than exponentially, i.e. with a difference between adjacent sizes growing linearly with respect to a value of the desired resolution.

The program was used via the Python scripts (see the section below) to obtain figures 7 and 9 in the article.

Parameter                 | Description
--------------------------|-------------
``-h``, ``--help``        | Display help.
``-s``, ``--start-size``  | Starting size of addends. The value is expressed as base-2 logarithm of a number of 64-bit words. The experiments are performed for sizes of operands ranging from the value of ``-s`` to a maximum that is not greater than the value of ``-e``. The value of ``-s`` must not be less than 3 (the default) which corresponds to $2^3 \cdot 8 = 64\text{ bytes}$.
``-e``, ``--end-size``    | Inclusive upper bound of the set of sizes. The value is expressed as base-2 logarithm of a number of 64-bit words. The value can be internally increased in order to satisfy the requirement for resolution preserving 512 bit data alignment and the law of size growth. Also, the value can be decreased by the program to preserve 512 bit data alignment and the law of growth. The default value is 24.
``-r``, ``--resolution``  | A minimal number of measurements to produce. The default value is 256 measurements.
``--smp``                 | A number of CPU threads to run the parallel adders. Negative values turn off SMP runs. Zero (the default) or values greater than the number of logical CPU cores are internally changed to the actual number of the cores.
``--cuda-device``         | Perform CUDA experiments using the specified CUDA device. The device is specified by an integral 0-based index passed to [cudaSetDevice]. If the parameter is omitted, experiments on the CUDA adders are skipped.
``--gmp``                 | Perform computations using the GNU MP library, if one is available.
``-c``, ``--cycles-file`` | A name of a file to accept a CSV table with performance of the adders expressed as a difference between values of the CPU cycles counter returned by the RDTSC instruction after and before an invocation of an adder. The file must not be the same as one of ``--times-file``, if it is specified. If the file exists, it will be silently overwritten. By default no such file is created.
``-t``, ``--times-file``  | A name of a file to accept a CSV table with performance of the adders expressed in milliseconds passed between time points before and after each invocation of each adder. The file must not be the same as one of ``--cycles-file``, if it is specified. If the file exists, it will be silently overwritten. By default no such file is created.

**Example**

	performance_hi_res -s 3 -e 20 -r 50 --smp 18 --cuda-device 0 --gmp -c cycles.csv -t times.csv

Measure performance of adding up random integers of sizes parabolically changed from $2^3 \cdot 8 = 64\text{ bytes}$ (``-s 3``) until a value of around $2^{20} \cdot 8 = 8\text{ MiB}$ (``-e 20``) is reached, such that the requested resolution of at least 50 data points (``-r 50``) is met.

Perform the measurement of performance of every available single-threaded CPU adder as well as the manycore implementations using 18 threads (``--smp 18``), GPGPU CUDA adder on device #0 (``--cuda-device 0``) and the GNU Multiple Precision Arithmetic Library (``--gmp``).

Print a table with the results to ``stdout``.
Also, export the results in the CSV format: write measured values of the CPU counter to ``cycles.csv`` (``-c cycles.csv``), write time values to ``times.csv`` (``-t times.csv``) in the current directory.

### Measure scalability of the CPU-parallel adders

	scalability [-h] [-s SIZE] [-i IMPL]

Measures time performance, speedups and parallelization efficiency (speedup divided by the number of threads used) of parallel adders depending on how many logical CPU cores are employed.

The program was used via the ``collect_scalability_data.py`` Python script (see below) to obtain data for figure 6 in the article.

Parameter                 | Description
--------------------------|-------------
``-h``, ``--help``        | Display help.
``-s``, ``--size``        | Size of addends. The value is expressed as base-2 logarithm of a number of 64-bit words. The value must not be less than 3. The default value is 5 which yields size equal to $2^5 = 32\text{ words}$ or 256 bytes.
``-i``, ``--impl``        | Parallel adder implementation. Can be either ``SMP`` (the default), ``AVX512-W`` or ``AVX512-S`` (the latter has an alias ``AVX512``). A full list of adders along with their descriptions is presented below in the "Adders" section, in which the parallel adders are described under the respective names "SMP", "SMP-AVX512-W" and "SMP-AVX512-S".

**Example**

	scalability -s 25 -i SMP

Measure performance, speedups and efficiency of parallelization of adding up random integers of size $2^{25} \cdot 8 = 256\text{ MiB}$ (``-s 25``) using the SMP adder (-i SMP). Print a table with the results to ``stdout``.

### Tests

	tests [-h] [--cuda-device DEVICE_NO] [--gmp]

Test correctness of adders supported by the platform and returns a failure exit code, if the tests fail.

Parameter          | Description
-------------------|-------------
``-h``, ``--help`` | Display help.
``--cuda-device``  | Specifies an index of a CUDA device to run the tests on. The device is specified by an integral 0-based index passed to [cudaSetDevice]. If a negative value is set (the default), no CUDA tests are performed.
``--gmp``          | Test the GMP implementation.

**Example**

	test --cuda-device 0 --gmp && echo "Success" || echo "Failure"

Check whether the every adder, including the CUDA adder run on the device #0 and the GMP adder, successfully run and produce expected results, and, if they do, print out "Success", otherwise, print "Failure".

## Python scripts

Python scripts are defined on top of the basic programs to automate data gathering and visualization.
Every Python script resides in the ``code/scripts`` directory relatively to the root directory of the repository.
The basic programs need to be compiled prior to any Python script invocation.

There are two classes of Python scripts.
* The ``collect_*.py`` scripts perform data gathering by means of the basic programs invoking them multiple times, averaging the results and possibly writing the means to given CSV files.
* The ``visualize_*.py`` scripts visualize data as plots built from CSV tables obtained by the ``collect_*.py`` scripts or by the basic programs.

In the examples below it is assumed that the ``python`` program is an accessible Python interpreter of at least version 3.8 with the "Requirements for Running the Experiments" met.
Also, to invoke the examples, the current working directory should be  ``code/scripts``.

### Collect performance data based on size

	python collect_performance_data.py [-h] [-p] [-k]
		[-c CYCLES_FILE] [-t TIMES_FILE]
		[-N EXPERIMENT_COUNT] [-s START_SIZE] [-e END_SIZE]
		[--smp SMP] [--cuda-device CUDA_DEVICE] [--gmp]

Runs a given number of experiments invoking the compiled ``../bin/performance`` program. Afterward the script averages the results of the experiments and, if specified, stores their arithmetic means to files as CSV tables which can later be visualized using the ``visualize_performance_data.py`` or ``visualize_relative_performance_data.py`` scripts.

Parameter                      | Description
-------------------------------|-------------
``-h``, ``--help``             | Display help.
``-p``, ``--print-results``    | Print the averaged results to the terminal. To print intermediate results, also use the ``-k`` parameter.
``-c``, ``--cycles-file``      | A file to receive average time, in CPU cycles, taken by each of the adders. The cycles are obtained using the RDTSC instruction of the CPU.
``-t``, ``--times-file``       | A file to receive average time, in milliseconds, taken by each of the adders. In case of the CUDA adder, CUDA events are used to obtain time. Performance of other adders is measured using ``std::chrono::steady_clock`` of the C++ library.
``-N``, ``--experiment-count`` | A number of experiments, i.e. a number of times to run the executable, to obtain and average the results of. The default value is 1.
``-s``, ``--start-size``       |  A starting size of addends expressed as an integral base-2 logarithm of a number of 8-byte words: for example, the value of 20 corresponds to $2^{20} \cdot 8\text{ bytes}$, i.e. 8 MiB, of data per one addend. Each experiment measures performance of the adders for sizes whose logarithms range from the value of ``-s`` to the value of ``-e`` (inclusive). The default value is 3.
``-e``, ``--end-size``         | An inclusive upper bound of the set of sizes. The default value is 30.
``--smp``                      | A number of CPU threads to run the parallel adders. Negative values turn off the SMP experiments. Zero (the default) or values higher than the number of logical CPU cores are replaced with the actual number of the cores.
``--cuda-device``              | In addition to other adders, measure performance of the CUDA-based adder. Use a CUDA device, identified by the parameter. By default, the CUDA computation is omitted.
``--gmp``                      | In addition to other adders, measure performance of the adder which is based upon the GNU Multiple Precision Arithmetic Library (GMP). By default, the GMP computation is omitted.
``-k``, ``--keep-raw-results`` | Preserve intermediate results in files named "dataperf_\$DATE_\$TIME_\$EXP_cycles.csv" and "dataperf_\$DATE_\$TIME_\$EXP_ms.csv", where \$DATE and \$TIME are respectively the date and time of the python script invocation, and \$EXP is a counter value identifying the basic experiment. These are the files produced by the ``performance`` program. Additionally, if the ``-p`` parameter is set, print the intermediate results to the terminal.

**Example**

	python collect_performance_data.py -N 5 -s 3 -e 25 --smp 18 \
		--cuda-device 0 --gmp -k -c cycles.csv -t times.csv -p

Collect times of all supported CPU adders using a single thread, also using the supported parallel adders run on 18 CPU cores (``--smp 18``) as well as the CUDA GPU based adder run on device 0 (``--cuda-device 0``) and using the adder provided by The GNU Multiple Precision Arithmetic Library (``--gmp``) with its ``mpz_add`` function.

Times to collect are of adding up numbers of sizes ranging exponentially from $2^3 \cdot 8 = 64\text{ bytes}$ (``-s 3``) to $2^{25} \cdot 8 = 256\text{ MiB}$ (``-e 25``).

Run the experiment five times (``-N 5``) and, keeping the intermediate results (``-k``), collect performance measured in CPU cycles as well as in milliseconds taken by each of the adders and write the average of those to output CSV files named respectively ``cycles.csv`` (``-c cycles.csv``) and ``times.csv`` (``-t times.csv``). Also, print out the results to the terminal (``-p``).

### Collect performance data based on size with increased resolution

	python collect_performance_data_hi_res.py [-h] [-p] [-k]
		[-c CYCLES_FILE] [-t TIMES_FILE] [-N EXPERIMENT_COUNT]
		[-s START_SIZE] [-e END_SIZE] [-r RESOLUTION]
		[--smp SMP] [--cuda-device CUDA_DEVICE] [--gmp]

Runs a given number of experiments implemented in the ``../bin/performance_hi_res`` program measuring performance of adding up random numbers of sizes changed parabolically, i.e. a difference between adjacent sizes grows linearly, to achieve the requested resolution of the results.

Then the script computes pointwise average of the results and, if specified, stores the arithmetic means to files as CSV tables which can later be visualized using the ``visualize_performance_data.py`` or ``visualize_relative_performance_data.py`` scripts.

Parameter                      | Description
-------------------------------|-------------
``-h``, ``--help``             | Display help.
``-p``, ``--print-results``    | Print the averaged results to the terminal. To also obtain intermediate results, use the ``-k`` parameter.
``-c``, ``--cycles-file``      | A file to receive average CPU cycles taken by each of the adders. The cycles are obtained using the RDTSC instruction of the CPU.
``-t``, ``--times-file``       | A file to receive average time, in milliseconds, taken by each of the adders. In case of the CUDA adder, CUDA events are used to obtain time. Other adders are measured using ``std::chrono::steady_clock`` of the C++ library.
``-N``, ``--experiment-count`` | A number of experiments, i.e. a number of times to run the ``../bin/performance_hi_res`` executable, to obtain and average the results of. The default value is 1.
``-s``, ``--start-size``       | A starting size of addends expressed as an integral base-2 logarithm of a number of 8-byte words: for example the value of 20 corresponds to $2^{20} \cdot 8\text{ bytes}$, i.e. 8 MiB, of data per one addend. Each experiment measures performance of the adders for sizes ranging from the value of ``-s`` to the value of ``-e`` (inclusive). The default value is 3.
``-e``, ``--end-size``         | An inclusive ending bound of the set of sizes. An actual ending size can be changed to meet the requirements for resolution and 512 bit data alignment. The default value is 30.
``-r``, ``--resolution``       | A minimal number of measurements to make. The actual number of measurements can be increased by the program in order to achieve 64 byte data alignment. The default value is 50.
``--smp``                      | A number of CPU threads to run the parallel adders. Negative values turn off the SMP experiments. Zero (the default) or values higher than the number of logical CPU cores are replaced with the number of the cores.
``--cuda-device``              | Use a CUDA device, identified by the parameter, in addition to other implementations. By default, the CUDA computation is omitted.
``--gmp``                      | In addition to other adders, measure performance of the adder which is based upon the GNU Multiple Precision Arithmetic Library (GMP). By default, the GMP computation is omitted.
``-k``, ``--keep-raw-results`` | Preserve intermediate results in files named "dataperf_\$DATE_\$TIME_\$EXP_cycles.csv" and "dataperf_\$DATE_\$TIME_\$EXP_ms.csv", where \$DATE and \$TIME are respectively the date and time of the python script invocation, and \$EXP is a counter value identifying the basic experiment. These are the files produced by the ``../bin/performance_hi_res`` program. Additionally, if the ``-p`` parameter is specified, print the output of the program to the terminal.

**Example**

	python collect_performance_data_hi_res.py -N 2 -s 3 -e 25 -r 50 \
		--smp 18 --cuda-device 0 --gmp -k -c cycles.csv -t times.csv -p

Collect times of all supported CPU adders using a single thread, also using the supported parallel adders run on 18 logical CPU cores (``--smp 18``) as well as the CUDA GPU based adder run on device 0 (``--cuda-device 0``) and using the adder provided by The GNU Multiple Precision Arithmetic Library (``--gmp``) with its ``mpz_add`` function.

Times to collect are of adding up numbers of sizes ranging from $2^3 \cdot 8 = 64\text{ bytes}$ (``-s 3``) to about $2^{25} \cdot 8 = 256\text{ MiB}$ (``-e 25``) to meet the desired resolution of at least 50 (``-r 50``) data points.

Run the experiment two times (``-N 2``) and, keeping intermediate results (``-k``), collect performance measured in CPU cycles as well as milliseconds taken by each of the adders and write the average of those to output CSV files named respectively ``cycles.csv`` (``-c cycles.csv``) and ``times.csv`` (``-t times.csv``). Also, print out the intermediate results to the terminal (``-p``).

### Collect scalability data of the CPU-parallel adders

	python collect_scalability_data.py [-h] [-o OUTPUT]
		[-N EXPERIMENT_COUNT] [-s SIZE] [-i PARALLEL_ADDERS] [-k]

Measures scalability of chosen implementations of parallel addition implemented in the ``../bin/scalability`` program. The measurements are performed a number of times specified by the value of ``-N`` and are averaged by the script. The results are written to an output CSV file and can be later visualized using the ``visualize_scalability_data.py`` script.

Parameter                      | Description
-------------------------------|-------------
``-h``, ``--help``             | Display help.
``-o``, ``--output``           | A file which receives the results in the CSV format.
``-N``, ``--experiment-count`` | A number of experiments, i.e. a number of times to run the executable to obtain and average the outputs. The default value is 1.
``-s``, ``--size``             |  Size of each addend expressed as a base-2 logarithm of 8-byte word count. The default value is 27, which corresponds to $2^{27} \cdot 8\text{ bytes}$ or 1 GiB of data.
``-i``, ``--implementation``   |  A space-separated list of parallel adders to measure scalability of. Choices are: SMP, AVX512 (equivalently, AVX512-S) and AVX512-W. The details on each are described in the "Adders" section, in which the adders are respectively referred to as "SMP", "SMP-AVX512-S" and "SMP-AVX512-W".
``-k``, ``--keep-raw-results`` | Preserve intermediate results in the file "scalability_\$DATE_\$TIME_\$ADDER_\$EXP.txt", where \$DATE and \$TIME are respectively the date and time of the python script invocation, \$ADDER sequentially accepts values of the adder list specified by ``-i``, and \$EXP is an ordinal identifying the experiment on a given adder. These are the files generated by the ``../bin/scalability`` program.

**Example**

	python collect_scalability_data.py -N 2 -s 24 -i SMP AVX512 -k \
		-o example.csv

Collect times, derive speedups and efficiency of parallelization of SMP and AVX512 adders (``-i SMP AVX512``) used to add up $2^{24} \cdot 8 = 128\text{ MiB}$ addends (``-s 24``).

Perform these actions two times (``-N 2``) and, keeping the intermediate results (``-k``), write their average as CSV to the ``example.csv`` file (``-o example.csv``).

### Visualize performance with respect to data size

	python visualize_performance_data.py [-h]
		[-c CYCLES_INPUT] [-t TIMES_INPUT] [-S] [-o OUTPUT]
		[--no-show-legend] [--no-output-legend]
		[adders ...]

Visualizes performance of adders based on files generated by the ``collect_performance_data.py``, ``collect_performance_data_hi_res.py`` Python scripts or by the basic programs ``../bin/performance`` and ``../bin/performance_hi_res``.

The adders to visualize performance of are identified with names, full list of which is defined in the "Adders" section below.
These names match the column names within a CSV file produced by the collecting scripts or the basic programs.
Presence of columns in the CSV file depends on which experiments actually took place with regard to the respective script or program parameters and the platform requirements met.

Parameter                  | Description
---------------------------|-------------
``-h``, ``--help``         | Display help.
``-c``, ``--cycles-input`` | Input CSV file with measurements of performance based on values of CPU clock counter returned by the RDTSC instruction.
``-t``, ``--times-input``  | Input CSV file with measurements of performance in milliseconds, based on system clock values. The parameter may be specified in addition to ``-c`` in which case both results are visualized on the same plot.
``-S``, ``--show-result``  | Display the plot of performance in a dialog window.
``-o``, ``--output``       | Create an image file with the plot.
``--no-show-legend``       | Hide legend from the displayed plot.
``--no-output-legend``     | Hide legend from the plot written to the output file.
``adders``                 | Space-separated list of adder names, i.e. names of columns in the input file, to visualize. The full list of adder names is available below in the "Adders" section.

**Example**

	python visualize_performance_data.py -c cycles.csv -t times.csv \
		-S -o performance.png --no-output-legend CUDA SMP ADC

Build a plot of times taken by CUDA, SMP and ADC adders to operate upon integers of different sizes.
The times are based on CPU clock counter values stored in a CSV file named ``cycles.csv`` (``-c cycles.csv``) as well as respective times in milliseconds in the file named ``times.csv`` (``-t times.csv``).
Show the plot in a dialog window (``-S``) and also save it to ``performance.png`` file (``-o performance.png``) such that the image file will contain no legend (``--no-output-legend``).

### Visualize relative performance of two adders depending on data size

	python visualize_relative_performance_data.py [-h] -i INPUT
		[-S] [-o OUTPUT] [--show-legend] [--output-legend]
		adder1 adder2

Visualizes relative performance of two adders evaluated based on measurements collected with ``collect_performance_data.py`` or ``collect_performance_data_hi_res.py`` scripts or the corresponding basic programs.
The script is invoked to evaluate and visualize change, in percents, of performance of the second adder, ``adder2``, compared to performance of the first adder, ``adder1``, using the formula $(1 - t_2/t_1) \cdot 100$, where $t_1$ is time taken by the first adder, and $t_2$ is time taken by the second adder to add up random integers.
The times are read from an input CSV file.

The adders to visualize are identified with names, full list of which is defined in the "Adders" section below.
These names match the column names within the input CSV file.
Presence of columns in the file depends on which experiments actually took place with regard to the respective script or program parameters and the platform requirements met.

Parameter                 | Description
--------------------------|-------------
``-h``, ``--help``        | Display help.
``-i``, ``--input``       | Input CSV file with timings of the two adders.
``-S``, ``--show-result`` | Display the plot of performance in a dialog window.
``-o``, ``--output``      | Create an image file with the plot.
``--show-legend``         | Show legend on the displayed plot.
``--output-legend``       | Show legend on the plot written to the output file.
``adder1``                | The name of the first adder, i.e. name of the respective column of the input CSV file, to check performance against. See the "Adders" section below.
``adder2``                | The name of the second adder and the name of the CSV column with times to check against ones of the first adder. See the "Adders" section below.

**Example**

	python visualize_relative_performance_data.py
		-i example.csv -S -o example.png CUDA ADC

Build a plot of relative performance of the ADC adder compared to the CUDA adder based on timings in the ``example.csv`` file (``-i example.csv``), show the plot in a dialog window (``-S``) and also save it to the ``example.png`` file (``-o example.png``).

### Visualize scalability of CPU-parallel adders

	visualize_scalability_data.py [-h] -i INPUT [-S] [-o OUTPUT]
		[--no-output-legend] [--no-show-legend]
		[-m MEASURE] [adders ...]

Visualize a specified measure of scalability of parallel adders based on performance values produced with ``collect_scalability_data.py``.

Parameter                 | Description
--------------------------|-------------
``-h``, ``--help``        | Display help.
``-i``, ``--input``       | An input CSV file with measurements of scalability, i.e. the output of ``collect_scalability_data.py``.
``-S``, ``--show-result`` | Display the plot of scalability in a dialog window.
``-o``, ``--output``      | Create an image file with the plot.
``--no-show-legend``      | Hide legend of the displayed plot.
``--no-output-legend``    | Hide legend of the plot written to the output file.
``-m``, ``--measure``     | Specifies a column to visualize in a plot, i.e. ``Time``, ``Speedup`` or ``Efficiency``. ``Speedup`` is the default.
``adders``                | Space-separated names of parallel adders to display scalability of. The names are those of corresponding rows in the input CSV file. Only the CPU-parallel adders are supported. The acceptable names include "SMP", "AVX512" (and its alias "AVX512-S") and "AVX512-W". These are names which correspond to "SMP", "SMP-AVX512-S" and "SMP-AVX512-W" given in the list of adders below in the "Adders" section.

**Example**

	python visualize_scalability_data.py -i example.csv -S \
		-m Speedup --no-output-legend -o example.png SMP AVX512

Build a plot of speedups (``-m Speedup``), as defined in the paper, based on measurements for parallel SMP and AVX512 adders in the ``example.csv`` file (``-i example.csv``).
Show the plot in a dialog window (``-S``) and also save it to the ``example.png`` file (``-o example.png``) such that the image file will contain no legend (``--no-output-legend``).

## Adders

The following names of adders are used as arguments for the python scripts and basic programs listed above.

All of the adders are implemented in the ``code/adders`` directory with respect to the root directory of the repository.
The files given below reside in it.


Note that implementations of scalability measurements only accept CPU-parallel adders under their shorter alternative names.
In the following comprehensive list of adders these have names beginning with "SMP".
Also, LUT here stands for a lookup table as described in the paper.

* ADC - a sequential ripple-carry adder using the ``_addcarry_u64`` intrinsic function.
The adder is defined in ``scalar.cpp`` as the ``add_vectors_scalar`` function.
* ADC-ASM - same, implemented using x86-64 assembly.
The adder is defined in ``masm.asm`` (for Microsoft Macro Assembler) and ``as.s`` (for the GNU assembler) as the ``add_vectors_scalar_asm`` function.
* SSE4.2 (NO LUT) - SSE4.2 implementation of the vector algorithm (cf. section 5 of the article).
Instead of a LUT, this implementation uses blending and shuffling instructions for carry propagation which essentially make this adder sequential despite being implemented using vector instructions.
The adder is defined in ``sse4.2.cpp`` as the ``add_vectors_sse4_2_no_lut`` function.
* SSE4.2 (LUT) - SSE4.2 implementation of the vector algorithm using LUT to obtain the adjustments for vector additions.
The adder is described in the section 5 of the article and is defined in ``sse4.2.cpp`` as the ``add_vectors_sse4_2_lut`` function.
* AVX2 - AVX2 implementation using LUT (cf. section 5 in the article).
The adder is defined in ``avx2.cpp`` as the ``add_vectors_avx2`` function.
* AVX2 (ASM) - same, using assembly (cf. section 5 and fig. 4 in the article).
The adder is defined in ``masm.asm`` (for Microsoft Macro Assembler) and ``as.s`` (for the GNU assembler) as the ``add_vectors_avx2_asm`` function.
* AVX-512 - a single-core adder with AVX-512 vector algorithm presented in the section 5 of the article.
The adder is defined in ``avx512.cpp`` as the ``add_vectors_avx512`` function.
* AVX-512 (ASM) - same using assembly (cf. section 5 and fig. 3 in the article).
This implementation is more telling than AVX-512 when the latter is compiled with Visual Studio because it (as of CL version 19.29) duplicates computations upon mask registers with ones upon general-purpose registers.
The adder is defined in ``masm.asm`` (for Microsoft Macro Assembler) and ``as.s`` (for the GNU assembler) as the ``add_vectors_avx512_asm`` function.
* SMP - manycore implementation of the parallel algorithm without vectorization (cf. section 4 in the article).
The scalability experiments accept this adder under the same name "SMP".
The adder is defined in ``smp.cpp`` as the ``add_vectors_smp`` function.
* SMP-AVX512-S - manycore addition with AVX-512 vectorization as described in the section 5 of the article. This is the fastest adder.
Each thread of execution evaluates its own adjustment bits $i$, $c$ and $\varepsilon$ based on it entire _subvector_ (hence, "-S" in the name) as a result of its independent execution of the algorithm with vectorization (fig. 3 in the article), and then applies the adjustments to the sum in parallel (cf. fig. 2 in the article).  
The scalability experiments accept this adder under a shorter name "AVX512-S" and an alias "AVX512". The adder is defined in ``smp.cpp`` as the ``add_vectors_smp_avx512_subvector`` function.
* SMP-AVX512-W - an alternative way to perform addition in parallel and with AVX-512 vectorization.
As opposed to "SMP-AVX512-S", this adder evaluates the adjustment bits $i$, $c$ and $\varepsilon$ for every 512 bit ZMM _word_ (hence, "-W" in the name), rather than an entire subvector of a thread of execution.
Then addition proceeds as described in the section 4 of the paper using the same adder recursively.
Similarly to the other parallel adders, this implementation divides the vectors holding operands and the sum onto subvectors operated on in each thread individually with 64 bytes of data granularity (which matches the size of a cache line in x86-64) which excludes cache misses and false sharing.
However, due to greater number of IO operations this adder shows worse performance than the other parallel adders.
Because of that and due to less straightforward implementation, this adder is not described in the article.  
The scalability experiments accept this adder under a shorter name "AVX512-W".
The adder is defined in ``smp.cpp`` as the ``add_vectors_smp_avx512_word`` function.
* GMP - an adder which is based on the GNU Multiple Precision library (cf. fig. 10 in the article).
Performance measures do not take into account time taken by `mpz_import` and `mpz_export` to prepare and postprocess data with respect to the internal GMP data representation.  
The adder is defined in ``gmp.cpp`` as the ``add_vectors_gmp`` function.
* CUDA - an adder using NVIDIA CUDA devices (see fig. 9 in the article).
The performance measurements include time taken to exchange data between host and device.
See the note about CUDA implementation below.
* CUDA (LED) - Same, but performance measurements do not include time taken to initialize adder, transferring data between host and device, unless the device memory is insufficient to hold all of the data at once, in which case additional transfers occur impacting the resulting time taken into account by this experiment (see fig. 9 in the article). LED stands for "Loading Extra Data".
See the note about CUDA implementation below.
* CUDA (kernel) - Same but performance measurements only include summary time taken by the device to run the kernel code (fig. 9 in the article).
It does not take into account any data transfers between host and device.
This adder uses CUDA events to obtain the times.
These times are only available in milliseconds, not in CPU cycles.
See the note about CUDA implementation below.

**Note on CUDA implemetation**. All three CUDA adders use a single adder implementation, and the difference between them is only in times which are taken into account, as described above.
This implementation is presented in the ``cuda.cu`` file as the following functions: ``prepare_cuda_adder`` which initializes the adder and performs necessary allocations, ``run_cuda_adder`` which actually performs the long addition (and, if necessary, extra data transfers), ``finalize_cuda_adder`` which copies the results back to the host memory and frees up most resources, and ``delete_cuda_adder`` frees up the remaining resources and closes an adder handle.
These functions are used to obtain all three performance values, for "CUDA", "CUDA (LED)" and "CUDA (kernel)".
Also, ``add_vectors_cuda`` is defined on top of these functions to evaluate sums in one go.

## Shell scripts

The shell scripts are defined on top of the Python scripts to automate experimental data gathering during one of the following three workflows on Linux.
* **Full run**. Used to conduct all available experiments parameterized exactly as described in the article. On the platform described in the paper, this takes about 5.5 hours to complete all experiments.
* **For Reproducible Run** (the default) of CodeOcean such that all computational load is decreased compared to the full run. Additionally, in this workflow no CUDA adders are tested for compatibility reasons. The required time is reduced to about 30 minutes.
* **Debug run** used to test the scripts with reduced time impact of computations. This takes about a minute to complete.

By default, the experiments are parameterized for the Reproducible Run.

In order to replicate the experiments described in the article, the ``FOR_PAPER`` environment variable needs to be set to a non-empty value, e.g. by invoking ``export FOR_PAPER=1``. Then the shell scripts will perform the Full run.

Likewise, in order to perform a test invocation of the scripts, one can set the ``DEBUG`` environment variable which causes the shell scripts to perform the Debug run.
Note that the Debug run takes precedence over other workflows, i.e. setting the variable causes the script to perform the Debug run independently of the value of ``FOR_PAPER``.

All of these workflows are implemented in four shell scripts located in the ``code`` directory of the repository: ``run.sh`` (the main script), ``setdef.sh`` (defines parameters of experiment workflows), ``collect_csv.sh`` (collects experimental data by running the experiments) and ``visualize.sh`` (builds the performance plots).
The scripts are supposed to be invoked from the ``code`` subdirectory of the repository, i.e. the ``echo $PWD`` should display ``<path to repository>/code``.

The main script is ``run.sh`` which performs the experiments by means of the other three scripts.

In order to perform an experiment using the "For Reproducible Run" workflow, one can invoke ``run.sh`` as follows:

	./run.sh

This builds the basic programs using the ``make`` utility and the ``g++`` compiler, invokes the Python scripts using the ``python`` interpreter to run every experiment, except for CUDA, once, gather and visualize the results writing them to the ``../results`` directory.

In order to exactly reproduce the experiments as described in the article, the following invocation can be used:

	export FOR_PAPER=1 && ./run.sh

This builds the basic programs using the ``make`` utility, the ``g++`` C++ compiler and the ``nvcc`` CUDA compiler, invokes the Python scripts using the ``python`` interpreter to run every experiment, including CUDA on device #0, multiple times (10 or 20 depending on the experiment), gather, compute the average of and visualize the results writing CSV and corresponding plots to the ``../results`` directory.

Note a possible compilation error in ``host_config.h`` which is a part of CUDA library distribution:

	error: #error -- unsupported GNU version!

It originates from the NVIDIA CUDA compiler ``nvcc`` and tells about a mismatch of the C++ compiler and the CUDA compiler.
To resolve it, use matching compilers as described in the [NVIDIA CUDA System Requirements] (also confer the "Requirements for Compilation" section above):

	export CXX=g++-11 && \
	export NVCC=/usr/bin/nvcc-12 && \
	export FOR_PAPER=1 && \
	./run.sh

This causes the workflow to use the ``g++-11`` compiler, rather than the default ``g++``, together with the CUDA compiler ``/usr/bin/nvcc-12``.

In order to run the experiment using a CUDA device #1, rather than #0, use the following invocation:

	export CUDA_DEVICE=1 && export FOR_PAPER=1 && ./run.sh

In order to use a custom Python interpreter ``python3``, skip CUDA experiments and choose the ``~/adders`` directory for output, use the following invocation:

	export CUDA_DEVICE=-1 && \
	export PY=python3 && \
	export RESULTS_PATH=~/adders && \
	export FOR_PAPER=1 && \
	./run.sh

The full list of environment variables which provides points of customization of an experimental run, including compilation, is summarized in the table below.

Variable         | Meaning
-----------------|--------
``CXX``          | Path to a C++ compiler. Defaults to ``g++``.
``AS``           | Path to the GNU Assembler. Defaults to ``as``.
``NVCC``         | Path to the CUDA compiler. Defaults to ``nvcc``.
``CUDA_DEVICE``  | A 0-based identifier of a CUDA device to run the CUDA experiments on. On a computer with $N$ NVIDIA CUDA devices this value accepts values from 0 to $N-1$. Negative values turn off CUDA runs. The default is 0 unless the "For Reproducible Run" workflow is employed, in which case the default value is -1 (no CUDA runs).
``PY``           | Path to a python interpreter used to invoke the Python scripts. The default is ``python``.
``RESULTS_PATH`` | Path to an output directory with the results. If the directory does not exist, it is created. Defaults to ``results`` with respect to the root directory of the repository.

[NVIDIA CUDA System Requirements]: https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#system-requirements
[NVIDIA drivers]: https://docs.nvidia.com/deploy/cuda-compatibility/index.html
[GCC ABI Policy and Guideliness]: https://gcc.gnu.org/onlinedocs/libstdc++/manual/abi.html
[Visual Studio redistributables]: https://learn.microsoft.com/en-US/cpp/windows/latest-supported-vc-redist?view=msvc-170
[cudaSetDevice]: https://developer.download.nvidia.com/compute/DevZone/docs/html/C/doc/html/group__CUDART__DEVICE_g418c299b069c4803bfb7cab4943da383.html
