#include "../include/randomize.h"
#include "../include/utility.h"
#ifdef __GNUC__
#include <x86intrin.h>
//#define _rdtsc __rdtsc
#elif defined(_MSC_VER)
#include <intrin.h>
#define _rdtsc __rdtsc
#endif

bool avx512_available() noexcept;

static void randomize_drbg_seq(unsigned long long seed, void* pData, std::size_t cbData)
{
	unsigned char* pbData = (unsigned char*) pData;
	while (cbData >= 8)
	{
		seed = MMIX_MULTIPLIER * seed + MMIX_INCREMENT;
		*(unsigned long long*) pbData = seed;
		pbData += 8;
		cbData -= 8;
	}
	if (cbData != 0)
	{
		seed = seed * MMIX_MULTIPLIER + MMIX_INCREMENT;
		switch (cbData)
		{
		case 7:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 6:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 5:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 4:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 3:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 2:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		//case 1:
		default:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		};
	}
}

void randomize_drbg_vec(unsigned long long seed, void* pData, std::size_t cbData);

void randomize_seed(unsigned long long seed, void* pData, std::size_t cbData)
{
    static auto randomize_fn = avx512_available()?randomize_drbg_vec:randomize_drbg_seq;
    randomize_fn(seed, pData, cbData);
}

void randomize(void* pData, std::size_t cbData)
{
	randomize_seed((unsigned long long) _rdtsc(), pData, cbData);
}
