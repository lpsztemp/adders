#include <immintrin.h>
#include <cstdlib>
#include "../include/randomize.h"

constexpr unsigned long long get_m(unsigned n)
{
	return n == 0?1ull:get_m(n - 1) * MMIX_MULTIPLIER;
}
constexpr unsigned long long get_i(unsigned n)
{
	unsigned long long sum = 0;
	for (unsigned i = 0; i < n; ++i)
		sum += get_m(i);
	return sum * MMIX_INCREMENT;
}

alignas(64) const unsigned long long muls_epi64_0_7[] = 
{
	get_m(1), get_m(2), get_m(3), get_m(4), get_m(5), get_m(6), get_m(7), get_m(8)
};
alignas(64) const unsigned long long offs_epi64_0_7[] = 
{
	get_i(1), get_i(2), get_i(3), get_i(4), get_i(5), get_i(6), get_i(7), get_i(8)
};
alignas(64) const unsigned long long mul_epi64_8[] = 
{
	get_m(8), get_m(8), get_m(8), get_m(8), get_m(8), get_m(8), get_m(8), get_m(8)
};
alignas(64) const unsigned long long off_epi64_8[] = 
{
	get_i(8), get_i(8), get_i(8), get_i(8), get_i(8), get_i(8), get_i(8), get_i(8)
};
alignas(64) const unsigned long long mul_epi64_16[] = 
{
	get_m(16), get_m(16), get_m(16), get_m(16), get_m(16), get_m(16), get_m(16), get_m(16)
};
alignas(64) const unsigned long long off_epi64_16[] = 
{
	get_i(16), get_i(16), get_i(16), get_i(16), get_i(16), get_i(16), get_i(16), get_i(16)
};

__m512i my_mm512_mul_epu64(__m512i a, __m512i b)
{
	__m512i c00 = _mm512_mul_epu32(a, b);
	__m512i c01 = _mm512_mul_epu32(a, _mm512_srli_epi64(b, 32));
	__m512i c10 = _mm512_mul_epu32(b, _mm512_srli_epi64(a, 32));
	return _mm512_add_epi64(c00, _mm512_slli_epi64(_mm512_add_epi64(c01, c10), 32));
}

__m256i my_mm256_mul_epu64(__m256i a, __m256i b) //AVX2
{
	__m256i c00 = _mm256_mul_epu32(a, b);
	__m256i c01 = _mm256_mul_epu32(a, _mm256_srli_epi64(b, 32));
	__m256i c10 = _mm256_mul_epu32(b, _mm256_srli_epi64(a, 32));
	return _mm256_add_epi64(c00, _mm256_slli_epi64(_mm256_add_epi64(c01, c10), 32));
}

__m128i my_mm_mul_epu64(__m128i a, __m128i b) //SSE2
{
	__m128i c00 = _mm_mul_epu32(a, b);
	__m128i c01 = _mm_mul_epu32(a, _mm_srli_epi64(b, 32));
	__m128i c10 = _mm_mul_epu32(b, _mm_srli_epi64(a, 32));
	return _mm_add_epi64(c00, _mm_slli_epi64(_mm_add_epi64(c01, c10), 32));
}

//AVX-512
void randomize_drbg_vec(unsigned long long seed, void* pData, std::size_t cbData)
{
	unsigned char* pbData = (unsigned char*) pData;
	if (cbData >= 0x40)
	{
		__m512i X = _mm512_set1_epi64((long long) seed);
		X = _mm512_add_epi64(my_mm512_mul_epu64(X, _mm512_load_si512(muls_epi64_0_7)), _mm512_load_si512(offs_epi64_0_7));
		_mm512_storeu_si512(pbData, X);
		pbData += 0x40;
		if ((cbData -= 0x40) < 0x40)
			seed = (unsigned long long) _mm_extract_epi64(_mm512_extracti64x2_epi64(X, 3), 1);
		else
		{
			__m512i mul_8 = _mm512_load_si512(mul_epi64_8);
			__m512i off_8 = _mm512_load_si512(off_epi64_8);
			__m512i XX = _mm512_add_epi64(my_mm512_mul_epu64(X, mul_8), off_8);
			_mm512_storeu_si512(pbData, XX);
			pbData += 0x40;
			if ((cbData -= 0x40) >= 0x80)
			{
				__m512i mul_16 = _mm512_load_si512(mul_epi64_16);
				__m512i off_16 = _mm512_load_si512(off_epi64_16);
				do
				{
					X = _mm512_add_epi64(my_mm512_mul_epu64(X, mul_16), off_16);
					_mm512_storeu_si512(pbData, X);
					pbData += 0x40;
					XX = _mm512_add_epi64(my_mm512_mul_epu64(XX, mul_16), off_16);
					_mm512_storeu_si512(pbData, XX);
					pbData += 0x40;
				}while ((cbData -= 0x80) >= 0x80);
			}
			if (cbData & 0x40)
			{
				XX = _mm512_add_epi64(my_mm512_mul_epu64(XX, mul_8), off_8);
				_mm512_storeu_si512(pbData, XX);
				pbData += 0x40;
				cbData -= 0x40;
			}
			seed = (unsigned long long) _mm_extract_epi64(_mm512_extracti64x2_epi64(XX, 3), 1);
		}
	}
	if (cbData & 0x20)
	{
		__m256i X = _mm256_set1_epi64x((long long) seed);
		X = _mm256_add_epi64(my_mm256_mul_epu64(X, _mm256_load_si256((const __m256i*) muls_epi64_0_7)), _mm256_load_si256((const __m256i*) offs_epi64_0_7));
		_mm256_storeu_si256((__m256i*) pbData, X);
		pbData += 0x20;
		seed = (unsigned long long) _mm256_extract_epi64(X, 3);
	}
	if (cbData & 0x10)
	{
		__m128i X = _mm_set1_epi64x((long long) seed);
		X = _mm_add_epi64(my_mm_mul_epu64(X, _mm_load_si128((const __m128i*) muls_epi64_0_7)), _mm_load_si128((const __m128i*) offs_epi64_0_7));
		_mm_storeu_si128((__m128i*) pbData, X);
		pbData += 0x10;
		seed = (unsigned long long) _mm_extract_epi64(X, 1);
	}
	if (cbData & 8)
	{
		seed = seed * MMIX_MULTIPLIER + MMIX_INCREMENT;
		*(unsigned long long*) pbData = seed;
		pbData += 8;
	}
	if ((cbData &= 7) != 0)
	{
		seed = seed * MMIX_MULTIPLIER + MMIX_INCREMENT;
		switch (cbData)
		{
		case 7:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 6:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 5:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 4:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 3:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		case 2:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		//case 1:
		default:
			*pbData++ = (unsigned char) (seed & 0xFF);
			seed >>= 8;
		};
	}
}
