#include "./include/profile.h"
#include <limits>
#include <filesystem>
#include <cmath>

template <std::unsigned_integral UInt>
constexpr std::size_t ceil_log_2(UInt x)
{
	if (!x)
		return 1u;
	else if (std::countl_zero(x) == std::countr_zero(x) + 1)
		return static_cast<std::size_t>(std::countr_zero(x));
	else
		return static_cast<std::size_t>(std::numeric_limits<UInt>::digits - std::countl_zero(x));
}

std::size_t get_max_resolution(std::size_t start_log, std::size_t end_log, std::size_t min_delta)
{
	double d_min = (double) min_delta;
	verify(start_log > 0 && start_log < 64);
	verify(end_log > 0 && end_log < 64);
	verify(end_log > start_log);
	return (std::size_t) std::floor((d_min + std::sqrt(d_min * d_min + std::scalbn(d_min, (int) end_log + 3) - std::scalbn(d_min, (int) start_log + 3))) / (d_min * 2));
}

std::size_t get_min_end(std::size_t start_size, std::size_t resolution, std::size_t min_delta)
{
	if (ceil_log_2(min_delta) + ceil_log_2(resolution) + ceil_log_2(resolution - 1) >= std::numeric_limits<std::size_t>::digits)
		return 0;
	return (min_delta * (resolution * resolution - resolution) + 1) / 2 + start_size;
}

#include <tuple>
#include <regex>

auto parse_arguments(int argc, char** argv)
{
	using namespace std::literals;
	bool cq_log_start_def = true, cq_log_end_def = true, cq_resolution_def = true, cuda_device_def = true, smp_def = true;
	std::size_t cq_log_start = 3u, cq_log_end = 0x18u, cq_resolution = 256;
	std::size_t start_size, end_size, min_delta = 8;

	int smp = (int) get_num_threads();
	int cuda_device = -1;
	std::filesystem::path cycles_file, times_file;
	std::regex re_start_size{"(?:\\-s|\\-\\-start[\\-_]size)(?:=(\\w+))?\\b"};
	std::regex re_end_size{"(?:\\-e|\\-\\-end[\\-_]size)(?:=(\\w+))?\\b"};
	std::regex re_resolution{"(?:\\-r|\\-\\-resolution)(?:=(\\w+))?\\b"};
	std::regex re_smp{"\\-\\-smp(?:=([\\+\\-]?\\w+))?\\b"};
	std::regex re_cuda_device{"\\-\\-cuda[\\-_]device(?:=(\\-?\\w+))?\\b"};
	std::regex re_cycles_file{"(?:\\-c|\\-\\-cycles[\\-_]file)(?:=(.+))?\\b"};
	std::regex re_times_file{"(?:\\-t|\\-\\-times[\\-_]file)(?:=(.+))?\\b"};
	std::regex re_help{"(?:\\-h|\\-\\-help)"};
	bool use_gmp = false;
	bool use_gmp_def = true;
	std::regex re_gmp{"\\-\\-gmp\\b"};
	auto help_string = BASENAME " [-h] [-s START_SIZE] [-e END_SIZE] [-r resolution] [--smp T] [--cuda-device CUDA_DEVICE] [--gmp] [-c CYCLES_FILE] [-t TIMES_FILE]\n"
		R"raw(
Measures performance of long addition of different sizes implemented with available adders. Unlike the "performance" program,
the sizes are changed parabolically, rather than exponentially, i.e. with a difference between adjacent sizes growing linearly
with respect to a value of the desired resolution.

Parameters:
-h, --help        Display this message.
-s, --start-size  Starting size of addends. The value is expressed as base-2 logarithm of a number of 64-bit words. The
                  experiments are performed for sizes of operands ranging from the value of -s to a maximum that is not
                  greater than the value of -e. The value of -s must not be less than 3 (the default) which corresponds to
                  power(2, 3) * 64 = 512 bit.
-e, --end-size    Inclusive upper bound of the set of sizes. The value is expressed as base-2 logarithm of a number of 64-bit
                  words. The value can be increased in order to satisfy the requirement for resolution preserving 512 bit data
                  alignment and the law of size growth. Also, the value can be decreased by the program to preserve 512 bit
                  data alignment and the law of growth. The default value is )raw"s + std::to_string(cq_log_end) + R"raw(.
-r, --resolution  A minimal number of measurements to produce. The default value is )raw"s + std::to_string(cq_resolution) + R"raw( measurements.
--smp             A number of CPU threads to run the parallel adders. Negative values turn off SMP runs. Zero (the default) or
                  values greater than the number of logical CPU cores are internally changed to the actual number of the cores.
--cuda-device     Perform CUDA experiments using the specified CUDA device. The device is specified by an integral 0-based
                  index passed to cudaSetDevice. If the parameter is omitted, experiments on the CUDA adders are skipped.
--gmp             Perform computations using the GNU MP library, if one is available.
-c, --cycles-file A name of a file to accept a CSV table with performance of the adders expressed as a difference between
                  values of the CPU cycles counter returned by the RDTSC instruction after and before an invocation of an
                  adder. The file must not be the same as one of --times-file, if it is specified. If the file exists, it will
                  be silently overwritten. By default no such file is created.
-t, --times-file  A name of a file to accept a CSV table with performance of the adders expressed in milliseconds passed
                  between time points before and after each invocation of each adder. The file must not be the same as
                  one of --cycles-file, if it is specified. If the file exists, it will be silently overwritten. By default
                  no such file is created.

EXAMPLE

)raw"s + BASENAME + R"raw( -s 3 -e 20 -r 50 --smp 18 --cuda-device 0 --gmp -c cycles.csv -t times.csv

Measure performance of adding up random integers of sizes parabolically changed from power(2, 3) * 8 = 64 bytes (-s 3) until
a value of around power(2, 20) * 8 = 8 MiB (-e 20) is reached, such that the requested resolution of at least 50 data points
(``-r 50``) is met. The number of measurements must be at least 50 (-r 50).
Perform the measurement of performance of every available single-threaded CPU adder as well as the manycore implementations
using 18 threads (--smp 18), GPGPU CUDA adder on device #0 (--cuda-device 0) and the GNU Multiple Precision Arithmetic Library
(--gmp).
Print a table with the results to stdout. Also, export the results in the CSV format: write measured values of the CPU counter
to cycles.csv (-c cycles.csv), write time values to times.csv (-t times.csv) in the current directory.)raw"s;
	try {
		for (int i = 1; i < argc; ++i) try
		{
			std::cmatch m;
			if (std::regex_search(argv[i], m, re_start_size))
			{
				if (!cq_log_start_def)
					throw invalid_argument(argv[i]);
				auto val = std::stoll(m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]), nullptr, 0);
				if (val <= 2 || val > 63u)
					throw invalid_argument(argv[i]);
				cq_log_start = val;
				cq_log_start_def = false;
			}else if (std::regex_match(argv[i], m, re_end_size))
			{
				if (!cq_log_end_def)
					throw invalid_argument(argv[i]);
				auto val = std::stoll(m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]), nullptr, 0);
				if (val <= 2 || val > 63u)
					throw invalid_argument(argv[i]);
				cq_log_end = val;
				cq_log_end_def = false;
			}else if (std::regex_match(argv[i], m, re_resolution))
			{
				if (!cq_resolution_def)
					throw invalid_argument(argv[i]);
				auto val = std::stoll(m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]), nullptr, 0);
				if (val < 1 || (unsigned long long) val > std::numeric_limits<std::size_t>::max())
					throw invalid_argument(argv[i]);
				cq_resolution = val;
				cq_resolution_def = false;
			}else if (std::regex_match(argv[i], m, re_smp))
			{
				std::size_t e;
				if (!smp_def)
					throw invalid_argument(argv[i]);
				auto val = m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]);
				smp = std::stoi(val, &e, 0);
				if (e != val.size())
					throw invalid_argument(argv[i]);
				if (smp >= 0 && (unsigned) smp < get_num_threads())
					smp = get_num_threads();
				smp_def = false;
			}else if (std::regex_match(argv[i], m, re_cuda_device))
			{
				if (!cuda_device_def)
					throw invalid_argument(argv[i]);
				std::size_t e;
				auto val = m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]);
				cuda_device = std::stoi(val, &e, 0);
				if (e != val.size())
					throw invalid_argument(argv[i]);
				cuda_device_def = false;
			}else if (std::regex_match(argv[i], m, re_help))
			{
				if (argc != 2)
					throw invalid_argument(argv[i]);
				std::cout << help_string << '\n';
				std::exit(0);
			}
			else if (std::regex_match(argv[i], m, re_gmp))
			{
				if (!use_gmp_def)
					throw invalid_argument(argv[i]);
				use_gmp = true;
				use_gmp_def = false;
			}else if (std::regex_match(argv[i], m, re_cycles_file))
			{
				if (!cycles_file.empty())
					throw invalid_argument(argv[i]);
				cycles_file = std::filesystem::path{m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1])};
				{std::ofstream{cycles_file} << '#';}
				if (!std::filesystem::is_regular_file(cycles_file))
					throw invalid_arguments(std::string("Cannot access file ") + argv[i]);
			}else if (std::regex_match(argv[i], m, re_times_file))
			{
				if (!times_file.empty())
					throw invalid_argument(argv[i]);
				times_file = std::filesystem::path{m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1])};
				{std::ofstream{times_file} << '#';}
				if (!std::filesystem::is_regular_file(times_file))
					throw invalid_arguments(std::string("Cannot access file ") + argv[i]);
			}
			else
				throw invalid_argument(argv[i]);
		}catch (std::out_of_range&)
		{
			throw invalid_argument(argv[i]);
		}catch (std::filesystem::filesystem_error&)
		{
			throw invalid_argument(argv[i]);
		}
		if (cq_log_start >= cq_log_end)
			throw invalid_arguments("starting value (-s) must be strictly less than the ending value (-e)");
		if (!cycles_file.empty() && !times_file.empty() && std::filesystem::equivalent(cycles_file, times_file))
			throw invalid_arguments("output files must be different");
		start_size = std::size_t{1} << cq_log_start;
		end_size = std::size_t{1} << cq_log_end;
		std::size_t resolution_limit = get_max_resolution(cq_log_start, cq_log_end, min_delta);
		if (cq_resolution > resolution_limit)
		{
			std::size_t new_end_size = get_min_end(start_size, cq_resolution, min_delta);
			if (!new_end_size)
				throw invalid_arguments("for the specified size limits the resolution must not be greater than "s + std::to_string(resolution_limit));
			std::cerr << "Warning: for the specified size limits the resolution must not be greater than "  << resolution_limit << ". Increasing end size from " << end_size * 8 << "B to " << new_end_size * 8 << "B.\n";
			end_size = new_end_size;
		}
	}catch (std::invalid_argument& ex)
	{
		std::cerr << ex.what() << '\n';
		std::exit(1);
	}

	return std::tuple{start_size, end_size, cq_resolution, smp, cuda_device, use_gmp, cycles_file, times_file};
}

#include <bit>
#include <iostream>
#include <iomanip>
#include <limits>

int main(int argc, char** argv)
{
	auto [start_size, end_size, resolution, smp, cuda_device, gmp, cycles_file, times_file] = parse_arguments(argc, argv);
	std::vector<std::size_t> sizes;
	std::size_t delta = (end_size - start_size) / ((resolution * (resolution - 1)) >> 1) & ~std::size_t(7);
	for (std::size_t sz = start_size, d = 0; sz <= end_size; d += delta, sz += d)
		sizes.emplace_back(sz);
	auto fos_cycles = cycles_file.empty()?std::ofstream{}:std::ofstream{cycles_file};
	verify(!fos_cycles.fail());
	auto fos_times = times_file.empty()?std::ofstream{}:std::ofstream{times_file};
	verify(!fos_times.fail());
	auto result_table = profile(sizes, smp, cuda_device, gmp);

	std::cout << ">>RDTSC<<\n";
	std::cout << table_export{sizes, result_table.cycles_table, export_format::TXT};
	if (fos_cycles.is_open())
		fos_cycles << table_export{sizes, result_table.cycles_table, export_format::CSV};
	std::cout << ">>Time, ms.<<\n";
	auto get_times = [](const auto& arch) {return arch.times;};
	std::cout << table_export{sizes, result_table.ms_table, export_format::TXT};
	if (fos_times.is_open())
		fos_times << table_export{sizes, result_table.ms_table, export_format::CSV};

	return 0;
}
