ifeq ($(CXX), )
CXX=g++
endif
ifeq ($(AS), )
AS=as
endif
ifeq ($(NVCC), )
NVCC=nvcc
endif

ifeq ($(OBJ), )
OBJ=$(CURDIR)/obj
endif

ifeq ($(BIN), )
BIN=$(CURDIR)/bin
endif

GMP_AVAILABLE:=$(shell echo "\#include <gmp.h>" | $(CXX) -x c++ -E - > /dev/null 2>&1 && echo -DGMP_AVAILABLE=1)
NVCCAVAILABLE:=$(shell command -v $(NVCC) 2> /dev/null)
#NVCCVERSION_MAJOR:=$(shell command nvcc --version | grep " V" | sed -E 's/.*V([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+)/\1/'

DEBUG:=-g
OPTIMIZE=-O3
CXXFLAGS:=$(CXXFLAGS) $(DEBUG) $(OPTIMIZE)
NVCCFLAGS:=$(NVCCFLAGS) $(DEBUG) $(OPTIMIZE)

ifdef NVCCAVAILABLE
CUDAFILE:=cuda.cu
NVCCPARAMS:=$(NVCCFLAGS) --machine 64 --compile -cudart static --generate-code arch=compute_35,code=sm_35 -ccbin $(CXX)
NVCCLDPARAMS=-lcudart_static
ADDERFILES:=$(filter-out %.asm %cuda-stub.cpp, $(wildcard $(CURDIR)/adders/*))
else
$(warning $(NVCC) is not available. Skipping CUDA compilation.)
CUDAFILE:=cuda-stub.cpp
NVCC=$(CXX)
NVCCPARAMS=$(CXXFLAGS) -fPIE
ADDERFILES:=$(filter-out %.asm %cuda.cu, $(wildcard $(CURDIR)/adders/*))
endif

ifndef GMP_AVAILABLE
$(warning The "gmp.h" header is not available. Skipping GMP compilation.)
endif

TESTFILES:=$(wildcard $(CURDIR)/correctness-tests/*.cpp)
UTILITYFILES:=$(wildcard $(CURDIR)/utility/*.cpp)

ADDERFILESD=$(patsubst $(CURDIR)/%, $(OBJ)/%.d, $(filter-out %.s %.cu, $(ADDERFILES)))
ADDERFILESO=$(patsubst $(CURDIR)/%, $(OBJ)/%.obj, $(ADDERFILES))

UTILITYFILESD=$(patsubst $(CURDIR)/%, $(OBJ)/%.d, $(UTILITYFILES))
UTILITYFILESO=$(patsubst $(CURDIR)/%, $(OBJ)/%.obj, $(UTILITYFILES))

TESTFILESD=$(patsubst $(CURDIR)/%, $(OBJ)/%.d, $(TESTFILES))
TESTFILESO=$(patsubst $(CURDIR)/%, $(OBJ)/%.obj, $(TESTFILES))

.PHONY: all performance performance_hi_res scalability tests clean

all:performance performance_hi_res scalability tests
performance: $(BIN)/performance
performance_hi_res: $(BIN)/performance_hi_res
tests: $(BIN)/tests
scalability: $(BIN)/scalability

$(OBJ)/%.cpp.d: $(CURDIR)/%.cpp
	mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -MM -MF$@ -MT$@ -MT"$(@:.d=.obj)"  $<

-include $(ADDERFILESD) $(UTILITYFILESD) $(TESTFILESD) $(OBJ)/performance.cpp.d $(OBJ)/performance_hi_res.cpp.d $(OBJ)/scalability.cpp.d

$(BIN)/performance:$(OBJ)/performance.cpp.obj $(ADDERFILESO) $(UTILITYFILESO)
	mkdir -p $(dir $@)
	$(CXX) $(LDFLAGS) -o $@ $^ $(NVCCLDPARAMS) -ldl -lpthread -lrt

$(BIN)/performance_hi_res:$(OBJ)/performance_hi_res.cpp.obj $(ADDERFILESO) $(UTILITYFILESO)
	mkdir -p $(dir $@)
	$(CXX) $(LDFLAGS) -o $@ $^ $(NVCCLDPARAMS) -ldl -lpthread -lrt

$(BIN)/scalability:$(OBJ)/scalability.cpp.obj $(ADDERFILESO) $(UTILITYFILESO)
	mkdir -p $(dir $@)
	$(CXX) $(LDFLAGS) -o $@ $^ $(NVCCLDPARAMS) -ldl -lpthread -lrt

$(BIN)/tests: $(ADDERFILESO) $(UTILITYFILESO) $(TESTFILESO)
	mkdir -p $(dir $@)
	$(CXX) $(LDFLAGS) -o $@ $^ $(NVCCLDPARAMS) -ldl -lpthread -lrt

$(OBJ)/adders/avx512.cpp.obj: $(CURDIR)/adders/avx512.cpp $(OBJ)/adders/avx512.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -mavx512f -mavx512bw -mavx512vl -c -o $@ $<

$(OBJ)/adders/avx2.cpp.obj: $(CURDIR)/adders/avx2.cpp $(OBJ)/adders/avx2.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -mavx2 -mavx -c -o $@ $<

$(OBJ)/adders/sse4.2.cpp.obj: $(CURDIR)/adders/sse4.2.cpp $(OBJ)/adders/sse4.2.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -msse -msse2 -msse3 -mssse3 -msse4.1 -msse4.2 -c -o $@ $<

$(OBJ)/adders/scalar.cpp.obj: $(CURDIR)/adders/scalar.cpp $(OBJ)/adders/scalar.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -msse2 -msse -c -o $@ $<

$(OBJ)/adders/generic.cpp.obj: $(CURDIR)/adders/generic.cpp $(OBJ)/adders/generic.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -c -o $@ $<

$(OBJ)/adders/smp.cpp.obj: $(CURDIR)/adders/smp.cpp $(OBJ)/adders/smp.cpp.d
	$(CXX) $(CXXFLAGS) -m64 --std=c++20 -c -o $@ $<

$(OBJ)/adders/as.s.obj: $(CURDIR)/adders/as.s
	$(AS) $(DEBUG) -o $@ $<

$(OBJ)/adders/gmp.cpp.obj: $(CURDIR)/adders/gmp.cpp $(OBJ)/adders/gmp.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -c $(GMP_AVAILABLE) -o $@ $<

$(OBJ)/adders/$(CUDAFILE).obj: $(CURDIR)/adders/$(CUDAFILE)
	$(NVCC) $(NVCCPARAMS) -c -o $@ $<

$(OBJ)/utility/randomize.cpp.obj:$(CURDIR)/utility/randomize.cpp $(OBJ)/utility/randomize.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -c -o $@ $<

$(OBJ)/utility/randomize_v.cpp.obj:$(CURDIR)/utility/randomize_v.cpp $(OBJ)/utility/randomize_v.cpp.d
	$(CXX) $(CXXFLAGS) -m64 -mavx512f -mavx512dq --std=c++20 -c -o $@ $<

$(OBJ)/%.cpp.obj:$(CURDIR)/%.cpp $(OBJ)/%.cpp.d
	$(CXX) $(CXXFLAGS) -DBASENAME='"$(notdir $(basename $<))"' -m64 -c --std=c++20 -o $@ $<

clean:
	rm -rf obj bin
