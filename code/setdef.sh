SHPATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SCRIPT_PATH=$SHPATH/scripts
if [ -z "$CXX" ]; then
	export CXX=g++
fi;
if [ -z "$CUDA_DEVICE" ]; then
	CUDA_DEVICE=0
fi;
if [ -z "$PY" ]; then
	PY=python
fi;
if [ -z "$RESULTS_PATH" ]; then
	RESULTS_PATH=$SHPATH/../results
fi;

if [ "$DEBUG" ]; then
	E1_START=3
	E1_END=20
	E1_N=1
	REL_SMALL_START=12
	REL_SMALL_END=20
	REL_SMALL_RES=50
	REL_SMALL_N=1
	REL_BIG_START=14
	REL_BIG_END=24
	REL_BIG_RES=50
	REL_BIG_N=1
	SCAL_SMALL_SIZE=20
	SCAL_SMALL_N=1
	SCAL_BIG_SIZE=21
	SCAL_BIG_N=1
	CUDA_START=3
	CUDA_END=27
	CUDA_N=1
	CUDA_RES=50
elif [ "$FOR_PAPER" ]; then
	E1_START=3
	E1_END=30
	E1_N=10
	REL_SMALL_START=12
	REL_SMALL_END=20
	REL_SMALL_RES=511
	REL_SMALL_N=20
	REL_BIG_START=14
	REL_BIG_END=24
	REL_BIG_RES=2000
	REL_BIG_N=20
	SCAL_SMALL_SIZE=22
	SCAL_SMALL_N=10
	SCAL_BIG_SIZE=30
	SCAL_BIG_N=10
	CUDA_START=3
	CUDA_END=29
	CUDA_N=10
	CUDA_RES=200
else #for CodeOcean
	E1_START=3
	E1_END=30
	E1_N=3
	REL_SMALL_START=12
	REL_SMALL_END=20
	REL_SMALL_RES=511
	REL_SMALL_N=20
	REL_BIG_START=14
	REL_BIG_END=24
	REL_BIG_RES=511
	REL_BIG_N=3
	SCAL_SMALL_SIZE=22
	SCAL_SMALL_N=3
	SCAL_BIG_SIZE=29
	SCAL_BIG_N=3
	CUDA_DEVICE=-1 #In CodeOcean it is either CUDA or the required g++-10 compiler
fi;

size_string()
{
	if [ $1 -lt 7 ]; then
		echo "$((2**($1 + 3)))B"
	elif [ $1 -lt 17 ]; then
		echo "$((2**($1 + 3 - 10)))KiB"
	elif [ $1 -lt 27 ]; then
		echo "$((2**($1 + 3 - 20)))MiB"
	else
		echo "$((2**($1 + 3 - 30)))GiB"
	fi;
}

if [ ! -d "$RESULTS_PATH" ]; then
  mkdir -p $RESULTS_PATH
fi
