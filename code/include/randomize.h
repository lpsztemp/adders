#pragma once
#include <cstdlib>

#define MMIX_MULTIPLIER 6364136223846793005ull
#define MMIX_INCREMENT  1442695040888963407ull
//modulus: 1ull << 64

void randomize_seed(unsigned long long seed, void* pData, std::size_t cbData);
void randomize(void* pData, std::size_t cbData);

