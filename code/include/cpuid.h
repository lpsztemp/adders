#pragma once

#ifdef _MSC_VER
#include <intrin.h>
#define cpuid(fn, subfn, regs) __cpuidex((regs), (fn), (subfn))
#elif defined(__GNUC__)
#include "cpuid.h"
#define cpuid(fn, subfn, regs) \
	__asm__("cpuid\n" \
		: "=a" (regs[0]), "=b" (regs[1]), "=c" (regs[2]), "=d" (regs[3]) \
		: "a" (fn), "c" (subfn))
#else
#error "Not supported"
#endif

#define SSE4_2_MASK ((1u << 20) | (1u << 19) | (1u << 9) | 1u) //lev1
#define AVX_MASK (SSE4_2_MASK | (1u << 28) | (1u << 27)) //lev 1
#define AVX2_MASK (1u << 5) //lev 7
#define AVX512_MASK (AVX2_MASK | (1u << 16) | (1u << 17) | (1u << 30) | (1u << 31)) //lev 7
