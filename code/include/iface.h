#pragma once
#include <cstdlib>

void add_vectors_words(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
void add_vectors_scalar(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
extern "C" void add_vectors_scalar_asm(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;

bool sse4_2_available() noexcept;
void add_vectors_sse4_2_no_lut(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
void add_vectors_sse4_2_lut(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;

bool avx2_available() noexcept;
void add_vectors_avx2(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
extern "C" void add_vectors_avx2_asm(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;

bool avx512_available() noexcept; //F, VL, BW, DQ
void add_vectors_avx512(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
extern "C" void add_vectors_avx512_asm(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;

unsigned get_num_threads() noexcept;
void set_num_threads(unsigned T) noexcept;
void add_vectors_smp(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
void add_vectors_smp_avx512_word(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
void add_vectors_smp_avx512_subvector(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;

bool gmp_available() noexcept;
struct gmp_buffers;
gmp_buffers* prepare_gmp_adder(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;
void run_gmp_adder(gmp_buffers* handle) noexcept;
void finalize_gmp_adder(gmp_buffers* handle) noexcept;
void add_vectors_gmp(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;

bool set_cuda_device(int device) noexcept;
struct cuda_buffers;
cuda_buffers* prepare_cuda_adder(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb);
void run_cuda_adder(cuda_buffers* handle) noexcept;
struct cuda_event_times
{
	float kernel_time, total_time, old_time;
};
cuda_event_times finalize_cuda_adder(cuda_buffers* handle) noexcept;
void delete_cuda_adder(cuda_buffers* handle) noexcept;
void add_vectors_cuda(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept;

