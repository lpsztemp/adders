#pragma once
#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <string>

#define verify(flag) do {\
	if (!(flag)) { \
		std::cerr << #flag << " in " << __FILE__ << " (" << __LINE__ << ")\n"; \
		std::abort(); \
	}} while (false)

struct invalid_arguments:std::invalid_argument
{
	inline invalid_arguments():std::invalid_argument("Invalid arguments. Use -h for help.") {}
	inline invalid_arguments(const std::string& reason):std::invalid_argument(std::string("Invalid arguments: ") + reason + ". Use -h for help.") {}
};

struct invalid_argument:invalid_arguments
{
	inline invalid_argument() = default;
	inline invalid_argument(const std::string& arg):invalid_arguments(arg) {}
};
