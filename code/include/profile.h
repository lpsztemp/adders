#pragma once
#include "iface.h"
#include "utility.h"
#include "randomize.h"
#ifdef __GNUC__
#include <x86intrin.h>
#define rdtsc() ((long long) _rdtsc())
#elif defined(_MSC_VER)
#include <intrin.h>
#define rdtsc() ((long long) __rdtsc())
#endif
#include <chrono>
#include <memory>
#include <algorithm>
#include <vector>
#include <string>
#include <string_view>
#include <ranges>
#include <concepts>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <bit>
#include <utility>
#include <tuple>
#include <type_traits>
#include <functional>

struct times
{
	long long cycles;
	float milliseconds;
};

template <std::invocable<void*, const void*, const void*, std::size_t> fn_t>
times measure_time_of(fn_t fn, void* result, const void* addend_1, const void* addend_2, std::size_t cb)
{
	auto ms = std::chrono::steady_clock::now();
	auto cyc = -(long long) rdtsc();
	fn(result, addend_1, addend_2, cb);
	cyc += rdtsc();
	return times{cyc, static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - ms).count()) / 1000.0f};
}

struct cuda_times
{
	long long cycles_io;
	long long cycles_io_partial; //old measurement, no IO is taken into account unless data is too big to fit into device global memory in its entirety
	float time_io, time_kernel;
	float time_io_partial; //old measurement using events
};

inline cuda_times measure_time_of_cuda(void* result, const void* addend_1, const void* addend_2, std::size_t cb)
{
	auto cyc_io = -rdtsc();
	auto h = prepare_cuda_adder(result, addend_1, addend_2, cb);
	auto cyc_old = -rdtsc();
	run_cuda_adder(h);
	cyc_old += rdtsc();
	cuda_event_times event_time = finalize_cuda_adder(h);
	cyc_io += rdtsc();
	delete_cuda_adder(h);
	return {cyc_io, cyc_old, event_time.total_time, event_time.kernel_time, event_time.old_time};
}

inline times measure_time_of_gmp(void* result, const void* addend_1, const void* addend_2, std::size_t cb)
{
	auto h = prepare_gmp_adder(result, addend_1, addend_2, cb);
	auto tm = std::chrono::steady_clock::now();
	auto cyc = -rdtsc();
	run_gmp_adder(h);
	cyc += rdtsc();
	float ms = static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - tm).count()) / 1E3f;
	finalize_gmp_adder(h);
	return {cyc, ms};
}

template <class T>
concept StringViewish = std::constructible_from<std::string, T>;

template <class T>
concept StringViewishTuple = requires (T t) {{std::get<0>(t)} -> StringViewish; {std::tuple_size_v<std::decay_t<T>>} -> std::convertible_to<std::size_t>;};

struct performance_table_column
{
	std::string name;
	std::vector<std::string> measurements;

	template <StringViewish NameStr>
	performance_table_column(NameStr&& str):name(std::forward<NameStr>(str)) {}
	template <StringViewish Measurement>
	void add_value(Measurement&& m)
	{
		measurements.emplace_back(std::forward<Measurement>(m));
	}
};

struct performance_table
{
	std::vector<performance_table_column> columns;

	template <StringViewish...ColumnNames>
	performance_table(ColumnNames&&...column_names):columns({performance_table_column(std::forward<ColumnNames>(column_names))...}) {}
	template <StringViewish ... Values>
	void append_row(Values&& ... values)
	{
		verify(columns.size() == sizeof...(values));
		this->add_subrow(0, std::forward<Values>(values)...);
	}
	inline void append_columns(performance_table&& other)
	{
		if (!std::size(columns))
			columns = std::move(other.columns);
		else if (!!std::size(other.columns))
		{
			verify(std::size(columns[0].measurements) == std::size(other.columns[0].measurements));
			for (auto& c:other.columns)
				columns.emplace_back(std::move(c));
		}
	}
private:
	template <StringViewish T0, StringViewish...Ts>
	void add_subrow(std::size_t i, T0&& v0, Ts&&...vs)
	{
		columns[i].add_value(std::forward<T0>(v0));
		if constexpr (sizeof ... (Ts) != 0)
			this->add_subrow(i + 1, std::forward<Ts>(vs)...);
	}
};

struct performance_table_pair
{
	performance_table cycles_table, ms_table;

	performance_table_pair() = default;
	template <StringViewish StringView>
	performance_table_pair(StringView&& name):cycles_table(name), ms_table(name) {}
	template <StringViewishTuple CycleNamesTuple, StringViewishTuple MsNamesTuple>
	performance_table_pair(CycleNamesTuple&& cycle_names, MsNamesTuple&& ms_names)
		:performance_table_pair(std::forward<CycleNamesTuple>(cycle_names), std::make_index_sequence<std::tuple_size_v<std::decay_t<CycleNamesTuple>>>{},
			std::forward<MsNamesTuple>(ms_names), std::make_index_sequence<std::tuple_size_v<std::decay_t<MsNamesTuple>>>{}) {}
	inline void append_columns(performance_table_pair&& other)
	{
		cycles_table.append_columns(std::move(other.cycles_table));
		ms_table.append_columns(std::move(other.ms_table));
	}
	template <StringViewishTuple CycleValuesTuple, StringViewishTuple MsValuesTuple>
	void append_rows(CycleValuesTuple&& cycle_values, MsValuesTuple&& ms_values)
	{
		auto table_add = []<StringViewishTuple Tpl, std::size_t...Ind>(performance_table& table, Tpl&& tpl, std::index_sequence<Ind...>)
		{
			table.append_row(std::get<Ind>(std::forward<Tpl>(tpl))...);
		};
		table_add(cycles_table, std::forward<CycleValuesTuple>(cycle_values), std::make_index_sequence<std::tuple_size_v<std::decay_t<CycleValuesTuple>>>{});
		table_add(ms_table, std::forward<MsValuesTuple>(ms_values), std::make_index_sequence<std::tuple_size_v<std::decay_t<MsValuesTuple>>>{});
	}
	template <class Tpl> requires (std::tuple_size_v<std::decay_t<Tpl>> == 2) && StringViewishTuple<std::tuple_element_t<0, std::decay_t<Tpl>>> && StringViewishTuple<std::tuple_element_t<1, std::decay_t<Tpl>>>
	void append_rows(Tpl&& tpl)
	{
		this->append_rows(std::get<0>(std::forward<Tpl>(tpl)), std::get<1>(std::forward<Tpl>(tpl)));
	}
	template <class Tpl> requires (std::tuple_size_v<std::decay_t<Tpl>> == 2) && StringViewish<std::tuple_element_t<0, std::decay_t<Tpl>>> && StringViewish<std::tuple_element_t<1, std::decay_t<Tpl>>>
	void append_rows(Tpl&& tpl)
	{
		this->append_rows(std::tuple{std::get<0>(std::forward<Tpl>(tpl))}, std::tuple{std::get<1>(std::forward<Tpl>(tpl))});
	}

private:
	template <class CycleNamesTuple, std::size_t...IndCyc, class MsNamesTuple, std::size_t...IndMs>
	performance_table_pair(CycleNamesTuple&& cycle_names, std::index_sequence<IndCyc...>, MsNamesTuple&& ms_names, std::index_sequence<IndMs...>)
		:cycles_table(std::get<IndCyc>(std::forward<CycleNamesTuple>(cycle_names))...), ms_table(std::get<IndMs>(std::forward<MsNamesTuple>(ms_names))...) {}
};

template <std::invocable<void*, const void*, const void*, std::size_t> fn_t>
struct get_time_of
{
	template <StringViewish Name, class Fn>
	get_time_of(Name&& arch_name, Fn add_fn):arch_name(std::forward<Name>(arch_name)), m_add_fn(add_fn) {}
	performance_table_pair make_performance_table_pair()
	{
		return performance_table_pair{arch_name};
	}
	auto operator()(void* result, const void* addend_1, const void* addend_2, std::size_t cb) const
	{
		auto [cyc, tm] = measure_time_of(m_add_fn, result, addend_1, addend_2, cb);
		return std::tuple{std::to_string(cyc), std::to_string(tm)};
	}
	std::string arch_name;
private:
	fn_t m_add_fn;
};

template <class N, class F>
get_time_of(N, F) -> get_time_of<F>;

struct get_time_of_gmp
{
	inline performance_table_pair make_performance_table_pair()
	{
		return performance_table_pair{arch_name};
	}
	inline auto operator()(void* result, const void* addend_1, const void* addend_2, std::size_t cb) const
	{
		auto [cyc, tm] = measure_time_of_gmp(result, addend_1, addend_2, cb);
		return std::tuple{std::to_string(cyc), std::to_string(tm)};
	}

	static constexpr std::string_view arch_name = "GMP";
};

struct get_time_of_cuda
{
	static constexpr std::string_view arch_name = "CUDA";
	inline performance_table_pair make_performance_table_pair()
	{
		using namespace std::literals::string_literals;
		return performance_table_pair{std::tuple{"CUDA"s, "CUDA (LED)"s}, std::tuple{"CUDA"s, "CUDA (kernel)"s, "CUDA (LED)"s}};
	}
	inline auto operator()(void* result, const void* addend_1, const void* addend_2, std::size_t cb) const
	{
		auto [cyc_io, cyc_io_old, tm_io, tm_krnl, tm_io_old] = measure_time_of_cuda(result, addend_1, addend_2, cb);
		return std::tuple{std::tuple{std::to_string(cyc_io), std::to_string(cyc_io_old)}, std::tuple{std::to_string(tm_io), std::to_string(tm_krnl), std::to_string(tm_io_old)}};
	}
};

template <std::ranges::input_range S, std::invocable<void*, const void*, const void*, std::size_t> measure_fn_t>
performance_table_pair run_experiment(S sizes, measure_fn_t measurement, void* result, const void* addend_1, const void* addend_2)
{
	performance_table_pair measurements = measurement.make_performance_table_pair();
	std::clog << "Profiling " << measurement.arch_name << "\n";
	std::size_t N = std::size(sizes);
	std::size_t percent = (N + 99u) / 100u, next_percentage = percent, iteration = 0;
	for (std::size_t i = N / percent; i < 100u; ++i)
		std::clog.put('=');
	for (auto cq: sizes)
	{
		measurements.append_rows(measurement(result, addend_1, addend_2, cq * sizeof(long long)));
		if (++iteration >= next_percentage)
		{
			std::clog.put('=');
			next_percentage += percent;
		}
	}
	std::clog << '\n';
	return measurements;
}

template <std::ranges::input_range S> requires(std::is_same_v<std::ranges::range_value_t<S>, std::size_t>)
performance_table_pair profile(S sz, int smp, int cuda_device, bool do_gmp)
{
	std::vector<std::size_t> sizes;
	std::ranges::copy(sz, std::back_inserter(sizes));
	auto cq_max = *std::rbegin(sizes);
	auto input_1 = std::make_unique<unsigned long long[]>(cq_max);
	auto input_2 = std::make_unique<unsigned long long[]>(cq_max);
	auto result = std::make_unique<unsigned long long[]>(cq_max);
	randomize(input_1.get(), cq_max * sizeof(unsigned long long));
	randomize(input_2.get(), cq_max * sizeof(unsigned long long));
	performance_table_pair tables;
	tables.append_columns(run_experiment(sizes, get_time_of{"ADC", &add_vectors_scalar}, result.get(), input_1.get(), input_2.get()));
	tables.append_columns(run_experiment(sizes, get_time_of{"ADC-ASM", add_vectors_scalar_asm}, result.get(), input_1.get(), input_2.get()));
	if (!sse4_2_available())
		std::cerr << "Warning: SSE4.2 support is missing.\n";
	else
	{
		tables.append_columns(run_experiment(sizes, get_time_of{"SSE4.2 (NO LUT)", add_vectors_sse4_2_no_lut}, result.get(), input_1.get(), input_2.get()));
		tables.append_columns(run_experiment(sizes, get_time_of{"SSE4.2 (LUT)", add_vectors_sse4_2_lut}, result.get(), input_1.get(), input_2.get()));
	}
	if (!avx2_available())
		std::cerr << "Warning: AVX2 support is missing.\n";
	else
	{
		tables.append_columns(run_experiment(sizes, get_time_of{"AVX2", add_vectors_avx2}, result.get(), input_1.get(), input_2.get()));
		tables.append_columns(run_experiment(sizes, get_time_of{"AVX2 (ASM)", add_vectors_avx2_asm}, result.get(), input_1.get(), input_2.get()));
	}
	if (!avx512_available())
		std::cerr << "Warning: AVX-512 support is missing.\n";
	else
	{
		tables.append_columns(run_experiment(sizes, get_time_of{"AVX-512", add_vectors_avx512}, result.get(), input_1.get(), input_2.get()));
		tables.append_columns(run_experiment(sizes, get_time_of{"AVX-512 (ASM)", add_vectors_avx512_asm}, result.get(), input_1.get(), input_2.get()));
	}
	if (smp > 0)
	{
		set_num_threads((unsigned) smp);
		tables.append_columns(run_experiment(sizes, get_time_of{"SMP", add_vectors_smp}, result.get(), input_1.get(), input_2.get()));
		if (avx512_available())
		{
			tables.append_columns(run_experiment(sizes, get_time_of{"SMP-AVX512-W", add_vectors_smp_avx512_word}, result.get(), input_1.get(), input_2.get()));
			tables.append_columns(run_experiment(sizes, get_time_of{"SMP-AVX512-S", add_vectors_smp_avx512_subvector}, result.get(), input_1.get(), input_2.get()));
		}
	}
	if (do_gmp)
	{
		if (!gmp_available())
			std::cerr << "Warning: GMP support is missing.\n";
		else
			tables.append_columns(run_experiment(sizes, get_time_of_gmp{}, result.get(), input_1.get(), input_2.get()));
	}
	if (cuda_device >= 0)
	{
		if (!set_cuda_device(cuda_device))
			std::cerr << "Warning: CUDA support is missing.\n";
		else
			tables.append_columns(run_experiment(sizes, get_time_of_cuda{}, result.get(), input_1.get(), input_2.get()));
	}
	return tables;
}

enum class export_format {TXT, CSV};
template <std::ranges::sized_range S>
struct table_export 
{
	const S& sizes;
	const performance_table& values;
	export_format format;
};

template <std::ranges::input_range S>
std::ostream& operator<<(std::ostream& os, table_export<S>&& table)
{
	if (table.format == export_format::TXT)
	{
		auto size_w = std::max(std::size_t(4), sizeof(std::size_t) * 2 - std::ranges::min(table.sizes | std::views::transform(std::countl_zero<std::size_t>)) / 4);
		auto widths = std::invoke([](auto rng) {return std::vector(std::begin(rng), std::end(rng));},
			table.values.columns | std::views::transform([&table](auto& col){return std::max(col.name.size(), std::ranges::max(col.measurements, {}, &std::string::size).size());}));

		os << std::left << std::setw(size_w) << "Size" << "  ";
		for (std::size_t c = 0; c < std::size(table.values.columns); ++c)
			os << '|' << std::setw(widths[c]) << table.values.columns[c].name;
		os << '\n';
		for (std::size_t r = 0; r < std::size(table.sizes); ++r)
		{
			os << "0x" << std::hex << std::setw(size_w) << table.sizes[r] << std::dec;
			for (std::size_t c = 0; c < std::size(table.values.columns); ++c)
				os << '|' << std::left << std::setw(widths[c]) << table.values.columns[c].measurements[r];
			os << '\n';
		}
	}else
	{
		os << "Size";
		for (std::size_t c = 0; c < std::size(table.values.columns); ++c)
		{
			os << ',';
			const auto& arch_name = table.values.columns[c].name;
			auto p0 = arch_name.find_first_not_of(" \t");
			verify(p0 != arch_name.npos);
			auto p1 = arch_name.find_last_not_of(" \t");
			verify(p1 != arch_name.npos);
			auto trim = arch_name.substr(p0, p1 + 1 - p0);
			bool enquote = trim.find_first_of("\",") != ~std::size_t{};
			if (enquote)
				os << '\"';
			p0 = 0;
			while (true)
			{
				p1 = trim.find('\"', p0);
				if (p1 == trim.npos)
					break;
				os << trim.substr(p0, p1 - p0) << "\"\"";
				p0 = p1 + 1;
			}
			os << trim.substr(p0);
			if (enquote)
				os << '\"';
		}
		os << '\n';
		for (std::size_t r = 0; r < std::size(table.sizes); ++r)
		{
			os << table.sizes[r];
			for (std::size_t c = 0; c < std::size(table.values.columns); ++c)
				os << ',' << table.values.columns[c].measurements[r];
			os << '\n';
		}
	}
	return os;
}