#include "./include/iface.h"
#include "./include/utility.h"
#include "./include/randomize.h"
#include <limits>
#ifdef __GNUC__
#include <x86intrin.h>
#define rdtsc() ((long long) _rdtsc())
#elif defined(_MSC_VER)
#include <intrin.h>
#define rdtsc() ((long long) __rdtsc())
#endif
#include <memory>
#include <algorithm>
#include <vector>
#include <string>
#include <string_view>
#include <ranges>
#include <concepts>
#include <iostream>
#include <cstdint>

template <std::invocable<void*, const void*, const void*, std::size_t> fn_t>
long long measure_time_of(fn_t fn, void* result, const void* addend_1, const void* addend_2, std::size_t cb)
{
	auto tm = -(long long) rdtsc();
	fn(result, addend_1, addend_2, cb);
	return tm + rdtsc();
}

template <std::invocable<void*, const void*, const void*, std::size_t> SmpAddProc>
std::vector<long long> smp_scalability_test(SmpAddProc add_proc, std::size_t word_count)
{
	std::vector<long long> result;
	auto old_T = get_num_threads();
	auto buf_1 = std::make_unique<std::uint64_t[]>(word_count);
	auto buf_2 = std::make_unique<std::uint64_t[]>(word_count);
	auto buf_r = std::make_unique<std::uint64_t[]>(word_count);
	for (unsigned T = 1; T <= old_T; ++T)
	{
		randomize(buf_1.get(), word_count * sizeof(std::uint64_t));
		randomize(buf_2.get(), word_count * sizeof(std::uint64_t));
		set_num_threads(T);
		result.emplace_back(measure_time_of(add_proc, buf_r.get(), buf_1.get(), buf_2.get(), word_count * sizeof(std::uint64_t)));
		std::cerr << '=';
	}
	std::cerr << '\n';
	return result;
}

template <std::ranges::contiguous_range V>
double average_effectiveness(V&& rng)
{
	const auto t1 = rng[0];
	double result = 1;
	for (std::size_t T = 1; T < std::size(rng); ++T)
		result += static_cast<double>(t1) / (rng[T] * (T + 1));
	return result / std::size(rng);
}

#include <tuple>
#include <regex>

enum class impl_id_t {SMP, AVX512W, AVX512S};

auto parse_arguments(int argc, char** argv)
{
	using namespace std::literals;
	bool cq_size_def = true, impl_id_def = true;
	int cq_log_size = 5u;
	impl_id_t impl_id = impl_id_t::SMP;
	std::regex re_size{"(?:\\-s|\\-\\-size)(?:=(\\+?\\w+))?\\b"};
	std::regex re_impl{"(?:\\-i|\\-\\-impl)(?:=(\\w+))?\\b"};
	std::regex re_help{"(?:\\-h|\\-\\-help)"};
	auto invalid_use = "Invalid use. Run with -h for help."sv;
	auto help_string = BASENAME " [-h] [-s SIZE] [-i IMPL]\n\n"
		"Measures time performance, speedups and parallelization efficiency (speedup divided by the number of threads used) of parallel\n"
		"adders depending on how many logical CPU cores are employed.\n\n"
		"Parameters:\n"
		"-h, --help       Display this message.\n\n"
		"-s, --size       Size of addends. The value is expressed as base-2 logarithm of a number of 64-bit words. The value must not be\n"
		"                 less than 3. The default value is "s + std::to_string(cq_log_size) + " which yields size equal to power(2, "s + std::to_string(cq_log_size) +
			") = "s + std::to_string(std::size_t(1) << cq_log_size) + " words or "s + std::to_string(std::size_t(1) << (cq_log_size + 3)) + " bytes.\n" +
		"-i, --impl       Parallel adder implementation. Can be either \"SMP\" (the default), \"AVX512-W\" or \"AVX512-S\" (the latter has an\n"
		"                 alias \"AVX512\"). A full list of adders is presented in the \"Adders\" section, of the README.md file, in which\n"
		"                 the parallel adders are described under the respective names \"SMP\", \"SMP-AVX512-W\" and \"SMP-AVX512-S\"." + R"raw(

EXAMPLE

)raw"s + BASENAME + R"raw( -s 25 -i SMP

Measure performance, speedups and efficiency of parallelization of adding up random integers of size power(2, 25) * 8 = 256 MiB
(-s 25) using the SMP adder (-i SMP). Print a table with the results to stdout.)raw"s;
	try {
		for (int i = 1; i < argc; ++i)
		{
			std::cmatch m;
			if (std::regex_search(argv[i], m, re_size))
			{
				if (!cq_size_def)
					throw invalid_argument(argv[i]);
				cq_log_size = std::stoi(m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]), nullptr, 0);
				if (cq_log_size <= 2 || cq_log_size >= std::numeric_limits<std::size_t>::digits)
					throw invalid_argument(argv[i]);
				cq_size_def = false;
			}else if (std::regex_search(argv[i], m, re_impl))
			{
				if (!impl_id_def)
					throw invalid_argument(argv[i]);
				auto val = m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_arguments(argv[i-1]);
				if (std::regex_match(val, std::regex{"smp", std::regex::icase}))
					impl_id = impl_id_t::SMP;
				else if (std::regex_match(val, std::regex{"avx512\\-?W", std::regex::icase}))
					impl_id = impl_id_t::AVX512W;
				else if (std::regex_match(val, std::regex{"avx512(?:\\-S|S)?", std::regex::icase}))
					impl_id = impl_id_t::AVX512S;
				else
					throw invalid_argument(argv[i]);
				impl_id_def = false;
			}else if (std::regex_match(argv[i], m, re_help))
			{
				if (argc != 2)
					throw invalid_argument(argv[i]);
				std::cout << help_string << '\n';
				std::exit(0);
			}else
				throw invalid_argument(argv[i]);
		}
	}catch (invalid_arguments& ex)
	{
		std::cerr << ex.what() << '\n';
		std::exit(1);
	}

	return std::tuple{impl_id, static_cast<std::size_t>(1) << cq_log_size};
}

#include <bit>
#include <iostream>
#include <sstream>
#include <iomanip>

template <std::ranges::view V>
auto to_vector(V r)
{
	std::vector<std::ranges::range_value_t<V>> res;
	for (auto val:r)
		res.emplace_back(val);
	return res;
}

struct to_string
{
	template <class T> requires (!std::is_floating_point_v<T>) && requires (T x) {std::to_string(x);}
	auto operator()(const T& val) const
	{
		return std::to_string(val);
	}

	template <std::floating_point T>
	auto operator()(T val) const
	{
		std::ostringstream os;
		os.precision(3);
		os << val;
		return os.str();
	}
};

int main(int argc, char** argv)
{
	using namespace std::literals;
	auto [impl_id, size] = parse_arguments(argc, argv);
	auto proc = impl_id == impl_id_t::SMP?add_vectors_smp:
		impl_id == impl_id_t::AVX512S?add_vectors_smp_avx512_subvector:
		add_vectors_smp_avx512_word;
	if ((impl_id == impl_id_t::AVX512S || impl_id == impl_id_t::AVX512W) && !avx512_available())
	{
		std::cerr << "Required AVX-512 instructions are not implemented by the device. Terminating.\n";
		return 1;
	}
	auto sv_threads = "Threads"sv;
	auto threads = to_vector(std::views::iota(1u, get_num_threads() + 1u));
	auto sv_time = "Time"sv;
	auto times = smp_scalability_test(proc, size);
	auto sv_speedup = "Speedup"sv;
	auto speedups = to_vector(times | std::views::transform([&times](auto tm) {return static_cast<double>(times[0]) / tm;}));
	auto sv_efficiency = "Efficiency"sv;
	auto efficiencies = to_vector(threads | std::views::transform([&speedups](auto t) {return speedups[t-1] / t;}));
	
	std::cout.precision(3);
	auto threads_s = to_vector(threads | std::views::transform(to_string{}));
	auto times_s = to_vector(times | std::views::transform(to_string{}));
	auto speedups_s = to_vector(std::views::all(speedups) | std::views::transform(to_string{}));
	auto efficiencies_s = to_vector(efficiencies | std::views::transform(to_string{}));
	
	auto get_width = [](const auto& header, const std::vector<std::string>& str_vals)
		{return std::max(std::size(header), std::ranges::max(str_vals, {}, &std::string::size).size());};
	auto threads_w = get_width(sv_threads, threads_s);
	auto time_w = get_width(sv_time, times_s);
	auto speedup_w = get_width(sv_speedup, speedups_s);
	auto efficiency_w = get_width(sv_efficiency, efficiencies_s);
	
	std::cout
		<< std::setw(threads_w) << sv_threads << '|'
		<< std::setw(time_w) << sv_time << '|'
		<< std::setw(speedup_w) << sv_speedup << '|'
		<< std::setw(efficiency_w) << sv_efficiency << '\n';
	for (auto t:threads)
		std::cout
			<< std::setw(threads_w) << threads_s[t-1] << '|'
			<< std::setw(time_w) << times_s[t-1] << '|'
			<< std::setw(speedup_w) << speedups_s[t-1] << '|'
			<< std::setw(efficiency_w) << efficiencies_s[t-1] << '\n';
	
	return 0;
}
