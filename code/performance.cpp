#include "./include/profile.h"

#include <tuple>
#include <regex>
#include <filesystem>

auto parse_arguments(int argc, char** argv)
{
	using namespace std::literals;
	bool cq_start_def = true, cq_end_def = true, cuda_device_def = true, smp_def = true;
	std::size_t cq_start = 3u, cq_end = 0x18u;
	int smp = (int) get_num_threads();
	int cuda_device = -1;
	std::filesystem::path cycles_file, times_file;
	std::regex re_start_size{"(?:\\-s|\\-\\-start[\\-_]size)(?:=(\\w+))?\\b"};
	std::regex re_end_size{"(?:\\-e|\\-\\-end[\\-_]size)(?:=(\\w+))?\\b"};
	std::regex re_smp{"\\-\\-smp(?:=([\\+\\-]?\\w+))?\\b"};
	std::regex re_cuda_device{"\\-\\-cuda[\\-_]device(?:=(\\-?\\w+))?\\b"};
	std::regex re_cycles_file{"(?:\\-c|\\-\\-cycles[\\-_]file)(?:=(.+))?\\b"};
	std::regex re_times_file{"(?:\\-t|\\-\\-times[\\-_]file)(?:=(.+))?\\b"};
	std::regex re_help{"(?:\\-h|\\-\\-help)"};
	bool use_gmp = false;
	bool use_gmp_def = true;
	std::regex re_gmp{"\\-\\-gmp\\b"};
	auto help_string = BASENAME " [-h] [-s START_SIZE] [-e END_SIZE] [--smp T] [--cuda-device CUDA_DEVICE] [--gmp] [-c CYCLES_FILE] [-t TIMES_FILE]\n"
		R"raw(
Measures performance of long addition of different sizes implemented with various adders. The sizes, which are expressed as
powers of two, are changed exponentially, i.e. by incrementing the exponent from a given starting value to the given
(inclusively) ending value. To measure performance with higher custom resolution, use the "performance_hi_res" program.

Parameters:
-h, --help        Display this message.
-s, --start-size  Starting size of addends. The value is expressed as base-2 logarithm of a number of 64-bit words. The
                  experiments are invoked for sizes of operands changing exponentially as a power of 2 ranging from the value
                  of -s to the value of -e (inclusive). The value must be at least 3 (the default).
-e, --end-size    Inclusive final size of addends. The value is expressed as a base-2 logarithm of a number of 64-bit words.
                  The default value is )raw"s + std::to_string(cq_end) + R"raw(.
--smp             A number of CPU threads to run the parallel adders. Negative values turn off SMP runs. Zero (the default) or
                  values greater than the number of logical CPU cores are internally changed to the actual number of the cores.
--cuda-device     Perform CUDA experiments using the specified CUDA device. The device is specified by an integral 0-based
                  index passed to cudaSetDevice.
--gmp             Perform computations using the GNU MP library, if one is available.
-c, --cycles-file A name of a file to accept a CSV table with performance of the adders expressed as a difference between
                  values of the CPU cycles counter returned by the RDTSC instruction after and before each invocation of
                  each adder. The file must not be the same as one of --times-file, if the latter is specified. If the file
                  exists, it will be silently overwritten. By default no such file is created.
-t, --times-file  A name of a file to accept a CSV table with performance of the adders expressed in milliseconds passed
                  between time points before and after each invocation of each adder. The file must not be the same
                  as one of --cycles-file, if it is specified. If the file exists, it will be silently overwritten. By default
                  no such file is created.

EXAMPLE

)raw"s + BASENAME + R"raw( -s 3 -e 20 --smp 18 --cuda-device 0 --gmp -c cycles.csv -t times.csv

Measure performance of adding up random integer values of exponentially changed sizes starting from power(2, 3) * 8 = 64
bytes (-s 3) and ending after performance of power(2, 20) * 8 = 8 MiB addition (-e 20) has been measured. Perform the
measurements of performance of every available single-threaded CPU adder as well as manycore implementation using 18 threads
(--smp 18), GPGPU CUDA adder on device #0 (--cuda-device 0) and The GNU Multiple Precision Arithmetic Library (--gmp). Print
a table with the results to stdout. Also, export the results in the CSV format: write measured values of the CPU counter to
cycles.csv (-c cycles.csv), write time values to times.csv (-t times.csv), such that both files will reside in the current
directory.)raw"s;
	try {
		for (int i = 1; i < argc; ++i) try
		{
			std::cmatch m;
			if (std::regex_search(argv[i], m, re_start_size))
			{
				if (!cq_start_def)
					throw invalid_argument(argv[i]);
				auto val = std::stoll(m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_arguments(argv[i-1]), nullptr, 0);
				if (val <= 2 || val > 63u)
					throw invalid_argument(argv[i]);
				cq_start = val;
				cq_start_def = false;
			}else if (std::regex_match(argv[i], m, re_end_size))
			{
				if (!cq_end_def)
					throw invalid_argument(argv[i]);
				auto val = std::stoll(m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]), nullptr, 0);
				if (val <= 2 || val > 63u)
					throw invalid_argument(argv[i]);
				cq_end = val;
				cq_end_def = false;
			}else if (std::regex_match(argv[i], m, re_smp))
			{
				std::size_t e;
				if (!smp_def)
					throw invalid_argument(argv[i]);
				auto val = m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]);
				smp = std::stoi(val, &e, 0);
				if (e != val.size())
					throw invalid_argument(argv[i]);
				if (smp >= 0 && (unsigned) smp < get_num_threads())
					smp = get_num_threads();
				smp_def = false;
			}else if (std::regex_match(argv[i], m, re_cuda_device))
			{
				if (!cuda_device_def)
					throw invalid_arguments(argv[i]);
				std::size_t e;
				auto val = m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1]);
				cuda_device = std::stoi(val, &e, 0);
				if (e != val.size())
					throw invalid_argument(argv[i]);
				cuda_device_def = false;
			}else if (std::regex_match(argv[i], m, re_help))
			{
				if (argc != 2)
					throw invalid_argument(argv[i]);
				std::cout << help_string << '\n';
				std::exit(0);
			}else if (std::regex_match(argv[i], m, re_gmp))
			{
				if (!use_gmp_def)
					throw invalid_argument(argv[i]);
				use_gmp = true;
				use_gmp_def = false;
			}else if (std::regex_match(argv[i], m, re_cycles_file))
			{
				if (!cycles_file.empty())
					throw invalid_argument(argv[i]);
				cycles_file = std::filesystem::path{m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1])};
				{std::ofstream{cycles_file} << '#';}
				if (!std::filesystem::is_regular_file(cycles_file))
					throw invalid_arguments(std::string("Cannot access file ") + argv[i]);
			}else if (std::regex_match(argv[i], m, re_times_file))
			{
				if (!times_file.empty())
					throw invalid_argument(argv[i]);
				times_file = std::filesystem::path{m[1].matched?m[1].str():++i<argc?argv[i]:throw invalid_argument(argv[i-1])};
				{std::ofstream{times_file} << '#';}
				if (!std::filesystem::is_regular_file(times_file))
					throw invalid_arguments(std::string("Cannot access file ") + argv[i]);
			}else
				throw invalid_argument(argv[i]);
		}catch (std::out_of_range&)
		{
			throw invalid_argument(argv[i]);
		}catch (std::filesystem::filesystem_error&)
		{
			throw invalid_argument(argv[i]);
		}
		if (cq_start > cq_end)
			throw invalid_arguments("the start size must be less or equal to the end size");
		if (!cycles_file.empty() && !times_file.empty() && std::filesystem::equivalent(cycles_file, times_file))
			throw invalid_arguments("output files must be different");
	}catch (std::invalid_argument& ex)
	{
		std::cerr << ex.what() << '\n';
		std::exit(1);
	}

	return std::tuple{cq_start, cq_end, smp, cuda_device, use_gmp, cycles_file, times_file};
}


int main(int argc, char** argv)
{
	auto [start_size, end_size, smp, cuda_device, gmp, cycles_file, times_file] = parse_arguments(argc, argv);
	auto sizes = std::views::iota(start_size, end_size + 1) | std::views::transform([](auto size) {return std::size_t(1u) << size;});
	auto fos_cycles = cycles_file.empty()?std::ofstream{}:std::ofstream{cycles_file};
	verify(!fos_cycles.fail());
	auto fos_times = times_file.empty()?std::ofstream{}:std::ofstream{times_file};
	verify(!fos_times.fail());

	auto result_table = profile(sizes, smp, cuda_device, gmp);

	std::cout << ">>RDTSC<<\n";
	std::cout << table_export{sizes, result_table.cycles_table, export_format::TXT};
	if (fos_cycles.is_open())
		fos_cycles << table_export{sizes, result_table.cycles_table, export_format::CSV};
	std::cout << ">>Time, ms.<<\n";
	std::cout << table_export{sizes, result_table.ms_table, export_format::TXT};
	if (fos_times.is_open())
		fos_times << table_export{sizes, result_table.ms_table, export_format::CSV};
	return 0;
}
