. setdef.sh

tm0=$SECONDS
make || exit 1
$SHPATH/bin/tests || { echo 'Test failure'; exit 1; }
. $SHPATH/collect_csv.sh
. $SHPATH/visualize.sh
echo Time elapsed: $(date -u -d @$(($SECONDS - $tm0)) +%T)
