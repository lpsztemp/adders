import pandas as pd
import os
import argparse
import datetime
import sys
import tempfile
import impl.utils as utils
from subprocess import Popen, PIPE

default_size = 27
default_experiment_count=1

script_dir=os.path.dirname(os.path.realpath(__file__))
if os.name == 'posix':
	rel_exec_name = '../bin/scalability'
elif os.name == 'nt':
	rel_exec_name = '..\\bin\\scalability.exe'

exec_name = os.path.normpath(os.path.join(script_dir, rel_exec_name))
result_prefix = 'scalability'

params = argparse.ArgumentParser(description=f'Measures scalability of chosen implementations of parallel addition implemented in the {exec_name} program. The measurements are performed a number of times specified by the value of -N and are averaged by the script. The results are written to an output CSV file and can be later visualized using the visualize_scalability_data.py script.', formatter_class=argparse.RawDescriptionHelpFormatter, epilog=
"""EXAMPLE

python collect_scalability_data.py -N 2 -s 24 -i SMP AVX512 -k -o example.csv

Collect times, derive speedups and efficiency of parallelization of SMP and AVX512 adders (-i SMP AVX512) used to add up 8*(2**24)=128 MiB addends (-s 24).
Perform these actions two times (-N 2) and, keeping the intermediate results (-k), write their average as CSV to the example.csv file (-o example.csv).""")
params.add_argument("-o", "--output", type=argparse.FileType('w', encoding='utf-8'), help="A file which receives the results in the CSV format.");
params.add_argument("-N", "--experiment-count", type=lambda x: int(x,0), default=default_experiment_count, help=f'A number of experiments, i.e. a number of times to run the executable and average the outputs. The default value is {default_experiment_count}.');
params.add_argument("-s", "--size", type=lambda x: int(x,0), default=default_size, help=f'Size of each addend expressed as a base-2 logarithm of 8-byte word count. The default value is {default_size}, which corresponds to {2**default_size} words or {2**(default_size + 3 - 30)} GiB of data.');
params.add_argument("-i", "--implementation", default=['SMP'], choices=['SMP', 'AVX512', 'AVX512-S', 'AVX512-W'], nargs='+', help="A space-separated list of parallel adders to measure scalability of. Choices are: SMP, the default generic implementation with no vectorization; AVX512 (equivalently, AVX512-S), in which each thread performs full vectorized addition of its subvectors and yields a pair of bits, c and i, as described in the article; and AVX512-W, in which the bits c and i are produced by each pair of ZMM words irrespective of how many threads are used.");
params.add_argument("-k", "--keep-raw-results", action="store_true", help=f'Preserve intermediate results in the file \"{result_prefix}_$DATE_$TIME_$ADDER_$EXP.txt\", where $DATE and $TIME are respectively the date and time of the python script invocation, $ADDER sequentially accepts values of the list specified by -i, and $EXP is a counter value identifying the experiment on a given adder. These are the files which accept data produced by {exec_name}.');

params = params.parse_args()
if params.output:
	intdir = os.path.dirname(params.output.name)
elif params.keep_raw_results:
	intdir = os.getcwd()
else:
	intdir = tempfile.gettempdir()


if not os.path.isfile(exec_name):
	sys.exit(f'Please, build the executable \"{exec_name}\" first.')

def read_table(file):
	table = pd.read_table(file, sep=' *\| *', engine='python')
	table.set_index('Threads', inplace=True)
	return table

def run_experiment(i, impl):
	with Popen([exec_name, f'-s={params.size}', f'-i={impl}'], stdout=PIPE, encoding="utf-8") as proc:
		(out, err) = proc.communicate()
		if proc.wait() != 0:
			sys.exit()
	file_name = os.path.join(intdir, f'{result_prefix}_{utils.timemark}_{impl}_{i+1}.txt')
	with open(file_name, "w") as f:
		f.write(out.rstrip())
	table = read_table(file_name)
	if not params.keep_raw_results:
		os.remove(file_name)
	return table

tables = []
for impl in params.implementation:
	print(f'Scalability of {impl}')
	print(f'Experiment #1 of {params.experiment_count}')
	table = run_experiment(0, impl)
	for i in range(1, params.experiment_count):
		print(f'Experiment #{i + 1} of {params.experiment_count}')
		table += run_experiment(i, impl)
	tables.append(table / params.experiment_count)

table = pd.concat(tables, keys=params.implementation, axis=0)

if params.output:
	params.output.write(table.to_csv(line_terminator='\n'))
else:
	print(table.to_string())
