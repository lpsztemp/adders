import sys
import pandas as pd
import argparse
import impl.visualize as viz

params = argparse.ArgumentParser(description=
"""Visualizes relative performance of two adders evaluated based on measurements collected with collect_performance_data.py or collect_performance_data_hi_res.py scripts or the corresponding programs in the bin directory. The evaluation is (t1 - t2) * 100 / t1, where t1 is time performance of the first adder, and t2 is time performance of the second adder. The times are read from an input CSV file.
The adders to visualize are identified with names, full list of which is presented in README.md.
These names match the column names within the input CSV file. Presence of columns in the file depends on which experiments actually took place with regard to the respective script or program parameters and the platform requirements met.""", formatter_class=argparse.RawDescriptionHelpFormatter, epilog=
"""EXAMPLE

python visualize_relative_performance_data.py -i example.csv -S -o example.png CUDA ADC

Build a plot of relative performance of the ADC adder compared to the CUDA adder based on timings in the example.csv file (-i example.csv), show the plot in a dialog window (-S) and also save it to the example.png file (-o example.png).""")
params.add_argument('adder', metavar='adder', nargs=2, help="Space-separated list of two adder names, i.e. names of columns in the input file, to compare and visualize. The full list of adder names is available in README.md.")
params.add_argument("-i", "--input", type=argparse.FileType('r', encoding='utf-8'), required=True, help="Input CSV file with timings of the two adders.");
params.add_argument("-S", "--show-result", action="store_true", default=False, help="Display the plot of performance in a dialog window.");
params.add_argument("-o", "--output", type=argparse.FileType('wb'), help="Create an image file with the plot.");
params.add_argument("--show-legend", action="store_true", default=False, help="Show legend on the displayed plot.");
params.add_argument("--output-legend", action="store_true", default=False, help="Show legend on the plot written to the output file.");

params = params.parse_args()
cols = params.adder

if len(cols) != 2:
	sys.exit('Exactly two columns must be specified to evaluate relative decrease in time of the second computation with respect to the first one. Exiting.') 

if not params.show_result and not params.output:
	sys.exit('There is nothing to do. Exiting.')

table = pd.read_csv(params.input)

for col in cols:
	if not col in table.columns:
		sys.exit(f'There is no {col} entry in the input file. Exiting.')

layout_columns = pd.DataFrame(data = table['Size'] * 8)
layout_columns['Relative Performance'] = (table[cols[0]] - table[cols[1]]) * 100 / table[cols[0]]

plot = viz.plot(x_label='Size in bytes', visualized_columns=['Relative Performance'])
plot.add_y_axis(values_name='Performance gain, %', table=layout_columns, index_column='Size')

if params.output:
	plot.save(file=params.output, show_legend_left=params.output_legend, show_legend_right = False, show_grid = True)
if params.show_result:
	plot.show(show_legend_left = not params.show_legend, show_legend_right = False, show_grid = True)