import os
import argparse
import sys
import impl.utils as utils
import impl.run as run

default_start = 3
default_end = 30
default_resolution = 50
default_experiment_count=1

script_dir=os.path.dirname(os.path.realpath(__file__))
if os.name == 'posix':
	rel_exec_name = '../bin/performance_hi_res'
elif os.name == 'nt':
	rel_exec_name = '..\\bin\\performance_hi_res.exe'

exec_name = os.path.normpath(os.path.join(script_dir, rel_exec_name))
result_prefix = f'dataperf'

params = argparse.ArgumentParser(description=f'Runs a given number of experiments implemented in the \"{exec_name}\" program measuring performance of adding up random numbers of sizes changed parabolically, i.e. a difference between adjacent sizes grows linearly, to achieve the requested resolution of the results. Then the script computes pointwise average of the results and, if specified, stores the arithmetic means to files as CSV tables which can be later visualized using the visualize_performance_data.py or visualize_relative_performance_data.py scripts.', formatter_class=argparse.RawDescriptionHelpFormatter, epilog=
"""EXAMPLE

python collect_performance_data_hi_res.py -N 2 -s 3 -e 25 -r 50 --smp 18 --cuda-device 0 --gmp -k -c cycles.csv -t times.csv -p

Collect times of all supported CPU adders using a single thread, also using supported parallel adders run on 18 CPU cores (--smp 18) as well as a CUDA GPU based adder run on device 0 (--cuda-device 0) and using the adder provided by The GNU Multiple Precision Arithmetic Library (--gmp) with its mpz_add function.
Times to collect are of adding up numbers of sizes ranging from 8*(2**3) = 64 bytes (-s 3) to about 8*(2**25) = 256 MiB (-e 25) to meet the desired resolution of at least 50 (-r 50) data points.
Run the experiment two times (-N 2) and, keeping the intermediate results (-k), collect performance measured in CPU cycles as well as milliseconds taken by each of the adders and write the average of those to output CSV files named respectively cycles.csv (-c cycles.csv) and times.csv (-t times.csv). Also, print out the intermediate results to the terminal (-p).""")
params.add_argument("-p", "--print-results", action="store_true", help="Print the averaged results to the terminal. To also obtain intermediate results, use the -k parameter.");
params.add_argument("-c", "--cycles-file", type=argparse.FileType('w', encoding='utf-8'), help="A file to receive average CPU cycles taken by each of the adders. The cycles are obtained using the RDTSC instruction of the CPU.");
params.add_argument("-t", "--times-file", type=argparse.FileType('w', encoding='utf-8'), help="A file to receive average time, in milliseconds, taken by each of the adders. In case of the CUDA adder, CUDA events are used to obtain time. Other adders are measured using std::chrono::steady_clock of the C++ library.");
params.add_argument("-N", "--experiment-count", type=lambda x: int(x,0), default=default_experiment_count, help=f'A number of experiments, i.e. a number of times to run the {rel_exec_name} executable, to obtain and average the results of. The default value is {default_experiment_count}.');
params.add_argument("-s", "--start-size", type=lambda x: int(x,0), default=default_start, help=f'A starting size of addends expressed as an integral base-2 logarithm of a number of 8-byte words: for example the value of 20 corresponds to 8*(2**20) bytes, i.e. 8 MiB, of data per one addend. Each experiment measures performance of the adders for sizes ranging from the value of -s to the value of -e (inclusive). The default value is {default_start}.');
params.add_argument("-e", "--end-size", type=lambda x: int(x,0), default=default_end, help=f'An inclusive ending bound of the set of sizes. An actual ending size can be changed to meet the requirements for resolution and 512 bit data alignment. The default value is {default_end}.');
params.add_argument("-r", "--resolution", type=lambda x: int(x,0), default=default_resolution, help=f'A minimal number of measurements to make. The actual number of measurements can be increased by the program in order to achieve 64 byte data alignment. The default value is {default_resolution}.');
params.add_argument("--smp", type=int, default=0, help="A number of CPU threads to run the parallel adders. Negative values turn off the SMP experiments. Zero (the default) or values higher than the number of logical CPU cores are replaced with the number of the cores.");
params.add_argument("--cuda-device", type=int, default=-1, help="Use a CUDA device, identified by the parameter, in addition to other implementations. By default, the CUDA computation is omitted.");
params.add_argument("--gmp", action="store_true", help="In addition to other adders, measure performance of the adder which is based upon the GNU Multiple Precision Arithmetic Library (GMP). By default, the GMP computation is omitted.");
params.add_argument("-k", "--keep-raw-results", action="store_true", help=f'Preserve intermediate results in files named \"{result_prefix}_$DATE_$TIME_$EXP_cycles.csv\" and \"{result_prefix}_$DATE_$TIME_$EXP_ms.csv\", where $DATE and $TIME are respectively the date and time of the python script invocation, and $EXP is a counter value identifying the experiment. These are the files produced by \"{exec_name}\" program. Additionally, if the -p parameter is specified, print the output of the program to the terminal.');
params = params.parse_args()

if not params.print_results and not params.cycles_file and not params.times_file:
	sys.exit('There is nothing to do. Exiting.')

if not os.path.isfile(exec_name):
	sys.exit(f'Please, build the executable \"{exec_name}\" first!')

if params.cycles_file and params.times_file and os.path.samefile(params.cycles_file.name, params.times_file.name):
	sys.exit('--cycles-file (-c) and --times-file (-t) cannot refer to the same file')

invoke_string=f'{exec_name} -s={params.start_size} -e={params.end_size} -r={params.resolution} --smp={params.smp} --cuda-device={params.cuda_device}'
if params.gmp:
	invoke_string += ' --gmp'

run.collect_performance_data(exec_string=invoke_string, intermediate_prefix=result_prefix + '_' + utils.timemark, keep_intermediate=params.keep_raw_results, number_of_experiments=params.experiment_count, print_results=params.print_results, cycles_file=params.cycles_file, times_file=params.times_file)
