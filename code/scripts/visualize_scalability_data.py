import pandas as pd
import argparse
import sys
import impl.visualize as viz

params = argparse.ArgumentParser(description="Visualize a specified measure of scalability of parallel adders based on performance values produced with collect_scalability_data.py.", formatter_class=argparse.RawDescriptionHelpFormatter, epilog=
"""EXAMPLE

python visualize_scalability_data.py -i example.csv -S -m Speedup --no-output-legend -o example.png SMP AVX512

Build a plot of speedups (-m Speedup), as defined in the paper, based on measurements for parallel SMP and AVX512 adders in the example.csv file (-i example.csv), show the plot in a dialog window (-S) and also save it to example.png file (-o example.png) such that the image file will contain no legend (--no-output-legend).""")
params.add_argument('adders', metavar='adders', nargs='*', help="Space-separated list of parallel adders, i.e. names of columns in the input file, to visualize. The full list of adders supported by this script is available in README.md.")
params.add_argument("-i", "--input", type=argparse.FileType('r', encoding='utf-8'), required=True, help="An input CSV file with measurements of scalability, i.e. the output of collect_scalability_data.py");
params.add_argument("-S", "--show-result", action="store_true", default=False, help="Display the plot of scalability in a dialog window.");
params.add_argument("-o", "--output", type=argparse.FileType('wb'), help="Create an image file with the plot.");
params.add_argument("--no-show-legend", action="store_true", default=False, help="Hide legend of the displayed plot.");
params.add_argument("--no-output-legend", action="store_true", default=False, help="Hide legend of the plot written to the output file.");
params.add_argument("-m", "--measure", default="Speedup", help="Specifies a column to visualize in a plot, i.e. Time, Speedup or Efficiency. Speedup is the default.");

params = params.parse_args()
impls = params.adders

table = pd.read_csv(params.input, index_col=0)

if not params.show_result and not params.output:
	sys.exit('There is nothing to do. Exiting.')

if not params.measure in table.columns:
	sys.exit(f'There are no measurements of \'{params.measure}\'. Exiting.')

for impl in impls:
	if not impl in table.index:
		print(f'There is no \'{impl}\' implementation. Skipping.')
		impls.remove(impl)

if len(impls) == 0:
	sys.exit('There is nothing to do. Exiting.')

res=[table.loc[impls[0], ['Threads']].set_index('Threads')]
for impl in  impls:
	loc = table.loc[impl, ['Threads', params.measure]].set_index('Threads')
	loc.rename(columns={params.measure: impl}, inplace=True)
	res.append(loc)
table = pd.concat(res, axis=1)

plot = viz.plot('Threads', impls)
plot.add_y_axis(params.measure, table);

if params.output:
	plot.save(file=params.output, show_legend_left=not params.no_output_legend, show_legend_right = False, show_grid = True)
if params.show_result:
	plot.show(show_legend_left = not params.no_show_legend, show_legend_right = False, show_grid = True)
