import matplotlib.pyplot as plt
import pandas as pd
import os

class _viz_table:
	def __init__(self, name, pd):
		self.name = name
		self.data = pd

class plot:
	styles = ['-', '--','-.',':','-', '--','-.',':','-', '--','-.',':']
	colors1 = ['black', 'black', 'black', 'black', '#888', '#888', '#888', '#888', '#aaa', '#aaa', '#aaa', '#aaa']
	colors2 = ['#ccc', '#ccc', '#ccc', '#ccc', '#ddd', '#ddd', '#ddd', '#ddd', '#eee', '#eee', '#eee', '#eee']
	def __init__(self, x_label, visualized_columns):
		self.cols = visualized_columns
		self.index_label = x_label
		self.tables = []
	def add_y_axis(self, values_name, table, index_column = None):
		if len(self.tables) == 2:
			raise Exception("At most, visualization of two tables is supported")
		if not isinstance(table, pd.DataFrame):
			table = pd.read_csv(table)
		if index_column != None:
			table.set_index(index_column, inplace=True)
		if len(self.tables) == 1 and not self.tables[0].data.index.equals(table.index):
			raise ValueError("Inconsistent tables")
		if len(self.tables) == 0 and len(self.cols) == 0:
			self.cols = list(table.columns)
		else:
			unknown_cols = [s for s in table.columns if not s in self.cols]
			table.drop(columns=unknown_cols, inplace=True)
			unknown_cols = [s for s in self.cols if not s in table.columns]
			if len(unknown_cols) != 0:
				for c in unknown_cols:
					print(f'There is no column \"{c}\" in the input table for \"{values_name}\". Excluding the column from the output.')
					self.cols.remove(c)
				for t in self.tables:
					t.data.drop(columns=unknown_cols, inplace=True)
		self.tables.append(_viz_table(name=values_name, pd=table))
	def is_empty(self):
		return len(self.tables) == 0 or any(len(t.data.columns) == 0 for t in self.tables)
	def _prepare_plot(self, show_legend_left, show_legend_right, show_grid):
		fig, ax1 = plt.subplots()
		ax1 = self.tables[0].data.plot(style=self.styles, color=self.colors1, ax=ax1);
		ax1.grid(show_grid)
		if len(self.tables) == 2:
			ax2 = ax1.twinx()
			ax1.set_zorder(ax2.get_zorder() + 1)
			self.tables[1].data.plot(kind='line', secondary_y=True, ax=ax2, style=self.styles, color=self.colors2).set_ylabel(self.tables[1].name)
			ax1.set_facecolor('none')
			if not show_legend_right:
				ax2.legend([]).set_visible(False)
		ax1.set_xlabel(self.index_label)
		ax1.set_ylabel(self.tables[0].name)
		ax1.legend().set_visible(show_legend_left)
		return fig
	def show(self, show_legend_left, show_legend_right, show_grid):
		figure = self._prepare_plot(show_legend_left, show_legend_right, show_grid)
		plt.show()
		plt.close(figure)
	def save(self, file, show_legend_left, show_legend_right, show_grid):
		figure = self._prepare_plot(show_legend_left, show_legend_right, show_grid)
		figure.savefig(file, format=os.path.splitext(file.name)[-1][1:], bbox_inches='tight')
		plt.close(figure)