import pandas as pd
import os
import sys
import tempfile
from subprocess import Popen, PIPE

class _experiment:
	def __init__(self, exec_string, file_prefix, keep_intermediate, cycles_path, times_path):
		self.exec_string = exec_string
		self.keep_intermediate = keep_intermediate
		self.cycles_path_prefix = os.path.join(cycles_path, file_prefix)
		self.times_path_prefix = os.path.join(times_path, file_prefix)
	def run(self, id_num, print_intermediate):
		rdtsc_file = f'{self.cycles_path_prefix}_{id_num}_cycles.csv'
		times_file = f'{self.times_path_prefix}_{id_num}_ms.csv'
		invocation = self.exec_string + f' -c {rdtsc_file} -t {times_file}'
		process = Popen(invocation, stdout=PIPE, encoding="utf-8", shell=True)
		(output, err) = process.communicate()
		if process.wait() != 0:
			sys.exit()
		if print_intermediate:
			print(output.rstrip())
		rdtsc_table = pd.read_csv(rdtsc_file).set_index('Size')
		times_table = pd.read_csv(times_file).set_index('Size')
		if not self.keep_intermediate:
			os.remove(rdtsc_file)
			os.remove(times_file)
		return {'cycles': rdtsc_table, 'times': times_table}
def collect_performance_data(exec_string, intermediate_prefix, keep_intermediate, number_of_experiments, print_results, cycles_file, times_file):
	if cycles_file:
		cycles_path = os.path.dirname(cycles_file.name)
		times_path = os.path.dirname(times_file.name) if times_file else cycles_path
	elif times_file:
		cycles_path = times_path = os.path.dirname(times_file.name)
	elif keep_intermediate:
		cycles_path = times_path = os.getcwd()
	else:
		cycles_path = times_path = tempfile.gettempdir()
	exp = _experiment(exec_string, intermediate_prefix, keep_intermediate, cycles_path, times_path)
	print_intermediate = keep_intermediate and print_results
	print(f'==Experiment #1 of {number_of_experiments}==')
	tables = exp.run(1, print_intermediate)
	for i in range(2, number_of_experiments + 1):
		print(f'==Experiment #{i} of {number_of_experiments}==')
		other = exp.run(i, print_intermediate)
		tables['cycles'] += other['cycles']
		tables['times'] += other['times']
	tables['cycles'] /= number_of_experiments
	tables['times'] /= number_of_experiments
	if print_results:
		print('RDTSC cycles')
		print(tables['cycles'].to_string())
		print('Time, ms')
		print(tables['times'].to_string())
	if cycles_file:
		cycles_file.write(tables['cycles'].to_csv(line_terminator='\n'))
	if times_file:
		times_file.write(tables['times'].to_csv(line_terminator='\n'))
