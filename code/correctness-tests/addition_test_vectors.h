#include <cstdlib>
#pragma once
constexpr std::size_t test_addition_byte_size = 16384;
extern const unsigned test_addend_1[4096];
extern const unsigned test_addend_2[4096];
extern const unsigned test_expected_sum[4096];
