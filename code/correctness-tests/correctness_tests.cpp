#include "../include/iface.h"
#include "rnd_test_vectors.h"
#include "addition_test_vectors.h"
#include "../include/utility.h"
#include <memory>
#include <algorithm>
#include <iostream>

#include "../include/randomize.h"

std::size_t find_difference(const void* pb1, const void* pb2, std::size_t cb)
{
	return static_cast<std::size_t>(std::mismatch((const unsigned char*) pb1, (const unsigned char*) pb1 + cb, (const unsigned char*) pb2).first - (const unsigned char*) pb1);
}

static bool randomize_correctness_test()
{
	auto cb = std::size(test_rnd_123);
	auto buf = std::make_unique<unsigned char[]>(cb);
	randomize_seed(123, buf.get(), cb);
	auto off = find_difference(buf.get(), test_rnd_123, cb);
	if (off != cb)
	{
		std::cerr << "DRBG mismatch at offset " << off << " (size: " << cb << " bytes)\n";
		return false;
	}
	return true;
}

template <std::invocable<void*, const void*, const void*, std::size_t> AddProc>
static bool addition_known_vector_test(AddProc addition_proc, void* result_buf, std::size_t cb)
{
	verify(cb <= test_addition_byte_size);
	addition_proc(result_buf, test_addend_1, test_addend_2, cb);
	auto off = find_difference(result_buf, test_expected_sum, cb);
	if (off != cb)
	{
		std::cerr << "Addition mismatch with known vector at offset " << off << " (size: " << cb << " bytes)\n";
		return false;
	}
	
	return true;
}

static void negate_vector(void* __restrict dest, const void* __restrict src, std::size_t cb)
{
	auto s = static_cast<const char*>(src);
	auto d = static_cast<char*>(dest);
	for (std::size_t off = 0; off < cb; ++off)
	{
		auto val = s[off];
		if (!val)
			d[off] = val;
		else
		{
			d[off] = -val;
			while (++off < cb)
				d[off] = ~s[off];
		}
	}
}

template <std::invocable<void*, const void*, const void*, std::size_t> AddProc>
static bool addition_randomized_vector_test(AddProc addition_proc, void* __restrict result_buf, void* __restrict v1_buf, void* __restrict v2_buf, std::size_t cb)
{
	randomize(v1_buf, cb);
	negate_vector(v2_buf, v1_buf, cb);
	addition_proc(result_buf, v1_buf, v2_buf, cb);
	auto r = static_cast<unsigned char*>(result_buf);
	auto off = static_cast<std::size_t>(std::find_if_not(&r[0], &r[cb], [](auto ch) {return !ch;}) - &r[0]);
	if (off != cb)
	{
		std::cerr << "Randomized addition mismatch at offset " << off << " (size: " << cb << " bytes)\n";
		return false;
	}
	return true;	
}

static bool addition_correctness_test_for(void (*addition_proc) (void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept)
{
	constexpr std::size_t sizes[] = {0x40u, 0x80u, 0x100u, 0x200u, 0x400u, 0x800u, 0x1000u, 0x2000u, 0x4000u, 0x8000u, 0x10000u, 0x20000u, 0x40000u, 0x80000u, 0x100000u};
	constexpr std::size_t buff_size = std::max(std::ranges::max(sizes), test_addition_byte_size);
	static unsigned char sum[buff_size], v1[buff_size], v2[buff_size];
	if (!addition_known_vector_test(addition_proc, sum, test_addition_byte_size))
		return false;
	for (auto cb:sizes)
		if (!addition_randomized_vector_test(addition_proc, sum, v1, v2, cb))
			return false;
	return true;
}

static bool addition_correctness_all_tests(int cuda_device, bool do_gmp)
{
	if (!addition_correctness_test_for(add_vectors_words))
	{
		std::cerr << "CPP addition test failed.\n";
		return false;
	}
	if (!addition_correctness_test_for(add_vectors_scalar))
	{
		std::cerr << "Scalar test failed.\n";
		return false;
	}
	if (!addition_correctness_test_for(add_vectors_scalar_asm))
	{
		std::cerr << "Scalar ASM test failed.\n";
		return false;
	}
	if (!sse4_2_available())
		std::cerr << "The system does not provide SSE4.2. Skipping.\n";
	else
	{
		if (!addition_correctness_test_for(add_vectors_sse4_2_no_lut))
		{
			std::cerr << "SSE4.2 (w/o LUT) test failed.\n";
			return false;
		}
		if (!addition_correctness_test_for(add_vectors_sse4_2_lut))
		{
			std::cerr << "SSE4.2 (with LUT) test failed.\n";
			return false;
		}
	}
	if (!avx2_available())
		std::cerr << "The system does not provide AVX2. Skipping.\n";
	else
	{
		if (!addition_correctness_test_for(add_vectors_avx2))
		{
			std::cerr << "AVX2 test failed.\n";
			return false;
		}
		if (!addition_correctness_test_for(add_vectors_avx2_asm))
		{
			std::cerr << "AVX2 ASM test failed.\n";
			return false;
		}
	}
	if (!avx512_available())
		std::cerr << "The system does not provide AVX-512 (F, CD, DQ, BW or VL). Skipping.\n";
	else
	{
		if (!addition_correctness_test_for(add_vectors_avx512))
		{
			std::cerr << "AVX512 test failed.\n";
			return false;
		}
		if (!addition_correctness_test_for(add_vectors_avx512_asm))
		{
			std::cerr << "AVX512 ASM test failed.\n";
			return false;
		}
	}
	if (do_gmp) 
	{
		if (!gmp_available())
			std::cerr << "No GMP package found. Skipping.\n";
		else if (!addition_correctness_test_for(add_vectors_gmp))
		{
			std::cerr << "GNU MP test failed.\n";
			return false;
		}
	}
	if (cuda_device >= 0)
	{
		if (!set_cuda_device(cuda_device))
			std::cerr << "CUDA device #" << cuda_device << " is not available.\n";
		else if (!addition_correctness_test_for(add_vectors_cuda))
		{
			std::cerr << "CUDA test failed.\n";
			return false;
		}
	}
	if (!addition_correctness_test_for(add_vectors_smp))
	{
		std::cerr << "SMP test failed.\n";
		return false;
	}
	if (avx512_available())
	{
		if (!addition_correctness_test_for(add_vectors_smp_avx512_word))
		{
			std::cerr << "AVX512-SMP-W test failed.\n";
			return false;
		}
		if (!addition_correctness_test_for(add_vectors_smp_avx512_subvector))
		{
			std::cerr << "AVX512-SMP-SB test failed.\n";
			return false;
		}
	}
	return true;
}

#include <regex>
#include <string_view>
#include <stdexcept>
#include <tuple>

auto parse_arguments(int argc, char** argv)
{
	using namespace std::literals;
	const char help[] =
	"tests [-h] [--cuda-device DEVICE_NO] [--gmp]\n\n"
	"Test correctness of adders supported by the platform and returns a failure exit code, if the tests fail.\n"
	"\n"
	"--help        Display this message.\n"
	"--cuda-device Specifies an index of a CUDA device to run the tests on. The device is specified by an integral 0-based index\n"
	"              passed to cudaSetDevice. If a negative value is set (the default), no CUDA tests are performed.\n"
	"--gmp         Test the GMP implementation.\n"
	"\n"
	"EXAMPLE\n"
	"\n"
	BASENAME " --cuda-device 0 --gmp && echo \"Success\" || echo \"Failure\"\n"
	"\n"
	"Check whether the every adder, including the CUDA adder run on the device #0 and the GMP adder, successfully run and produce\n"
	"expected results, and, if they do, print out \"Success\", otherwise, print \"Failure\".";

	std::regex re_cuda{"\\-\\-cuda[\\-_]device(?:=(?:0x)?(\\d+))?\\b"};
	int cuda_device = -1; bool do_gmp = false;
	bool cuda_defaulted = true;

	for (int i = 1; i < argc; ++i)
	{
		if (argv[i] == "--help"sv || argv[i] == "-h"sv)
		{
			std::cout << help << "\n";
			std::exit(0);
		}else if (argv[i] == "--gmp"sv)
		{
			if (do_gmp)
				throw invalid_arguments();
			do_gmp = true;
		}else
		{
			std::cmatch m;
			if (std::regex_search(argv[i], m, re_cuda))
			{
				if (!cuda_defaulted)
					throw invalid_arguments();
				std::string val = m[1].matched?m[1].str():++i < argc?argv[i]:throw invalid_arguments();
				std::size_t count;
				cuda_device = std::stoi(val, &count, 0);
				if (count != val.size())
					throw invalid_arguments();
				cuda_defaulted = false;
			}else
				throw invalid_argument(argv[i]);
		}
	}
	return std::make_tuple(cuda_device, do_gmp);
}

int main(int argc, char** argv)
{
	try
	{
		auto [cuda_device, do_gmp] = parse_arguments(argc, argv);
		if (!randomize_correctness_test())
			return 1;
		std::cerr << "Passed tests of randomization.\n";
		if (!addition_correctness_all_tests(cuda_device, do_gmp))
			return 2;
		std::cerr << "Passed tests of adders.\n";
	}catch (std::exception &ex)
	{
		std::cerr << ex.what() << "\n";
		return -1;
	}
	return 0;
}


