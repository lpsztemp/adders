.globl add_vectors_scalar_asm
.globl add_vectors_avx2_asm
.globl add_vectors_avx512_asm
.data
.align 32
carry_lut: 	.quad 00, 00, 00, 00
			.quad -1, 00, 00, 00
			.quad 00, -1, 00, 00
			.quad -1, -1, 00, 00
			.quad 00, 00, -1, 00
			.quad -1, 00, -1, 00
			.quad 00, -1, -1, 00
			.quad -1, -1, -1, 00
			.quad 00, 00, 00, -1
			.quad -1, 00, 00, -1
			.quad 00, -1, 00, -1
			.quad -1, -1, 00, -1
			.quad 00, 00, -1, -1
			.quad -1, 00, -1, -1
			.quad 00, -1, -1, -1
			.quad -1, -1, -1, -1
.text
add_vectors_scalar_asm:
#result equ rdi
#a_addr equ rsi
#b_addr equ rdx
#count equ rcx
test %rcx, %rcx
jz exit_scalar
test $0x3F, %rcx
jz start_scalar
call abort
start_scalar:
xor %r8, %r8
xor %r9b, %r9b
loop_start_scalar:
add $-1, %r9b
mov (%rsi, %r8), %rax
adc (%rdx, %r8), %rax
mov %rax, (%rdi, %r8)
mov 8(%rsi, %r8), %rax
adc 8(%rdx, %r8), %rax
mov %rax, 8(%rdi, %r8)
mov 16(%rsi, %r8), %rax
adc 16(%rdx, %r8), %rax
mov %rax, 16(%rdi, %r8)
mov 24(%rsi, %r8), %rax
adc 24(%rdx, %r8), %rax
mov %rax, 24(%rdi, %r8)
mov 32(%rsi, %r8), %rax
adc 32(%rdx, %r8), %rax
mov %rax, 32(%rdi, %r8)
mov 40(%rsi, %r8), %rax
adc 40(%rdx, %r8), %rax
mov %rax, 40(%rdi, %r8)
mov 48(%rsi, %r8), %rax
adc 48(%rdx, %r8), %rax
mov %rax, 48(%rdi, %r8)
mov 56(%rsi, %r8), %rax
adc 56(%rdx, %r8), %rax
mov %rax, 56(%rdi, %r8)
setc %r9b
add $64, %r8
cmp %rcx, %r8
jc loop_start_scalar
exit_scalar:
ret

add_vectors_avx2_asm:
#result equ %rdi
#a_addr equ %rsi
#b_addr equ %rdx
#count equ %rcx
#zero equ %ymm7
#ones equ %ymm8
#carry equ %rax
test %rcx, %rcx
jz exit_avx2
test $0x3F, %rcx
jz start_avx2
call abort
start_avx2:
xor %r8, %r8
xor %rax, %rax
lea carry_lut(%rip), %r10
vpxor %ymm7, %ymm7, %ymm7
vpcmpeqq %ymm8, %ymm8, %ymm8
loop_start_avx2:

vmovdqu (%rsi, %r8), %ymm0
vmovdqu (%rdx, %r8), %ymm1
vpcmpgtq %ymm0, %ymm7, %ymm2
vpcmpgtq %ymm1, %ymm7, %ymm3
vpand %ymm3, %ymm2, %ymm5 #carry_add
vpxor %ymm3, %ymm2, %ymm6 #carry_ne
vpaddq %ymm1, %ymm0, %ymm0
vpcmpgtq %ymm0, %ymm7, %ymm4
vpandn %ymm6, %ymm4, %ymm3
vpor %ymm3, %ymm5, %ymm2
vpcmpeqq %ymm8, %ymm0, %ymm3
vmovmskpd %ymm2, %r9
add %r9, %r9
or %r9, %rax
vmovmskpd %ymm3, %r9
add %r9, %rax
xor %r9, %rax
mov $0x0F, %r9
and %rax, %r9
shl $5, %r9
vmovdqa (%r10, %r9), %ymm2
vpsubq %ymm2, %ymm0, %ymm0
vmovdqu %ymm0, (%rdi, %r8)
shr $4, %rax

vmovdqu 32(%rsi, %r8), %ymm0
vmovdqu 32(%rdx, %r8), %ymm1
vpcmpgtq %ymm0, %ymm7, %ymm2
vpcmpgtq %ymm1, %ymm7, %ymm3
vpand %ymm3, %ymm2, %ymm5 #carry_add
vpxor %ymm3, %ymm2, %ymm6 #carry_ne
vpaddq %ymm1, %ymm0, %ymm0
vpcmpgtq %ymm0, %ymm7, %ymm4
vpandn %ymm6, %ymm4, %ymm3
vpor %ymm3, %ymm5, %ymm2
vpcmpeqq %ymm8, %ymm0, %ymm3
vmovmskpd %ymm2, %r9
add %r9, %r9
or %r9, %rax
vmovmskpd %ymm3, %r9
add %r9, %rax
xor %r9, %rax
mov $0x0F, %r9
and %rax, %r9
shl $5, %r9
vmovdqa (%r10, %r9), %ymm2
vpsubq %ymm2, %ymm0, %ymm0
vmovdqu %ymm0, 32(%rdi, %r8)
shr $4, %rax

add $64, %r8
cmp %rcx, %r8
jc loop_start_avx2
exit_avx2:
ret

#Computes rdi[0...rcx-1] = rsi[0...rcx-1] + rdx[0...rcx-1]
add_vectors_avx512_asm:
test %rcx, %rcx
jz exit_avx512
test $0x3F, %rcx
jz start_avx512
call abort
start_avx512:
xor %rax, %rax
vpternlogq $0xFF, %zmm2, %zmm2, %zmm2
kxord %k1, %k1, %k1
loop_start_avx512:
vmovdqu32 (%rsi, %rax), %zmm0
vmovdqu32 (%rdx, %rax), %zmm1
vpaddd %zmm1, %zmm0, %zmm0
vpcmpud $1, %zmm1, %zmm0, %k0
kaddd %k0, %k0, %k0
kord %k1, %k0, %k0
vpcmpeqd %zmm2, %zmm0, %k1
kaddd %k1, %k0, %k0
kxord %k1, %k0, %k1
vpsubd %zmm2, %zmm0, %zmm0 {%k1}
kshiftrd $16, %k1, %k1
vmovdqu32 %zmm0, (%rdi, %rax)
add $64, %rax
cmp %rcx, %rax
jc loop_start_avx512
exit_avx512:
ret

.data
.align(64)
zmm_sll_permute: .quad 7, 0, 1, 2, 3, 4, 5, 6
.text

.globl zmm_epsilon
zmm_epsilon:
vmovdqu64 (%rdx), %zmm0
vmovdqa64 zmm_sll_permute(%rip), %zmm1
movq $0xFE, %rax
vpermq %zmm0, %zmm1, %zmm1
kmovb %eax, %k1
vpsrlq $63, %zmm1, %zmm1
vpaddq %zmm0, %zmm0, %zmm0
vporq %zmm1, %zmm0, %zmm0 {%k1}
vmovdqu64 (%rsi), %zmm1
vpaddq %zmm1, %zmm0, %zmm0
vpternlogq $0xFF, %zmm2, %zmm2, %zmm2
vpcmpuq $1, %zmm1, %zmm0, %k1
vpcmpeqq %zmm2, %zmm0, %k0
kaddb %k1, %k1, %k1
kaddb %k0, %k1, %k1
kxorb %k0, %k1, %k1
vpsubq %zmm2, %zmm0, %zmm0 {%k1}
vpxorq %zmm1, %zmm0, %zmm0
vmovdqu64 %zmm0, (%rdi)
ret

.globl tp_af_zw_vs
tp_af_zw_vs:
sub %rcx, %r8
jna tp_af_zw_vs_exit
mov $0xFE, %rax
kmovb %eax, %k1 #permute_mask
mov 8(%rsp), %r10 #next_c_ptr
vpternlogq $0xFF, %zmm8, %zmm8, %zmm8
add %rcx, %r9
add %rcx, %r10
shl $6, %rcx
add %rcx, %rdi #e_ptr
add %rcx, %rsi #i_ptr
add %rcx, %rdx #c_ptr
vmovdqa64 zmm_sll_permute(%rip), %zmm10 #permute_ind
knotb %k1, %k2
test %rcx, %rcx
jz tp_af_zw_vs_zeta_zero
vmovq -8(%rdx), %xmm11
vpsrlq $63, %zmm11, %zmm11 #zeta
jmp tp_af_zw_vs_got_zeta
tp_af_zw_vs_zeta_zero:
vpxorq %zmm11, %zmm11, %zmm11 #zeta
tp_af_zw_vs_got_zeta:
mov %r8, %rcx
tp_af_zw_vs_loop_start:
vmovdqa64 (%rdx), %zmm0
vpermq %zmm0, %zmm10, %zmm1
vpsrlq $63, %zmm1, %zmm1
vpaddq %zmm0, %zmm0, %zmm0
vporq %zmm1, %zmm0, %zmm0 {%k1}
vporq %zmm11, %zmm0, %zmm0
vmovdqa64 %zmm1, %zmm11 {%k2}{z}
vmovdqa64 (%rsi), %zmm1
vpaddq %zmm1, %zmm0, %zmm0
vpcmpuq $1, %zmm1, %zmm0, %k0
kmovb %k0, (%r10)
vpcmpeqq %zmm8, %zmm0, %k3
kmovb %k3, (%r9)
vmovdqa64 %zmm0, (%rdi)
add $64, %rdi
add $64, %rsi
add $64, %rdx
inc %r9
inc %r10
loop tp_af_zw_vs_loop_start
tp_af_zw_vs_exit:
ret

.globl tp_af_zw_as
tp_af_zw_as:
sub %rcx, %r8 #end_index - start_index
jna tp_af_zw_as_exit
add %rcx, %rdx #next_e_ptr
shl $6, %rcx
add %rcx, %rdi #e_ptr
add %rcx, %rsi #i_ptr
vpternlogq $0xFF, %zmm1, %zmm1, %zmm1
mov %r8, %rcx
tp_af_zw_as_loop_start:
vmovdqa64 (%rdi), %zmm0
kmovb (%rdx), %k1
vpsubq %zmm1, %zmm0, %zmm0 {%k1}
vpxorq (%rsi), %zmm0, %zmm0
vmovdqa64 %zmm0, (%rdi)
add $64, %rdi
add $64, %rsi
inc %rdx
loop tp_af_zw_as_loop_start
tp_af_zw_as_exit:
ret

.globl tp_av_zw_vs
tp_av_zw_vs:
mov 8(%rsp), %r10 #c_ptr
vpternlogq $0xFF, %zmm2, %zmm2, %zmm2
mov %rcx, %r11
shl $6, %r11
tp_av_zw_vs_loop_start:
cmp %rcx, %r8
jna tp_av_zw_vs_loop_end
vmovdqu64 (%rsi, %r11), %zmm0
vpaddq (%rdx, %r11), %zmm0, %zmm1
vpcmpuq $1, %zmm0, %zmm1, %k0
vpcmpeqq %zmm2, %zmm1, %k1
kmovb %k0, (%r10, %rcx) #c_ptr
kmovb %k1, (%r9, %rcx) #i_ptr
vmovdqu64 %zmm1, (%rdi, %r11)
inc %rcx
add $64, %r11
jmp tp_av_zw_vs_loop_start
tp_av_zw_vs_loop_end:
ret

.globl tp_av_zw_as
tp_av_zw_as:
mov %rdx, %r8
shl $6, %r8
vpternlogq $0xFF, %zmm1, %zmm1, %zmm1
tp_av_zw_as_loop_start:
cmp %rdx, %rcx
jna tp_av_zw_as_exit
vmovdqu64 (%rdi, %r8), %zmm0
kmovb (%rsi, %rdx), %k1
vpsubq %zmm1, %zmm0, %zmm0 {%k1}
vmovdqu64 %zmm0, (%rdi, %r8)
add $64, %r8
inc %rdx
jmp tp_av_zw_as_loop_start
tp_av_zw_as_exit:
ret

.globl tp_av_zsv_vs
tp_av_zsv_vs:
vpternlogq $0xFF, %zmm2, %zmm2, %zmm2
kxorw %k1, %k1, %k1 #zeta
knotb %k1, %k0 #i
shl $6, %rcx
shl $6, %r8
loop_start:
cmp %r8, %rcx
jnb tp_av_zsv_vs_exit
vmovdqu64 (%rsi, %rcx), %zmm0
vpaddq (%rdx, %rcx), %zmm0, %zmm1
vpcmpuq $1, %zmm0, %zmm1, %k2
kaddw %k2, %k2, %k2
korw %k1, %k2, %k1
vpcmpeqq %zmm2, %zmm1, %k3
kaddw %k3, %k1, %k1
kxorw %k3, %k1, %k1
vpsubq %zmm2, %zmm1, %zmm1 {%k1}
vpcmpeqq %zmm2, %zmm1, %k2
kandb %k2, %k0, %k0
vmovdqu64 %zmm1, (%rdi, %rcx)
kshiftrw $8, %k1, %k1
add $64, %rcx
jmp loop_start
tp_av_zsv_vs_exit:
kmovq %k0, %rax
popcnt %rax, %rax
shr $3, %rax
kmovq %k1, %rdx
ret

.globl tp_af_zsv_vs
tp_af_zsv_vs:
vpternlogq $0xFF, %zmm3, %zmm3, %zmm3
mov $0xFE, %rax
kmovb %eax, %k4 #shift_mask
vmovdqa64 zmm_sll_permute(%rip), %zmm5
kxorw %k1, %k1, %k1 #zeta
test %rcx, %rcx
jz tp_af_zsv_vs_exit
xor %r8, %r8
tp_af_zsv_vs_loop_start:
vmovdqu64 (%rdx, %r8), %zmm0
vpermq %zmm0, %zmm5, %zmm1
vpcmpq $2, %zmm3, %zmm1, %k3
kandnb %k3, %k4, %k3
vpsrlq $63, %zmm1, %zmm1
vpaddq %zmm0, %zmm0, %zmm0
vporq %zmm1, %zmm0, %zmm0 {%k4}
vmovdqu64 (%rsi, %r8), %zmm1
vpaddq %zmm1, %zmm0, %zmm0
vpcmpuq $1, %zmm1, %zmm0, %k0
vpcmpeqq %zmm3, %zmm0, %k2
kaddw %k0, %k0, %k0
korw %k0, %k1, %k1
kaddw %k2, %k1, %k1
kxorw %k2, %k1, %k1
vpsubq %zmm3, %zmm0, %zmm0 {%k1}
vpxorq %zmm1, %zmm0, %zmm0
vmovdqu64 %zmm0, (%rdi, %r8)
kshiftrw $8, %k1, %k1
korb %k3, %k1, %k1
add $64, %r8
loop tp_af_zsv_vs_loop_start
tp_af_zsv_vs_exit:
ret

.globl tp_av_zsv_as
tp_av_zsv_as:
vpternlogq $0xFF, %zmm3, %zmm3, %zmm3
shl $6, %rsi
shl $6, %rdx
tp_av_zsv_as_loop_start:
cmp %rsi, %rdx
jbe tp_av_zsv_as_exit
vmovdqu64 (%rdi, %rsi), %zmm0
vpcmpeqq %zmm3, %zmm0, %k0
kortestb %k0, %k0
jnc tp_av_zsv_as_loop_break
vpsubq %zmm3, %zmm0, %zmm0
vmovdqu64 %zmm0, (%rdi, %rsi)
add $64, %rsi
jmp tp_av_zsv_as_loop_start
tp_av_zsv_as_loop_break:
knotb %k0, %k0
kmovb %k0, %eax
blsi %rax, %r8
kmovb %r8d, %k1
vpsubq %zmm3, %zmm0, %zmm0 {%k1}
dec %r8
not %r8
kmovb %r8d, %k2
vmovdqa64 %zmm0, %zmm0 {%k2}{z}
vmovdqu64 %zmm0, (%rdi, %rsi)
tp_av_zsv_as_exit:
ret
