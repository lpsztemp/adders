#include "../include/iface.h"
#include <cstdint>

void add_vectors_words(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	const std::uint64_t *p1 = (const std::uint64_t*) v1, *p2 = (const std::uint64_t*) v2;
	std::uint64_t *pr = (std::uint64_t*) result;
	std::uint64_t cf = 0u;
	if (cb & 0x7)
		std::abort();
	cb >>= 3;
	for (std::size_t i = 0; i < cb; ++i)
	{
		pr[i] = p1[i] + p2[i] + cf;
		if (pr[i] < p1[i])
			cf = 1;
		else if (pr[i] == p1[i] && cf != 0)
			cf = 1;
		else
			cf = 0;
	}
}
