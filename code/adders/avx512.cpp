#include "../include/iface.h"
#include <immintrin.h>
#include "../include/cpuid.h"

bool avx512_available() noexcept
{
	int info[4];
	cpuid(0, 0, info);
	if (info[0] < 7)
		return false;
	cpuid(1, 0, info);
	if ((info[2] & AVX_MASK) != AVX_MASK)
		return false;
	cpuid(7, 0, info);
	return (info[1] & AVX512_MASK) == AVX512_MASK;
}

void add_vectors_avx512(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	const __m512i *p1 = (const __m512i*) v1, *p2 = (const __m512i*) v2;
	__m512i *pr = (__m512i*) result;
	__m512i ones = _mm512_set1_epi32(-1);
	__mmask16 carry = _cvtu32_mask16(0u);

	if (cb & 0x3F)
		std::abort();
	cb >>= 6;
	for (std::size_t i = 0; i < cb; ++i)
	{
		__m512i a = _mm512_loadu_si512(&p1[i]);
		__m512i b = _mm512_loadu_si512(&p2[i]);
		__mmask32 carry_add;
		__mmask32 carry_sat;
		__mmask32 carry_result;
		a = _mm512_add_epi32(a, b);
		carry_add = (__mmask32) _mm512_cmplt_epu32_mask(a, b);
		carry_sat = (__mmask32) _mm512_cmpeq_epi32_mask(a, ones);
		carry_result = _kxor_mask32(carry_sat, _kadd_mask32(carry_sat, _kor_mask32(_kadd_mask32(carry_add, carry_add), (__mmask32) carry)));
		carry = (__mmask16) _kshiftri_mask32(carry_result, 16);
		a = _mm512_mask_sub_epi32(a, (__mmask16) carry_result, a, ones);
		_mm512_storeu_si512(&pr[i], a);
	}
}
