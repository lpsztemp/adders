#include "../include/iface.h"
#include <immintrin.h>
#include "../include/cpuid.h"

bool avx2_available() noexcept
{
	int info[4];
	cpuid(0, 0, info);
	if (info[0] < 7)
		return false;
	cpuid(1, 0, info);
	if ((info[2] & AVX_MASK) != AVX_MASK)
		return false;
	cpuid(7, 0, info);
	return (info[1] & AVX2_MASK) == AVX2_MASK;
}

alignas(32) static const unsigned long long carry_lut[16][4] =
{
	{0x0000000000000000ull, 0x0000000000000000ull, 0x0000000000000000ull, 0x0000000000000000ull},
	{0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0x0000000000000000ull, 0x0000000000000000ull},
	{0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0x0000000000000000ull},
	{0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0x0000000000000000ull},
	{0x0000000000000000ull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull},
	{0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull},
	{0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull},
	{0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull},
	{0x0000000000000000ull, 0x0000000000000000ull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull},
	{0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull},
	{0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull},
	{0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull},
	{0x0000000000000000ull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull},
	{0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull},
	{0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull},
	{0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull}
};

void add_vectors_avx2(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	const __m256i *p1 = (const __m256i*) v1, *p2 = (const __m256i*) v2;
	__m256i *pr = (__m256i*) result;
	__m256i zero = _mm256_setzero_si256();
	__m256i ones = _mm256_set1_epi32(-1);
	//__m256i carry = zero;
	int carry = 0;

	if (cb & 0x3F)
		std::abort();
	cb >>= 6;
	for (std::size_t i = 0; i < cb; ++i)
	{
		__m256i a = _mm256_loadu_si256(&p1[i * 2]);
		__m256i b = _mm256_loadu_si256(&p2[i * 2]);
		__m256i cmp_a = _mm256_cmpgt_epi64(zero, a);
		__m256i cmp_b = _mm256_cmpgt_epi64(zero, b);
		__m256i carry_add = _mm256_and_si256(cmp_a, cmp_b);
		__m256i carry_ne = _mm256_or_si256(cmp_a, cmp_b);
		int lut;
		a = _mm256_add_epi64(a, b);

		carry_ne = _mm256_andnot_si256(_mm256_cmpgt_epi64(zero, a), carry_ne);
		carry_add = _mm256_or_si256(carry_add, carry_ne);
		lut = _mm256_movemask_pd(_mm256_castsi256_pd(_mm256_cmpeq_epi64(a, ones)));
		lut = (lut + (_mm256_movemask_pd(_mm256_castsi256_pd(carry_add)) << 1) + carry) ^ lut;
		carry_add = _mm256_load_si256((const __m256i*) &carry_lut[lut & 0xF]);
		a = _mm256_sub_epi64(a, carry_add);
		_mm256_storeu_si256(&pr[i * 2], a);
		carry = lut >> 4;

		a = _mm256_loadu_si256(&p1[i * 2 + 1]);
		b = _mm256_loadu_si256(&p2[i * 2 + 1]);
		cmp_a = _mm256_cmpgt_epi64(zero, a);
		cmp_b = _mm256_cmpgt_epi64(zero, b);
		carry_add = _mm256_and_si256(cmp_a, cmp_b);
		carry_ne = _mm256_or_si256(cmp_a, cmp_b);
		a = _mm256_add_epi64(a, b);

		carry_ne = _mm256_andnot_si256(_mm256_cmpgt_epi64(zero, a), carry_ne);
		carry_add = _mm256_or_si256(carry_add, carry_ne);
		lut = _mm256_movemask_pd(_mm256_castsi256_pd(_mm256_cmpeq_epi64(a, ones)));
		lut = (lut + (_mm256_movemask_pd(_mm256_castsi256_pd(carry_add)) << 1) + carry) ^ lut;
		carry_add = _mm256_load_si256((const __m256i*) &carry_lut[lut & 0xF]);
		a = _mm256_sub_epi64(a, carry_add);
		_mm256_storeu_si256(&pr[i * 2 + 1], a);
		carry = lut >> 4;

	}
}
