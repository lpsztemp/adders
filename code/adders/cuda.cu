#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "../include/utility.h"

static __global__ void try_device_krnl(int* ff)
{
	*ff = 0xff;
}

bool set_cuda_device(int device) noexcept
{
	int device_count;
	cudaDeviceProp props;
	int test_data_h, *test_data_d;
	if (device < 0 || cudaGetDeviceCount(&device_count) != cudaSuccess || device >= device_count)
		return false;
	if (cudaGetDeviceProperties(&props, device) != cudaSuccess || props.major < 3)
		return false;
	if (cudaSetDevice(device) != cudaSuccess)
		return false;
	if (cudaMalloc((void**) &test_data_d, sizeof(int)) != cudaSuccess)
		return false;
	try_device_krnl<<<1, 1>>>(test_data_d);
	verify(cudaMemcpy(&test_data_h, test_data_d, sizeof(int), cudaMemcpyDeviceToHost) == cudaSuccess);
	verify(cudaFree(test_data_d) == cudaSuccess);
	if (test_data_h != 0xFF)
		return false;
	fprintf(stderr, "Using CUDA device \"%s\" (#%d).\n", props.name, device);
	return true;
}

#define ceil_div(x, y) (((x) + (y) - 1) / (y))

static __device__ void add_flags_final_d(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr, unsigned zeta)
{
	unsigned c = *c_ptr, i = *i_ptr, rl, rh;
	rl = (c + c) | zeta;
	rh = c >> 31;
	rl += i;
	rh ^= (rl < i);
	e_ptr[0] = rl ^ i;
	e_ptr[1] = rh;
}

static __global__ void add_flags_finalz_g(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr, const unsigned* __restrict zeta)
{
	add_flags_final_d(e_ptr, i_ptr, c_ptr, *zeta);
}

static __global__ void add_flags_final_g(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr)
{
	add_flags_final_d(e_ptr, i_ptr, c_ptr, 0);
}

static __device__ void reduce_block_flags(unsigned* i_ptr, unsigned* c_ptr, unsigned* i_bits, unsigned* c_bits, size_t output_cap)
{
	for (unsigned reduction_step = 1u, reduction_step_next = 2u; reduction_step < 32u; reduction_step = reduction_step_next, reduction_step_next += reduction_step_next)
	{
		if ((threadIdx.x & (reduction_step_next - 1)) == 0)
		{
			c_bits[threadIdx.x] |= c_bits[threadIdx.x + reduction_step];
			i_bits[threadIdx.x] |= i_bits[threadIdx.x + reduction_step];
		}
		__syncthreads();
	}
	if (threadIdx.x < 32 && blockIdx.x * 32 + threadIdx.x < output_cap)
	{
		c_ptr[blockIdx.x * 32 + threadIdx.x] = c_bits[threadIdx.x * 32];
		i_ptr[blockIdx.x * 32 + threadIdx.x] = i_bits[threadIdx.x * 32];
	}
}

static __device__ void add_flags_vs_d(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr, size_t word_count, unsigned zeta)
{
	unsigned* next_i_ptr = (unsigned*) i_ptr + word_count;
	unsigned* next_c_ptr = (unsigned*) c_ptr + word_count;

	extern __shared__ unsigned shared_buf[];
	unsigned* local_c_flags = shared_buf;
	unsigned* local_i_flags = local_c_flags + 1024u;
	const size_t global_index = blockIdx.x * blockDim.x + threadIdx.x;
	if (global_index < word_count)
	{
		unsigned s = c_ptr[global_index], i = i_ptr[global_index];
		unsigned bit_index = (threadIdx.x & 0x1Fu);
		if (global_index == word_count - 1)
			e_ptr[global_index + 1] = s >> 31;
		s += s;
		s |= global_index?c_ptr[global_index - 1] >> 31:zeta;
		s += i;
		e_ptr[global_index] = s;
		local_c_flags[threadIdx.x] = (s < i) << bit_index;
		local_i_flags[threadIdx.x] = (s == UINT_MAX) << bit_index;
	}
	__syncthreads();
	reduce_block_flags(next_i_ptr, next_c_ptr, local_i_flags, local_c_flags, ceil_div(word_count, 32));
}

static __global__ void add_flags_vsz_g(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr, size_t word_count,
	const unsigned* __restrict zeta)
{
	add_flags_vs_d(e_ptr, i_ptr, c_ptr, word_count, *zeta);
}

static __global__ void add_flags_vs_g(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr, size_t word_count)
{
	add_flags_vs_d(e_ptr, i_ptr, c_ptr, word_count, 0u);
}

static __global__ void add_flags_as_g(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict next_e_ptr, size_t word_count)
{
	const size_t global_index = blockDim.x * blockIdx.x + threadIdx.x;
	if (global_index <= word_count)
	{
		if (next_e_ptr[global_index >> 5] & (1u << (global_index & 31)))
			++e_ptr[global_index];
		if (global_index < word_count)
			e_ptr[global_index] ^= i_ptr[global_index];
	}
}

static __global__ void add_values_vs_g(unsigned* __restrict result, const unsigned* __restrict v1, const unsigned* __restrict v2, size_t word_count,
	unsigned* __restrict i_ptr, unsigned* __restrict c_ptr)
{
	const size_t global_index = blockDim.x * blockIdx.x + threadIdx.x;
	extern __shared__ unsigned local_c_flags[];
	unsigned* local_i_flags = local_c_flags + 1024u;
	if (global_index < word_count)
	{
		unsigned bit_index = (threadIdx.x & 0x1Fu);
		unsigned x = v1[global_index], r = x + v2[global_index];
		result[global_index] = r;
		local_c_flags[threadIdx.x] = (r < x) << bit_index;
		local_i_flags[threadIdx.x] = (r == UINT_MAX) << bit_index;
	}
	__syncthreads();
	reduce_block_flags(i_ptr, c_ptr, local_i_flags, local_c_flags, ceil_div(word_count, 32));
}

static __global__ void add_values_as_g(unsigned* __restrict result, size_t word_count, const unsigned* __restrict e_ptr, unsigned* __restrict zeta)
{
	size_t global_index = blockDim.x * blockIdx.x + threadIdx.x;
	if (global_index <= word_count)
	{
		unsigned e = (e_ptr[global_index >> 5] >> (global_index & 31)) & 1u;
		if (global_index == word_count)
			*zeta = e;
		else if (e)
			++result[global_index];
	}
}

static __host__ void grid_sum_flags(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr, size_t word_count) noexcept
{
	if (word_count < 2)
		add_flags_final_g<<<1, 1>>>(e_ptr, i_ptr, c_ptr);
	else
	{
		size_t next_off = word_count;
		unsigned* next_e = e_ptr + next_off + 1;
		add_flags_vs_g<<<(unsigned) ceil_div(word_count, 1024u), 1024u, 1u << 13 >>>(e_ptr, i_ptr, c_ptr, word_count);
		grid_sum_flags(next_e, i_ptr + next_off, c_ptr + next_off, ceil_div(word_count, 32u));
		add_flags_as_g<<<(unsigned) ceil_div(word_count + 1, 1024u), 1024u>>>(e_ptr, i_ptr, next_e, word_count);
	}
}

static __host__ void grid_sum_flags_z(unsigned* __restrict e_ptr, const unsigned* __restrict i_ptr, const unsigned* __restrict c_ptr, size_t word_count, /*in,out*/ unsigned* __restrict zeta) noexcept
{
	if (word_count < 2)
		add_flags_finalz_g<<<1, 1>>>(e_ptr, i_ptr, c_ptr, zeta);
	else
	{
		size_t next_off = word_count;
		unsigned* next_e = e_ptr + next_off + 1;
		add_flags_vsz_g<<<(unsigned) ceil_div(word_count, 1024u), 1024u, 1u << 13 >>>(e_ptr, i_ptr, c_ptr, word_count, zeta);
		grid_sum_flags(next_e, i_ptr + next_off, c_ptr + next_off, ceil_div(word_count, 32u));
		add_flags_as_g<<<(unsigned) ceil_div(word_count + 1, 1024u), 1024u>>>(e_ptr, i_ptr, next_e, word_count);
	}
}

static __host__ void grid_sum_z(unsigned* __restrict result, const unsigned* __restrict v1, const unsigned* __restrict v2, size_t word_count, /*in,out*/ unsigned* __restrict zeta,
	unsigned* __restrict e_buf, unsigned* __restrict i_buf, unsigned* __restrict c_buf) noexcept
{
	size_t block_count = ceil_div(word_count, 1024u);
	verify(block_count <= UINT_MAX);
	add_values_vs_g<<<(unsigned) block_count, 1024u, 1u << 13 >>>(result, v1, v2, word_count, i_buf, c_buf);
	grid_sum_flags_z(e_buf, i_buf, c_buf, ceil_div(word_count, 32u), zeta);
	add_values_as_g<<<(unsigned) block_count, 1024u>>>(result, word_count, e_buf, zeta);
}

static __host__ void grid_sum(unsigned* __restrict result, const unsigned* __restrict v1, const unsigned* __restrict v2, size_t word_count, /*out*/ unsigned* __restrict zeta,
	unsigned* __restrict e_buf, unsigned* __restrict i_buf, unsigned* __restrict c_buf) noexcept
{
	size_t block_count = ceil_div(word_count, 1024u);
	verify(block_count <= UINT_MAX);
	add_values_vs_g<<<(unsigned) block_count, 1024u, 1u << 13 >>>(result, v1, v2, word_count, i_buf, c_buf);
	grid_sum_flags(e_buf, i_buf, c_buf, ceil_div(word_count, 32u));
	add_values_as_g<<<(unsigned) block_count, 1024u >>>(result, word_count, e_buf, zeta);
}

#ifdef _MSC_VER
#include <intrin.h>
__host__ static __forceinline size_t bsr(unsigned long long x) noexcept
{
	unsigned long index;
	if (!_BitScanReverse64(&index, x))
		return 64u;
	return index;
}
#elif defined(__GNUC__)
#define bsr(x) (63u - (size_t) __builtin_clzll(x))
#elif !defined(__CUDACC__)
#error "Missing generic implementation of bsr"
#endif

#include "../include/iface.h"

struct cuda_buffers
{
	std::size_t cbTotal;
	void *result;
	const void *v1, *v2;
	std::size_t cbDevice;
	std::size_t cbHostBlobOffset, cbDeviceBlobSize;
	std::size_t flag_buf_cw;
	void *device_buffer;
	cudaEvent_t start_kernel, stop_kernel, start_old, start_total, stop_total;
	float kernel_time, old_time;
};

static __host__ void init_cuda_adder(cuda_buffers* __restrict handle, void* __restrict host_result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	int device;
	cudaDeviceProp props;
	std::size_t free_mem, total_mem;
	char *v1d, *v2d;

	handle->result = host_result;
	handle->v1 = v1;
	handle->v2 = v2;
	handle->cbTotal = cb;
	handle->cbHostBlobOffset = handle->cbDeviceBlobSize = 0;
	if (!cb)
		return;
	verify(!(cb & 3));
	verify(cudaEventCreate(&handle->start_total) == cudaSuccess);
	verify(cudaEventCreate(&handle->stop_total) == cudaSuccess);
	verify(cudaEventCreate(&handle->start_kernel) == cudaSuccess);
	verify(cudaEventCreate(&handle->stop_kernel) == cudaSuccess);
	verify(cudaEventCreate(&handle->start_old) == cudaSuccess);
	verify(cudaEventRecord(handle->start_total) == cudaSuccess);
	verify(cudaGetDevice(&device) == cudaSuccess);
	verify(cudaGetDeviceProperties(&props, device) == cudaSuccess);
	verify(min((unsigned) props.maxThreadsPerBlock, (unsigned) props.sharedMemPerBlock / 24u) == 1024u);
	verify(cudaMemGetInfo(&free_mem, &total_mem) == cudaSuccess);
	auto words_available = (free_mem * 31 / 4 - 1730) / 96;
	size_t word_count = cb >> 2, word_count_per_grid = min(word_count, words_available);
	size_t k_max = ceil_div(bsr(word_count_per_grid + word_count_per_grid - 1) + 4, 5);
	handle->flag_buf_cw = k_max;
	if (word_count_per_grid > 2)
		handle->flag_buf_cw += 1u + (word_count_per_grid * word_count_per_grid - 3u * word_count_per_grid + 1) / (31u * word_count_per_grid);
	size_t buf_size = (handle->flag_buf_cw + word_count_per_grid) * 3;
	buf_size += k_max; //extra carry digit in e_ptr
	buf_size <<= 2;
	handle->cbDevice = word_count_per_grid << 2;
	verify(cudaMalloc(&handle->device_buffer, buf_size) == cudaSuccess);
	v1d = (char*) handle->device_buffer + handle->cbDevice, v2d = v1d + handle->cbDevice;
	verify(cudaMemcpy(v1d, v1, handle->cbDevice, cudaMemcpyHostToDevice) == cudaSuccess);
	verify(cudaMemcpy(v2d, v2, handle->cbDevice, cudaMemcpyHostToDevice) == cudaSuccess);
}

__host__ cuda_buffers* prepare_cuda_adder(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb)
{
	auto handle = new cuda_buffers();
	init_cuda_adder(handle, result, v1, v2, cb);
	return handle;
}

__host__ void delete_cuda_adder(cuda_buffers* handle) noexcept
{
	delete handle;
}

__host__ void run_cuda_adder(cuda_buffers* handle) noexcept
{
	size_t cw = handle->cbTotal >> 2, cwd = handle->cbDevice >> 2;
	unsigned* pz;
	unsigned* result = (unsigned*) handle->device_buffer;
	unsigned* v1 = (unsigned*) ((char*) result + handle->cbDevice);
	unsigned* v2 = (unsigned*) ((char*) v1 + handle->cbDevice);
	unsigned* i_ptr = (unsigned*) ((char*) v2 + handle->cbDevice);
	unsigned* c_ptr = i_ptr + handle->flag_buf_cw;
	unsigned* e_ptr = c_ptr + handle->flag_buf_cw;
	size_t blob_off = 0, blob_size = min(cw, cwd);
	float total_kernel_time = 0.f, kernel_time;

	verify(cudaMalloc((void**) &pz, sizeof(unsigned)) == cudaSuccess);
	verify(cudaEventRecord(handle->start_old) == cudaSuccess);
	verify(cudaEventRecord(handle->start_kernel) == cudaSuccess);
	grid_sum(result, v1, v2, blob_size, pz, e_ptr, i_ptr, c_ptr);
	verify(cudaEventRecord(handle->stop_kernel) == cudaSuccess);
	while (cw > blob_off + blob_size)
	{
		verify(cudaEventSynchronize(handle->stop_kernel) == cudaSuccess);
		verify(cudaEventElapsedTime(&kernel_time, handle->start_kernel, handle->stop_kernel) == cudaSuccess);
		total_kernel_time += kernel_time;
		verify(cudaMemcpyAsync((unsigned*) handle->result + blob_off, result, blob_size * sizeof(unsigned), cudaMemcpyDeviceToHost) == cudaSuccess);
		blob_off += blob_size;
		blob_size = min(cw - blob_off, cwd);
		verify(cudaMemcpyAsync(v1, (unsigned*) handle->v1 + blob_off, blob_size * sizeof(unsigned), cudaMemcpyHostToDevice) == cudaSuccess);
		verify(cudaMemcpyAsync(v2, (unsigned*) handle->v2 + blob_off, blob_size * sizeof(unsigned), cudaMemcpyHostToDevice) == cudaSuccess);
		verify(cudaEventRecord(handle->start_kernel) == cudaSuccess);
		grid_sum_z(result, v1, v2, blob_size, pz, e_ptr, i_ptr, c_ptr);
		verify(cudaEventRecord(handle->stop_kernel) == cudaSuccess);
	}
	verify(cudaEventSynchronize(handle->stop_kernel) == cudaSuccess);
	verify(cudaEventElapsedTime(&kernel_time, handle->start_kernel, handle->stop_kernel) == cudaSuccess);
	handle->kernel_time = total_kernel_time + kernel_time;
	verify(cudaEventElapsedTime(&handle->old_time, handle->start_old, handle->stop_kernel) == cudaSuccess);
	handle->cbHostBlobOffset = blob_off;
	handle->cbDeviceBlobSize = blob_size;
	verify(cudaDeviceSynchronize() == cudaSuccess);
}

__host__ cuda_event_times finalize_cuda_adder(cuda_buffers* vectors) noexcept
{
	cuda_event_times result;
	if (!vectors->cbTotal)
		return {0.f, 0.f, 0.f};
	verify(cudaMemcpy((unsigned*) vectors->result + vectors->cbHostBlobOffset, vectors->device_buffer, vectors->cbDeviceBlobSize * sizeof(unsigned), cudaMemcpyDeviceToHost) == cudaSuccess);
	verify(cudaEventRecord(vectors->stop_total) == cudaSuccess);
	verify(cudaEventSynchronize(vectors->stop_total) == cudaSuccess);
	verify(cudaEventElapsedTime(&result.total_time, vectors->start_total, vectors->stop_total) == cudaSuccess);
	result.kernel_time = vectors->kernel_time;
	result.old_time = vectors->old_time;
	verify(cudaFree(vectors->device_buffer) == cudaSuccess);
	verify(cudaEventDestroy(vectors->start_kernel) == cudaSuccess);
	verify(cudaEventDestroy(vectors->stop_kernel) == cudaSuccess);
	verify(cudaEventDestroy(vectors->start_old) == cudaSuccess);
	verify(cudaEventDestroy(vectors->start_total) == cudaSuccess);
	verify(cudaEventDestroy(vectors->stop_total) == cudaSuccess);
	return result;
}

__host__ void add_vectors_cuda(void* __restrict result, const void* __restrict v1, const void* __restrict v2, size_t cb) noexcept
{
	cuda_buffers params;
	init_cuda_adder(&params, result, v1, v2, cb);
	run_cuda_adder(&params);
	finalize_cuda_adder(&params);
}
