#include "../include/iface.h"
#include <nmmintrin.h>
#include "../include/cpuid.h"

bool sse4_2_available() noexcept
{
	int info[4];
	cpuid(0, 0, info);
	if (!info[0])
		return false;
	cpuid(1, 0, info);
	return (info[2] & SSE4_2_MASK) == SSE4_2_MASK;
}

void add_vectors_sse4_2_no_lut(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	const __m128i *p1 = (const __m128i*) v1, *p2 = (const __m128i*) v2;
	__m128i *pr = (__m128i*) result;
	__m128i zero = _mm_setzero_si128();
	__m128i carry = zero;
	__m128i ones = _mm_set1_epi32(-1);

	if (cb & 0xF)
		std::abort();
	cb >>= 4;
	for (std::size_t i = 0; i < cb; ++i)
	{
		__m128i a = _mm_loadu_si128(&p1[i]);
		__m128i b = _mm_loadu_si128(&p2[i]);
		__m128i cmp_a = _mm_cmpgt_epi64(zero, a);
		__m128i cmp_b = _mm_cmpgt_epi64(zero, b);
		__m128i carry_new = _mm_and_si128(cmp_a, cmp_b);
		__m128i carry_ne = _mm_or_si128(cmp_a, cmp_b);
		a = _mm_add_epi64(a, b);
		carry_ne = _mm_andnot_si128(_mm_cmpgt_epi64(zero, a), carry_ne);
		carry_new = _mm_or_si128(carry_new, carry_ne);
		carry_new = _mm_shuffle_epi32(carry_new, 0B01001110);
		carry = _mm_blend_epi16(carry_new, carry, 0xF);
		carry_ne = _mm_and_si128(_mm_cmpeq_epi64(a, ones), carry);
		a = _mm_sub_epi64(a, carry);
		carry_ne = _mm_shuffle_epi32(carry_ne, 0B01001110);
		carry_new = _mm_or_si128(carry_new, carry_ne);
		carry_ne = _mm_blend_epi16(carry_ne, zero, 0xF);
		carry = _mm_or_si128(carry_new, _mm_srli_si128(_mm_and_si128(_mm_cmpeq_epi64(a, ones), carry_ne), 8));
		a = _mm_sub_epi64(a, carry_ne);
		_mm_storeu_si128(&pr[i], a);
	}
}

alignas(16) static const unsigned long long carry_lut[4][2] = 
{
	{0x0000000000000000ull, 0x0000000000000000ull},
	{0xFFFFFFFFFFFFFFFFull, 0x0000000000000000ull},
	{0x0000000000000000ull, 0xFFFFFFFFFFFFFFFFull},
	{0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull},
};

void add_vectors_sse4_2_lut(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	const __m128i *p1 = (const __m128i*) v1, *p2 = (const __m128i*) v2;
	__m128i *pr = (__m128i*) result;
	__m128i zero = _mm_setzero_si128();
	int carry = 0;
	__m128i ones = _mm_set1_epi32(-1);

	if (cb & 0xF)
		std::abort();
	cb >>= 4;
	for (std::size_t i = 0; i < cb; ++i)
	{
		__m128i a = _mm_loadu_si128(&p1[i]);
		__m128i b = _mm_loadu_si128(&p2[i]);
		__m128i cmp_a = _mm_cmpgt_epi64(zero, a);
		__m128i cmp_b = _mm_cmpgt_epi64(zero, b);
		__m128i carry_new = _mm_and_si128(cmp_a, cmp_b);
		__m128i carry_ne = _mm_or_si128(cmp_a, cmp_b);
		int carry_add_msk, carry_incr_msk;
		a = _mm_add_epi64(a, b);
		carry_ne = _mm_andnot_si128(_mm_cmpgt_epi64(zero, a), carry_ne);
		carry_new = _mm_or_si128(carry_new, carry_ne);
		carry_add_msk = _mm_movemask_pd(_mm_castsi128_pd(carry_new));
		carry_incr_msk = _mm_movemask_pd(_mm_castsi128_pd(_mm_cmpeq_epi64(a, ones)));
		carry = (((carry_add_msk + carry_add_msk) | carry) + carry_incr_msk) ^ carry_incr_msk;
		a = _mm_sub_epi64(a, _mm_load_si128((const __m128i*) &carry_lut[carry & 3]));
		_mm_storeu_si128(&pr[i], a);
		carry >>= 2;
	}
}
