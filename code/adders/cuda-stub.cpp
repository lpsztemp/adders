#include "../include/utility.h"

bool set_cuda_device(int) noexcept
{
	return false;
}

#include "../include/iface.h"

struct cuda_buffers {};

static const char msg[] = "The program is built without CUDA implementation.";

cuda_buffers* prepare_cuda_adder(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb)
{
	std::cerr << msg << '\n';
	std::abort();
	return nullptr;
}

void run_cuda_adder(cuda_buffers* handle) noexcept
{
	std::cerr << msg << '\n';
	std::abort();
}

cuda_event_times finalize_cuda_adder(cuda_buffers* vectors) noexcept
{
	std::cerr << msg << '\n';
	std::abort();
}

void delete_cuda_adder(cuda_buffers* handle) noexcept
{
	std::cerr << msg << '\n';
	std::abort();
}

void add_vectors_cuda(void* __restrict result, const void* __restrict v1, const void* __restrict v2, size_t cb) noexcept
{
	std::cerr << msg << '\n';
	std::abort();
}
