#include "../include/utility.h"
#include "../include/iface.h"
#include <thread>
#include <concepts>
#include <bit>
#include <memory>
#include <vector>

template <std::unsigned_integral T>
constexpr auto floor_log_2(T x) noexcept
{
	return static_cast<unsigned>(std::bit_width(x) - 1);
}

template <std::unsigned_integral T>
constexpr auto ceil_log_2(T x) noexcept
{
	return static_cast<std::make_signed_t<T>>(x) < 0?static_cast<T>(std::numeric_limits<T>::digits):floor_log_2(x + x - 1);
}

template <std::unsigned_integral T, std::unsigned_integral U>
constexpr auto ceil_div(T x, U y) noexcept
{
	return static_cast<std::common_type_t<T, U>>((x + y - 1) / y);
}

template <class T, std::unsigned_integral U> requires std::integral<T> || std::floating_point<T>
constexpr auto pow(T x, U n) noexcept
{
	auto result = T(1);
	if (n > 0)
	{
		auto l2n = floor_log_2(n);
		for (U i = 0; i <= l2n; ++i, x *= x, n >>= 1)
			if (n & 1)
				result *= x;
	}
	return result;
}

#if defined(__GNUC__) && (__GNUC__ < 11)
#include <mutex>
#include <condition_variable>
class barrier
{
	int m_init;
	int m_val;
	bool m_curr_flag = false;
	std::mutex m_mtx;
	std::condition_variable m_cv;
public:
	explicit barrier(unsigned threads) :m_init((int) threads), m_val((int) threads) {}
	void arrive_and_wait()
	{
		std::unique_lock<std::mutex> lck(m_mtx);
		if (!--m_val)
		{
			m_curr_flag = !m_curr_flag;
			m_val = m_init;
			m_cv.notify_all();
		}
		else
		{
			auto l_curr_flag = m_curr_flag;
			do
			{
				m_cv.wait(lck);
			} while (l_curr_flag == m_curr_flag);
		}
	}
};
#else
#include <barrier>
using barrier = std::barrier<>;
#endif //__GNUC__

constexpr std::size_t l2w = 64u;

unsigned g_num_threads = std::thread::hardware_concurrency();

void set_num_threads(unsigned T) noexcept
{
	g_num_threads = T;
}

unsigned get_num_threads() noexcept
{
	return g_num_threads;
}

struct thread_proc_shared
{
	barrier bar;
	unsigned thread_count;
};

static inline void tp_add_flags_vector_sum(std::uint64_t* __restrict e_ptr, const std::uint64_t* __restrict i_ptr, const std::uint64_t* __restrict c_ptr,
	std::size_t start_index, std::size_t end_index, std::uint64_t* __restrict next_i_ptr, std::uint64_t* __restrict next_c_ptr)
{
	if (start_index < end_index)
	{
		std::uint64_t zeta = start_index == 0u?0u:c_ptr[start_index - 1] >> (l2w - 1);
		for (std::size_t elem = start_index; elem < end_index;)
		{
			auto flag_index = elem / l2w;
			std::uint64_t i = 0u, c = 0u;
			auto bits = std::min(end_index - elem, l2w);
			for (auto flag_bit_index = 0u; flag_bit_index < bits; ++flag_bit_index, ++elem)
			{
				e_ptr[elem] = (i_ptr[elem] + c_ptr[elem] + c_ptr[elem] + zeta);
				zeta = c_ptr[elem] >> (l2w - 1);
				i |= (std::uint64_t) (e_ptr[elem] == (std::uint64_t) -1) << flag_bit_index;
				c |= (std::uint64_t) (e_ptr[elem] < i_ptr[elem]) << flag_bit_index;
			}
			next_i_ptr[flag_index] = i;
			next_c_ptr[flag_index] = c;
		}
	}
}

static inline void tp_add_flags_adjust_sum(std::uint64_t* __restrict e_ptr, const std::uint64_t* __restrict i_ptr, const std::uint64_t* __restrict next_e_ptr,
	std::size_t start_index, std::size_t end_index)
{
	for (std::size_t elem = start_index; elem < end_index;)
	{
		auto flag_index = elem / l2w;
		auto e = next_e_ptr[flag_index];
		auto bits = std::min(end_index - elem, l2w);
		for (auto flag_bit_index = 0u; flag_bit_index < bits; ++flag_bit_index, ++elem)
			e_ptr[elem] = (e_ptr[elem] + ((e >> flag_bit_index) & 1u)) ^ i_ptr[elem];
	}
}

//*epsilon_ptr = (*i_ptr + *c_ptr * 2 + zeta) ^ *i_ptr
static void tp_add_flags(std::uint64_t* __restrict e_ptr, const std::uint64_t* __restrict i_ptr, const std::uint64_t* __restrict c_ptr, std::size_t word_count,
	unsigned thread_index, thread_proc_shared* shared)
{
	if (word_count == 1u)
	{
		*e_ptr = (*i_ptr + *c_ptr + *c_ptr) ^ *i_ptr;
		return;
	}
	unsigned t = thread_index;
	unsigned T = shared->thread_count;
	barrier& bar = shared->bar;
	std::size_t flag_words = ceil_div(word_count, l2w);
	std::size_t my_blocks = flag_words / T, start_index = flag_words % T, end_index;
	if (t < start_index)
		start_index = ++my_blocks * t;
	else
		start_index += my_blocks * t;
	end_index = std::min((start_index + my_blocks) * l2w, word_count);
	std::uint64_t *next_e_ptr = e_ptr + word_count, *next_i_ptr = const_cast<std::uint64_t*>(i_ptr) + word_count, *next_c_ptr = const_cast<std::uint64_t*>(c_ptr) + word_count;
	start_index *= l2w;
	tp_add_flags_vector_sum(e_ptr, i_ptr, c_ptr, start_index, end_index, next_i_ptr, next_c_ptr);
	bar.arrive_and_wait();
	tp_add_flags(next_e_ptr, next_i_ptr, next_c_ptr, flag_words, thread_index, shared);
	tp_add_flags_adjust_sum(e_ptr, i_ptr, next_e_ptr, start_index, end_index);
}

static inline void tp_add_vals_vector_sum(std::uint64_t* __restrict result, const std::uint64_t* __restrict v1, const std::uint64_t* __restrict v2,
	std::size_t start_index, std::size_t end_index, std::uint64_t* __restrict i_ptr, std::uint64_t* __restrict c_ptr)
{
	for (std::size_t elem = start_index; elem < end_index;)
	{
		std::uint64_t i = 0u, c = 0u;
		std::size_t flag_index = elem / l2w;
		auto bits = std::min(end_index - elem, l2w);
		for (auto flag_bit_index = 0u; flag_bit_index < bits; ++flag_bit_index, ++elem)
		{
			result[elem] = v1[elem] + v2[elem];
			i |= (std::uint64_t) (result[elem] == (std::uint64_t) -1) << flag_bit_index;
			c |= (std::uint64_t) (result[elem] < v1[elem]) << flag_bit_index;
		}
		i_ptr[flag_index] = i;
		c_ptr[flag_index] = c;
	}
}

static inline void tp_add_vals_adjust_sum(std::uint64_t* __restrict result, const std::uint64_t* __restrict e_ptr,
	std::size_t start_index, std::size_t end_index)
{
	for (std::size_t elem = start_index; elem < end_index;)
	{
		auto flag_index = elem / l2w;
		auto e = e_ptr[flag_index];
		auto bits = std::min(end_index - elem, l2w);
		for (auto flag_bit_index = 0u; flag_bit_index < bits; ++flag_bit_index, ++elem)
			result[elem] += (e >> flag_bit_index) & 1u;
	}
}

static void tp_add_vals(std::uint64_t* __restrict result, const std::uint64_t* __restrict v1, const std::uint64_t* __restrict v2, std::size_t word_count,
	std::uint64_t * __restrict e_ptr, std::uint64_t * __restrict i_ptr, std::uint64_t* __restrict c_ptr, unsigned thread_index, thread_proc_shared* __restrict shared)
{
	unsigned t = thread_index;
	unsigned T = shared->thread_count;
	barrier& bar = shared->bar;
	std::size_t flag_words = ceil_div(word_count, l2w);
	std::size_t my_blocks = flag_words / T, start_index = flag_words % T, end_index;
	if (t < start_index)
		start_index = ++my_blocks * t;
	else
		start_index += my_blocks * t;
	end_index = std::min((start_index + my_blocks) * l2w, word_count);
	start_index *= l2w;
	tp_add_vals_vector_sum(result, v1, v2, start_index, end_index, i_ptr, c_ptr);
	bar.arrive_and_wait();
	tp_add_flags(e_ptr, i_ptr, c_ptr, flag_words, thread_index, shared);
	tp_add_vals_adjust_sum(result, e_ptr, start_index, end_index);
}

void add_vectors_smp(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	verify((cb & 7) == 0);
	try
	{
		auto word_count = cb >> 3;
		std::size_t mask_space_size;
		if (word_count < 3)
			mask_space_size = 1;
		else
		{
			auto k_max = ceil_div(ceil_log_2(word_count), floor_log_2(l2w));
			auto l2w_pw_k = pow(l2w, k_max);
			mask_space_size = k_max + ((l2w_pw_k - 1) * (word_count - 1) - l2w_pw_k) / (l2w_pw_k * (l2w - 1)) + 1;
		}
		auto e_buf = std::make_unique<std::uint64_t[]>(mask_space_size);
		auto i_buf = std::make_unique<std::uint64_t[]>(mask_space_size);
		auto c_buf = std::make_unique<std::uint64_t[]>(mask_space_size);
		unsigned T = get_num_threads();
		thread_proc_shared shared = {barrier{T}, T};
		std::vector<std::thread> workers;
		workers.reserve(T);
		for (unsigned t = 1; t < T; ++t)
			workers.emplace_back(tp_add_vals, (std::uint64_t*) result, (std::uint64_t*) v1, (std::uint64_t*) v2, word_count,
				e_buf.get(), i_buf.get(), c_buf.get(), t, &shared);
		tp_add_vals((std::uint64_t*) result, (std::uint64_t*) v1, (std::uint64_t*) v2, word_count,
				e_buf.get(), i_buf.get(), c_buf.get(), 0, &shared);
		for (auto& thread:workers)
			thread.join();
	}catch(std::exception& ex)
	{
		std::cerr << ex.what() << '\n';
		std::abort();
	}
}

#if defined(USE_INTRINSICS) || (!defined(_MSC_VER) && !defined(__GNUC__))
#include <immintrin.h>
#endif

struct zw {alignas(64) char data[64];};

constexpr std::size_t l2zw = 512u;

#if !defined(USE_INTRINSICS) && (defined (_MSC_VER) || defined(__GNUC__))
extern "C" {

void zmm_epsilon(void* __restrict e_ptr, const void* __restrict i_ptr, const void* __restrict c_ptr);
//thread_proc_add_flags_zmm_word_vector_sum
void tp_af_zw_vs(void* __restrict e_ptr, const void* __restrict i_ptr, const void* __restrict c_ptr,
	std::size_t start_index, std::size_t end_index, std::uint8_t* __restrict next_i_ptr, std::uint8_t* __restrict next_c_ptr);
//thread_proc_add_flags_zmm_word_adjust_sum
void tp_af_zw_as(void* __restrict e_ptr, const void* __restrict i_ptr, const std::uint8_t* __restrict next_e_ptr,
	std::size_t start_index, std::size_t end_index);
//thread_proc_add_values_zmm_word_vector_sum
void tp_av_zw_vs(void* __restrict result, const void* __restrict v1, const void* __restrict v2,
	std::size_t start_index, std::size_t end_index, void* __restrict i_ptr, void* __restrict c_ptr);
//thread_proc_add_values_zmm_word_adjust_sum
void tp_av_zw_as(void* __restrict result, const void* __restrict e_ptr,
	std::size_t start_index, std::size_t end_index);
} //extern "C"
#else
static inline void zmm_epsilon(void* __restrict e_ptr, const void* __restrict i_ptr, const void* __restrict c_ptr)
{
	const __m512i ones = _mm512_set1_epi32(-1);
	__m512i c = _mm512_load_si512(c_ptr), i = _mm512_srli_epi64(_mm512_permutexvar_epi64(_mm512_set_epi64(6, 5, 4, 3, 2, 1, 0, 7), c), 63);
	c = _mm512_add_epi64(c, c);
	c = _mm512_mask_or_epi64(c, _cvtu32_mask8(0xFE), c, i);
	i = _mm512_load_si512(i_ptr);
	c = _mm512_add_epi64(c, i);
	__mmask8 ovfl = _mm512_cmplt_epu64_mask(c, i), satur = _mm512_cmpeq_epu64_mask(c, ones);
	ovfl = _kxor_mask8(_kadd_mask8(_kadd_mask8(ovfl, ovfl), satur), satur);
	_mm512_store_si512(e_ptr, _mm512_xor_epi64(_mm512_mask_sub_epi64(c, ovfl, c, ones), i));
}
static inline void tp_af_zw_vs(void* __restrict ge_ptr, const void* __restrict gi_ptr, const void* __restrict gc_ptr,
	std::size_t start_index, std::size_t end_index, std::uint8_t* __restrict next_i_ptr, std::uint8_t* __restrict next_c_ptr)
{
	auto e_ptr = (__m512i*) ge_ptr;
	auto i_ptr = (const __m512i*) gi_ptr;
	auto c_ptr = (const __m512i*) gc_ptr;
	if (start_index < end_index)
	{
		const __m512i ones = _mm512_set1_epi32(-1), zero = _mm512_setzero_si512();
		__mmask8 permute_mask = _cvtu32_mask8(0xFE);
		__m512i permute_ind = _mm512_set_epi64(6ull, 5ull, 4ull, 3ull, 2ull, 1ull, 0ull, 7ull);
		__m512i zeta = start_index == 0u?_mm512_setzero_si512():
			_mm512_zextsi128_si512(_mm_srli_epi64(_mm_cvtsi64_si128(*((std::uint64_t*) &c_ptr[start_index] - 1)), l2w - 1));
		for (std::size_t elem = start_index; elem < end_index; ++elem)
		{
			__m512i c_val = _mm512_load_si512(&c_ptr[elem]);
			__m512i c_shift_helper = _mm512_srli_epi64(_mm512_permutexvar_epi64(permute_ind, c_val), 63);
			c_val = _mm512_add_epi64(c_val, c_val);
			c_val = _mm512_mask_or_epi64(c_val, permute_mask, c_val, c_shift_helper);
			c_val = _mm512_or_si512(c_val, zeta);
			zeta = _mm512_mask_blend_epi64(permute_mask, c_shift_helper, zero);
			__m512i i_val = _mm512_load_si512(&i_ptr[elem]);
			c_val = _mm512_add_epi64(c_val, i_val);
			next_c_ptr[elem] = (std::uint8_t) _cvtmask8_u32(_mm512_cmplt_epu64_mask(c_val, i_val));
			next_i_ptr[elem] = (std::uint8_t) _cvtmask8_u32(_mm512_cmpeq_epu64_mask(c_val, ones));
			_mm512_store_si512(&e_ptr[elem], c_val);
		}
	}
}
static inline void tp_af_zw_as(void* __restrict ge_ptr, const void* __restrict gi_ptr, const std::uint8_t* __restrict next_e_ptr,
	std::size_t start_index, std::size_t end_index)
{
	auto e_ptr = (__m512i*) ge_ptr;
	auto i_ptr = (const __m512i*) gi_ptr;
	const __m512i ones = _mm512_set1_epi32(-1);
	for (std::size_t elem = start_index; elem < end_index; ++elem)
	{
		__m512i e = _mm512_load_si512(&e_ptr[elem]);
		e = _mm512_mask_sub_epi64(e, _cvtu32_mask8(next_e_ptr[elem]), e, ones);
		e = _mm512_xor_si512(e, _mm512_load_si512(&i_ptr[elem]));
		_mm512_store_si512(&e_ptr[elem], e);
	}
}
static inline void tp_av_zw_vs(void* __restrict gresult, const void* __restrict gv1, const void* __restrict gv2,
	std::size_t start_index, std::size_t end_index, void* __restrict gi_ptr, void* __restrict gc_ptr)
{
	auto result = (__m512i*) gresult;
	auto v1 = (const __m512i*) gv1;
	auto v2 = (const __m512i*) gv2;
	auto i_ptr = (__m512i*) gi_ptr;
	auto c_ptr = (__m512i*) gc_ptr;

	const __m512i ones = _mm512_set1_epi32(-1);
	for (std::size_t elem = start_index; elem < end_index;++elem)
	{
		__m512i x = _mm512_loadu_si512(&v1[elem]), y = _mm512_loadu_si512(&v2[elem]), r = _mm512_add_epi64(x, y);
		((std::uint8_t*) c_ptr)[elem] = (std::uint8_t) _cvtmask8_u32(_mm512_cmplt_epu64_mask(r, x));
		((std::uint8_t*) i_ptr)[elem] = (std::uint8_t) _cvtmask8_u32(_mm512_cmpeq_epu64_mask(r, ones));
		_mm512_storeu_si512(&result[elem], r);
	}
}
static inline void tp_av_zw_as(void* __restrict gresult, const void* __restrict ge_ptr,
	std::size_t start_index, std::size_t end_index)
{
	auto result = (__m512i*) gresult;
	auto e_ptr = (const __m512i*) ge_ptr;
	const __m512i ones = _mm512_set1_epi32(-1);
	for (std::size_t elem = start_index; elem < end_index; ++elem)
	{
		__m512i r = _mm512_loadu_si512(&result[elem]);
		r = _mm512_mask_sub_epi64(r, _cvtu32_mask8(((std::uint8_t*) e_ptr)[elem]), r, ones);
		_mm512_storeu_si512(&result[elem], r);
	}
}
#endif

//*epsilon_ptr = (*i_ptr + *c_ptr * 2 + zeta) ^ *i_ptr
static void tp_add_flags_avx512_word(void* __restrict e_ptr, const void* __restrict i_ptr, const void* __restrict c_ptr, std::size_t zmmword_count,
	unsigned thread_index, thread_proc_shared* shared)
{
	if (zmmword_count == 1u)
	{
		zmm_epsilon(e_ptr, i_ptr, c_ptr);
		return;
	}
	unsigned t = thread_index;
	unsigned T = shared->thread_count;
	barrier& bar = shared->bar;
	std::size_t flag_words = ceil_div(zmmword_count, l2zw / 8u);
	std::size_t end_index = flag_words / T, start_index = flag_words % T;
	if (t < start_index)
		start_index = ++end_index * t;
	else
		start_index += end_index * t;
	end_index += start_index;
	end_index *= l2zw / 8u;
	start_index *= l2zw / 8u;
	if (end_index > zmmword_count)
		end_index = zmmword_count;
	std::uint8_t *next_e_ptr = (std::uint8_t*) ((zw*) e_ptr + zmmword_count), *next_i_ptr = (std::uint8_t*) ((zw*) i_ptr + zmmword_count), *next_c_ptr = (std::uint8_t*) ((zw*) c_ptr + zmmword_count);
	tp_af_zw_vs(e_ptr, i_ptr, c_ptr, start_index, end_index, next_i_ptr, next_c_ptr);
	bar.arrive_and_wait();
	tp_add_flags_avx512_word(next_e_ptr, next_i_ptr, next_c_ptr, ceil_div(zmmword_count, l2zw / 8), thread_index, shared);
	tp_af_zw_as(e_ptr, i_ptr, next_e_ptr, start_index, end_index);
}

static void tp_add_vals_avx512_word(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t zmmword_count,
	void* __restrict e_ptr, void* __restrict i_ptr, void* __restrict c_ptr, unsigned thread_index, thread_proc_shared* __restrict shared)
{
	unsigned t = thread_index;
	unsigned T = shared->thread_count;
	barrier& bar = shared->bar;
	std::size_t flag_words = ceil_div(zmmword_count, l2zw / 8);
	std::size_t end_index = flag_words / T, start_index = flag_words % T;
	if (t < start_index)
		start_index = ++end_index * t;
	else
		start_index += end_index * t;
	end_index += start_index;
	end_index *= l2zw / 8;
	start_index *= l2zw / 8;
	if (end_index > zmmword_count)
		end_index = zmmword_count;
	tp_av_zw_vs(result, v1, v2, start_index, end_index, i_ptr, c_ptr);
	bar.arrive_and_wait();
	tp_add_flags_avx512_word(e_ptr, i_ptr, c_ptr, flag_words, thread_index, shared);
	tp_av_zw_as(result, e_ptr, start_index, end_index);
}

void add_vectors_smp_avx512_word(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	verify((cb & 0x3F) == 0);
	try
	{
		auto word_count = cb >> 6;
		std::size_t mask_space_size;
		if (word_count < 3)
			mask_space_size = 1;
		else
		{
			auto k_max = ceil_div(ceil_log_2(word_count), floor_log_2(64u));
			auto l2w512_pw_k = pow(64u, k_max);
			mask_space_size = k_max + ((l2w512_pw_k - 1) * (word_count - 1) - l2w512_pw_k) / (l2w512_pw_k * (64u - 1u)) + 1;
		}
		auto e_buf = std::make_unique<zw[]>(mask_space_size);
		auto i_buf = std::make_unique<zw[]>(mask_space_size);
		auto c_buf = std::make_unique<zw[]>(mask_space_size);
		unsigned T = get_num_threads();
		thread_proc_shared shared = {barrier{T}, T};
		std::vector<std::thread> workers;
		workers.reserve(T);
		for (unsigned t = 1; t < T; ++t)
			workers.emplace_back(tp_add_vals_avx512_word, result, v1, v2, word_count,
				e_buf.get(), i_buf.get(), c_buf.get(), t, &shared);
		tp_add_vals_avx512_word(result, v1, v2, word_count,
				e_buf.get(), i_buf.get(), c_buf.get(), 0, &shared);
		for (auto& thread:workers)
			thread.join();
	}catch(std::exception& ex)
	{
		std::cerr << ex.what() << '\n';
		std::abort();
	}
}

constexpr std::size_t avx512_words_per_thread_min = 4;

#if !defined(USE_INTRINSICS) && (defined (_MSC_VER) || defined(__GNUC__))
extern "C" {
struct i_c_pair {std::uint64_t i, c;};
//thread_proc_add_vals_zmm_sub_vector_vector_sum
i_c_pair tp_av_zsv_vs(void* __restrict result, const void* __restrict v1, const void* __restrict v2,
	std::size_t start_index, std::size_t end_index);
//thread_proc_add_flags_zmm_sub_vector_vector_sum
void tp_af_zsv_vs(void* __restrict e_ptr, const void* i_ptr, const void* c_ptr, std::size_t zmmword_count);
//thread_proc_add_vals_zmm_sub_vector_adjust_sum
void tp_av_zsv_as(void* __restrict result, std::size_t start_index, std::size_t end_index);
} //extern "C"
#else
static inline std::array<std::uint64_t, 2> tp_av_zsv_vs(void* __restrict gresult, const void* __restrict gv1, const void* __restrict gv2,
	std::size_t start_index, std::size_t end_index)
{
	auto result = (__m512i*) gresult;
	auto v1 = (const __m512i*) gv1;
	auto v2 = (const __m512i*) gv2;
	const __m512i ones = _mm512_set1_epi32(-1);
	__mmask32 zeta = _cvtu32_mask32(0u);
	__mmask16 i = _knot_mask16((__mmask16) zeta);
	for (std::size_t elem = start_index; elem < end_index; ++elem)
	{
		__m512i x = _mm512_loadu_si512(&v1[elem]), y = _mm512_loadu_si512(&v2[elem]), r = _mm512_add_epi32(x, y);
		__mmask32 current_c = (__mmask32) _mm512_cmplt_epu32_mask(r, x);
		__mmask16 current_i = _mm512_cmpeq_epi32_mask(r, ones);
		zeta = _kxor_mask32((__mmask32) current_i, _kadd_mask32((__mmask32) current_i, _kor_mask32(_kadd_mask32(current_c, current_c), zeta)));
		r = _mm512_mask_sub_epi32(r, (__mmask16) zeta, r, ones);
		i = _kand_mask16(i, _mm512_cmpeq_epi32_mask(r, ones));
		_mm512_storeu_si512(&result[elem], r);
		zeta = _kshiftri_mask32(zeta, 16);
	}
	return {(std::uint64_t) ((_cvtmask16_u32(i) + 1u) >> 16), (std::uint64_t) _cvtmask32_u32(zeta)};
}

static inline void tp_af_zsv_vs(void* __restrict ge_ptr, const void* gi_ptr, const void* gc_ptr, std::size_t zmmword_count)
{
	auto e_ptr = (__m512i*) ge_ptr;
	auto i_ptr = (const __m512i*) gi_ptr;
	auto c_ptr = (const __m512i*) gc_ptr;
	const __m512i ones = _mm512_set1_epi32(-1);
	__mmask16 shift_or_mask = _cvtu32_mask16(0xFFFEu);
	__m512i permute_ind = _mm512_set_epi32(14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 15);
	__mmask32 zeta = _cvtu32_mask32(0u);
	for (std::size_t i = 0; i < zmmword_count; ++i)
	{
		__m512i x = _mm512_load_si512(&c_ptr[i]), y = _mm512_load_si512(&i_ptr[i]), x_helper = _mm512_permutexvar_epi32(permute_ind, x), r;
		x = _mm512_add_epi32(x, x);
		x = _mm512_mask_or_epi32(x, shift_or_mask, x, _mm512_srli_epi32(x_helper, 31));
		r = _mm512_add_epi32(x, y);
		__mmask32 current_c = (__mmask32) _mm512_cmplt_epu32_mask(r, x);
		__mmask16 current_i = _mm512_cmpeq_epi32_mask(r, ones);
		zeta = _kxor_mask32((__mmask32) current_i, _kadd_mask32((__mmask32) current_i, _kor_mask32(_kadd_mask32(current_c, current_c), zeta)));
		r = _mm512_mask_sub_epi32(r, (__mmask16) zeta, r, ones);
		_mm512_store_si512(&e_ptr[i], _mm512_xor_epi32(r, y));
		zeta = _kor_mask32(_kshiftri_mask32(zeta, 16), (__mmask32) _kandn_mask16(shift_or_mask, _mm512_cmple_epi32_mask(x_helper, ones)));
	}
}

static inline void tp_av_zsv_as(void* gresult, std::size_t start_index, std::size_t end_index)
{
	auto result = (__m512i*) gresult;
	const __m512i ones = _mm512_set1_epi32(-1);
	for (std::size_t elem = start_index; elem < end_index; ++elem)
	{
		__m512i r = _mm512_loadu_si512(&result[elem]);
		__mmask8 cmp_1 = _mm512_cmpeq_epi64_mask(r, ones);
		if (!_kortestc_mask8_u8(cmp_1, cmp_1))
		{
			unsigned cmp_1_gpr = _blsi_u32(_cvtmask8_u32(_knot_mask8(cmp_1)));
			__mmask8 k_one = _cvtu32_mask8(cmp_1_gpr);
			__mmask8 k_zero = _knot_mask8(_cvtu32_mask8(cmp_1_gpr - 1));
			r = _mm512_mask_sub_epi64(r, _cvtu32_mask8(k_one), r, ones);
			_mm512_storeu_si512(&result[elem], _mm512_maskz_mov_epi64(_cvtu32_mask8(k_zero), r));
			break;
		}
		r = _mm512_sub_epi32(r, ones);
		_mm512_storeu_si512(&result[elem], r);
	}
}

#endif


static void tp_add_vals_avx512_subvector(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t zmmword_count,
	std::uint64_t* __restrict e_ptr, std::uint64_t * __restrict i_ptr, std::uint64_t* __restrict c_ptr, unsigned thread_index, thread_proc_shared* __restrict shared)
{
	unsigned t = thread_index;
	unsigned T = shared->thread_count;
	barrier& bar = shared->bar;
	std::size_t blocks_total = ceil_div(zmmword_count, avx512_words_per_thread_min);
	std::size_t end_index = blocks_total / T, start_index = blocks_total % T;
	if (t < start_index)
		start_index = ++end_index * t;
	else
		start_index += end_index * t;
	start_index *= avx512_words_per_thread_min;
	end_index *= avx512_words_per_thread_min;
	end_index += start_index;
	if (end_index > zmmword_count)
		end_index = zmmword_count;
	auto [i, c] = tp_av_zsv_vs(result, v1, v2, start_index, end_index);
	i_ptr[t] = i << (t % l2w);
	c_ptr[t] = c << (t % l2w);
	bar.arrive_and_wait();
	for (std::size_t k = 1, s = 2, k_max = std::min(std::size_t(T), l2w); k < k_max; k = s, s += s)
	{
		if (t + k < T && (t % s) == 0)
		{
			i_ptr[t] |= i_ptr[t + k];
			c_ptr[t] |= c_ptr[t + k];
		}
		bar.arrive_and_wait();
	}
	if (t == 0)
		tp_af_zsv_vs(e_ptr, i_ptr, c_ptr, ceil_div(T, l2zw));
	bar.arrive_and_wait();
	if (e_ptr[t / l2w] & (std::uint64_t(1) << (t % l2w)))
		tp_av_zsv_as(result, start_index, end_index);
}

void add_vectors_smp_avx512_subvector(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	verify((cb & 0x3F) == 0);
	try
	{
		unsigned T = get_num_threads();
		auto word_count = cb >> 6;
		auto e_buf = std::make_unique<zw[]>(T);
		auto i_buf = std::make_unique<zw[]>(T);
		auto c_buf = std::make_unique<zw[]>(T);
		thread_proc_shared shared = {barrier{T}, T};
		std::vector<std::thread> workers;
		workers.reserve(T);
		for (unsigned t = 1; t < T; ++t)
			workers.emplace_back(tp_add_vals_avx512_subvector, result, v1, v2, word_count,
				(std::uint64_t*) e_buf.get(), (std::uint64_t*) i_buf.get(), (std::uint64_t*) c_buf.get(), t, &shared);
		tp_add_vals_avx512_subvector(result, v1, v2, word_count,
				(std::uint64_t*) e_buf.get(), (std::uint64_t*) i_buf.get(), (std::uint64_t*) c_buf.get(), 0, &shared);
		for (auto& thread:workers)
			thread.join();
	}catch(std::exception& ex)
	{
		std::cerr << ex.what() << '\n';
		std::abort();
	}
}
