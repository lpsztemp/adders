extrn abort:proc
public add_vectors_scalar_asm
public add_vectors_avx2_asm
public add_vectors_avx512_asm
data segment align(32)
carry_lut 	dq 00, 00, 00, 00
			dq -1, 00, 00, 00
			dq 00, -1, 00, 00
			dq -1, -1, 00, 00
			dq 00, 00, -1, 00
			dq -1, 00, -1, 00
			dq 00, -1, -1, 00
			dq -1, -1, -1, 00
			dq 00, 00, 00, -1
			dq -1, 00, 00, -1
			dq 00, -1, 00, -1
			dq -1, -1, 00, -1
			dq 00, 00, -1, -1
			dq -1, 00, -1, -1
			dq 00, -1, -1, -1
			dq -1, -1, -1, -1
data ends
.code

add_vectors_scalar_asm proc
result equ rcx
a_addr equ rdx
b_addr equ r8
count equ r9
test count, count
jz exit
test r9, 3Fh
jz start
call abort
start:
xor r10, r10
xor r11b, r11b
loop_start:
add r11b, -1
mov rax, [rdx + r10]
adc rax, [r8 + r10]
mov [rcx + r10], rax
mov rax, 8[rdx + r10]
adc rax, 8[r8 + r10]
mov 8[rcx + r10], rax
mov rax, 16[rdx + r10]
adc rax, 16[r8 + r10]
mov 16[rcx + r10], rax
mov rax, 24[rdx + r10]
adc rax, 24[r8 + r10]
mov 24[rcx + r10], rax
mov rax, 32[rdx + r10]
adc rax, 32[r8 + r10]
mov 32[rcx + r10], rax
mov rax, 40[rdx + r10]
adc rax, 40[r8 + r10]
mov 40[rcx + r10], rax
mov rax, 48[rdx + r10]
adc rax, 48[r8 + r10]
mov 48[rcx + r10], rax
mov rax, 56[rdx + r10]
adc rax, 56[r8 + r10]
mov 56[rcx + r10], rax
setc r11b
add r10, 64
cmp r10, r9
jc loop_start
exit:
ret
add_vectors_scalar_asm endp

add_vectors_avx2_asm proc
result equ rcx
a_addr equ rdx
b_addr equ r8
count equ r9
zero equ ymm7
ones equ ymm8
carry equ rax
push r12
test count, count
jz exit
test r9, 3Fh
jz start
call abort
start:
xor r10, r10
xor rax, rax
lea r12, carry_lut
vpxor zero, zero, zero
vpcmpeqq ones, ones, ones
loop_start:

vmovdqu ymm0, ymmword ptr [a_addr + r10]
vmovdqu ymm1, ymmword ptr [b_addr + r10]
vpcmpgtq ymm2, zero, ymm0
vpcmpgtq ymm3, zero, ymm1
vpand ymm5, ymm2, ymm3 ;carry_add
vpor ymm6, ymm2, ymm3 ;carry_ne
vpaddq ymm0, ymm0, ymm1
vpcmpgtq ymm4, zero, ymm0
vpandn ymm3, ymm4, ymm6
vpor ymm2, ymm5, ymm3
vpcmpeqq ymm3, ymm0, ones
vmovmskpd r11, ymm2
add r11, r11
or rax, r11
vmovmskpd r11, ymm3
add rax, r11
xor rax, r11
mov r11, 0Fh
and r11, rax
shl r11, 5
vmovdqa ymm2, ymmword ptr [r12 + r11]
vpsubq ymm0, ymm0, ymm2
vmovdqu ymmword ptr [result + r10], ymm0
shr rax, 4

vmovdqu ymm0, ymmword ptr 32[a_addr + r10]
vmovdqu ymm1, ymmword ptr 32[b_addr + r10]
vpcmpgtq ymm2, zero, ymm0
vpcmpgtq ymm3, zero, ymm1
vpand ymm5, ymm2, ymm3 ;carry_add
vpxor ymm6, ymm2, ymm3 ;carry_ne
vpaddq ymm0, ymm0, ymm1
vpcmpgtq ymm4, zero, ymm0
vpandn ymm3, ymm4, ymm6
vpor ymm2, ymm5, ymm3
vpcmpeqq ymm3, ymm0, ones
vmovmskpd r11, ymm2
add r11, r11
or rax, r11
vmovmskpd r11, ymm3
add rax, r11
xor rax, r11
mov r11, 0Fh
and r11, rax
shl r11, 5
vmovdqa ymm2, ymmword ptr [r12 + r11]
vpsubq ymm0, ymm0, ymm2
vmovdqu ymmword ptr 32[result + r10], ymm0
shr rax, 4

add r10, 64
cmp r10, r9
jc loop_start
exit:
pop r12
ret
add_vectors_avx2_asm endp

add_vectors_avx512_asm proc
result equ rcx
a_addr equ rdx
b_addr equ r8
count equ r9
ones equ zmm2
carry equ k1
test count, count
jz exit
test r9, 3Fh
jz start
call abort
start:
xor rax, rax
vpternlogq zmm2, zmm2, zmm2, 0FFh ;zmm2 := {-1, -1, -1, -1, -1, -1, -1, -1}, i.e. set all bits of zmm2
kxorw k1, k1, k1 ; k1 := 0
loop_start:
vmovdqu64 zmm0, [a_addr + rax] ;zmm0 := {x0, x1, x2, x3, x4, x5, x6, x7}
vmovdqu64 zmm1, [b_addr + rax] ;zmm1 := {y0, y1, y2, y3, y4, y5, y6, y7}
vpaddq zmm0, zmm0, zmm1 ;vector addition: zmm0 := zmm0 + zmm1
vpcmpuq k0, zmm0, zmm1, 1 ; k0 := nu_{2^64}^8(CMPGT(zmm1, zmm0)) = c
kaddw k0, k0, k0 ; k0 := k0 + k0
korw k0, k0, carry ; k0 := k0 + k0 + zeta
vpcmpeqq k1, zmm0, ones ; k1 := nu_{2^64}^8(CMPEQ(zmm1, zmm0)) = i
kaddw k0, k0, k1 ; k0 := k0 + k1
kxorw k1, k0, k1 ; k1 := k0 xor k1 = epsilon
vpsubq zmm0 {k1}, zmm0, ones ; zmm0 := zmm0 + (gamma_2^8 \circ \nu_2^-8)(epsilon mod 2^8)
kshiftrw k1, k1, 8 ; k1 := floor(k1 / 8) = floor(epsilon / 8)
vmovdqu64 [result + rax], zmm0 ; {z0, z1, z2, z3, z4, z5, z6, z7} := zmm0
add rax, 64
cmp rax, r9
jc loop_start
exit:
ret
add_vectors_avx512_asm endp

data segment align(32)
zmm_sll_permute 	dq 7, 0, 1, 2, 3, 4, 5, 6
data ends

zmm_epsilon proc
vmovdqa64 zmm0, [r8] ;c
vmovdqa64 zmm1, [offset zmm_sll_permute]
mov rax, 0FEh
vpermq zmm1, zmm1, zmm0
kmovb k1, rax
vpsrlq zmm1, zmm1, 63
vpaddq zmm0, zmm0, zmm0
vporq zmm0 {k1}, zmm0, zmm1
vmovdqa64 zmm1, [rdx] ;i
vpaddq zmm0, zmm0, zmm1
vpternlogq zmm2, zmm2, zmm2, 0FFh
vpcmpuq k1, zmm0, zmm1, 1
vpcmpeqq k0, zmm0, zmm2
kaddb k1, k1, k1
kaddb k1, k1, k0
kxorb k1, k1, k0
vpsubq zmm0 {k1}, zmm0, zmm2
vpxorq zmm0, zmm0, zmm1
vmovdqa64 [rcx], zmm0
ret
zmm_epsilon endp

tp_af_zw_vs proc
mov r10, rcx ;e_ptr
mov rcx, 28h[rsp] ;end_index
sub rcx, r9
jna exit
mov rax, 0FEh
kmovb k1, eax ;permute_mask
mov r11, 30h[rsp] ;next_i_ptr
mov rax, 38h[rsp] ;next_c_ptr
vpternlogq zmm8, zmm8, zmm8, 0FFh
add r11, r9
add rax, r9
shl r9, 6
add r10, r9 ;e_ptr
add rdx, r9 ;i_ptr
add r8, r9 ;c_ptr
vmovdqu64 zmm10, [offset zmm_sll_permute] ;permute_ind
knotb k2, k1
test r9, r9
jz zeta_zero
vmovq xmm11, qword ptr [r8 - 8]
vpsrlq zmm11, zmm11, 63 ;zeta
jmp loop_start
zeta_zero:
vpxorq zmm11, zmm11, zmm11 ;zeta
loop_start:
vmovdqa64 zmm0, [r8]
vpermq zmm1, zmm10, zmm0
vpsrlq zmm1, zmm1, 63
vpaddq zmm0, zmm0, zmm0
vporq zmm0 {k1}, zmm0, zmm1
vporq zmm0, zmm0, zmm11
vmovdqa64 zmm11 {k2}{z}, zmm1
vmovdqa64 zmm1, [rdx]
vpaddq zmm0, zmm0, zmm1
vpcmpuq k0, zmm0, zmm1, 1
kmovb byte ptr[rax], k0
vpcmpeqq k3, zmm0, zmm8
kmovb byte ptr[r11], k3
vmovdqa64 [r10], zmm0
add r10, 64
add rdx, 64
add r8, 64
inc r11
inc rax
loop loop_start
exit:
ret
tp_af_zw_vs endp

tp_af_zw_as proc
mov r10, rcx ;e_ptr
mov rcx, 40[rsp]
sub rcx, r9
jna exit
add r8, r9 ;next_e_ptr
shl r9, 6
add r10, r9 ;e_ptr
add rdx, r9 ;i_ptr
vpternlogq zmm1, zmm1, zmm1, 0FFh
loop_start:
vmovdqa64 zmm0, [r10]
kmovb k1, byte ptr [r8]
vpsubq zmm0 {k1}, zmm0, zmm1
vpxorq zmm0, zmm0, [rdx]
vmovdqa64 [r10], zmm0
add rdx, 64
add r10, 64
inc r8
loop loop_start
exit:
ret
tp_af_zw_as endp

tp_av_zw_vs_ proc
mov r10, rcx ;result
mov rcx, 40[rsp] ;end_index
sub rcx, r9
jna exit
mov r11, 48[rsp] ;i_ptr
add r11, r9
mov rax, 56[rsp] ;c_ptr
add rax, r9
shl r9, 6
add r10, r9
add rdx, r9; v1
add r8, r9 ;v2
vpternlogq zmm2, zmm2, zmm2, 0FFh
loop_start:
vmovdqu64 zmm0, [rdx]
vpaddq zmm1, zmm0, [r8]
vpcmpuq k0, zmm1, zmm0, 1
vpcmpeqq k1, zmm1, zmm2
kmovb byte ptr [rax], k0
kmovb byte ptr [r11], k1
vmovdqu64 [r10], zmm1
inc rax
inc r11
add r10, 64
add rdx, 64
add r8, 64
loop loop_start
exit:
ret
tp_av_zw_vs_ endp

tp_av_zw_vs proc
mov r10, 40[rsp] ;end_index
mov r11, 48[rsp] ;i_ptr
mov rax, 56[rsp] ;c_ptr
vpternlogq zmm2, zmm2, zmm2, 0FFh
push r12
mov r12, r9
shl r12, 6
loop_start:
cmp r9, r10
jnb loop_end
vmovdqu64 zmm0, [rdx + r12]
vpaddq zmm1, zmm0, [r8 + r12]
vpcmpuq k0, zmm1, zmm0, 1
vpcmpeqq k1, zmm1, zmm2
kmovb byte ptr [rax + r9], k0
kmovb byte ptr [r11 + r9], k1
vmovdqu64 [rcx + r12], zmm1
inc r9
add r12, 64
jmp loop_start
loop_end:
pop r12
ret
tp_av_zw_vs endp

tp_av_zw_as proc
mov r10, r8
shl r10, 6
vpternlogq zmm1, zmm1, zmm1, 0FFh
loop_start:
cmp r8, r9
jnb exit
vmovdqu64 zmm0, [rcx + r10]
kmovb k1, byte ptr [rdx + r8]
vpsubq zmm0{k1}, zmm0, zmm1
vmovdqu64 [rcx + r10], zmm0
add r10, 64
inc r8
jmp loop_start
exit:
ret
tp_av_zw_as endp

tp_av_zsv_vs proc
vpternlogq zmm2, zmm2, zmm2, 0FFh
kxorw k1, k1, k1 ;zeta
knotb k0, k1 ;i
mov r10, 40[rsp] ;start_index
mov r11, 48[rsp] ;end_index
shl r10, 6
shl r11, 6
loop_start:
cmp r10, r11
jnb exit
vmovdqu64 zmm0, [r8 + r10]
vpaddq zmm1, zmm0, [r9 + r10]
vpcmpuq k2, zmm1, zmm0, 1
kaddw k2, k2, k2
korw k1, k2, k1
vpcmpeqq k3, zmm1, zmm2
kaddw k1, k1, k3
kxorw k1, k1, k3
vpsubq zmm1{k1}, zmm1, zmm2
vpcmpeqq k2, zmm1, zmm2
kandb k0, k0, k2
vmovdqu64 [rdx + r10], zmm1
kshiftrw k1, k1, 8
add r10, 64
jmp loop_start
exit:
kmovq rax, k0
inc rax
shr rax, 8
mov [rcx], rax
kmovq qword ptr 8[rcx], k1
mov rax, rcx
ret
tp_av_zsv_vs endp

tp_af_zsv_vs proc
vpternlogq zmm3, zmm3, zmm3, 0FFh
mov rax, 0FEh
kmovb k4, eax ;shift_mask
vmovdqa64 zmm5, [offset zmm_sll_permute]
kxorw k1, k1, k1 ;zeta
xor r10, r10
shl r9, 6
loop_start:
cmp r10, r9
jae exit
vmovdqu64 zmm0, [r8 + r10]
vpermq zmm1, zmm5, zmm0
vpcmpq k3, zmm1, zmm3, 2
kandnb k3, k4, k3
vpsrlq zmm1, zmm1, 63
vpaddq zmm0, zmm0, zmm0
vporq zmm0 {k4}, zmm0, zmm1
vmovdqu64 zmm1, [rdx + r10]
vpaddq zmm0, zmm0, zmm1
vpcmpuq k0, zmm0, zmm1, 1
vpcmpeqq k2, zmm0, zmm3
kaddw k0, k0, k0
korw k1, k1, k0
kaddw k1, k1, k2
kxorw k1, k1, k2
vpsubq zmm0 {k1}, zmm0, zmm3
vpxorq zmm0, zmm0, zmm1
vmovdqu64 [rcx + r10], zmm0
kshiftrw k1, k1, 8
korb k1, k1, k3
add r10, 64
jmp loop_start
exit:
ret
tp_af_zsv_vs endp

tp_av_zsv_as proc
vpternlogq zmm3, zmm3, zmm3, 0FFh
shl rdx, 6
shl r8, 6
loop_start:
cmp rdx, r8
jae exit
vmovdqu64 zmm0, [rcx + rdx]
vpcmpeqq k0, zmm0, zmm3
kortestb k0, k0
jnc loop_break
vpsubq zmm0, zmm0, zmm3
vmovdqu64 [rcx + rdx], zmm0
add rdx, 64
jmp loop_start
loop_break:
knotb k0, k0
kmovb eax, k0
blsi r8, rax
kmovb k1, r8d
vpsubq zmm0 {k1}, zmm0, zmm3
dec r8
not r8
kmovb k2, r8d
vmovdqa64 zmm0 {k2}{z}, zmm0
vmovdqu64 [rcx + rdx], zmm0
exit:
ret
tp_av_zsv_as endp

end
