#ifdef __GNUC__
#include <x86intrin.h>
//#define _rdtsc __rdtsc
#elif defined(_MSC_VER)
#include <intrin.h>
#endif
#include <cstdlib>
#include "../include/iface.h"

void add_vectors_scalar(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	const unsigned long long *p1 = (const unsigned long long*) v1, *p2 = (const unsigned long long*) v2;
	unsigned long long *pr = (unsigned long long*) result;
	unsigned char cf = 0u;

	if (cb & 0x1F)
		std::abort();
	cb >>= 6;
	for (std::size_t i = 0; i < cb; ++i)
	{
		cf = _addcarry_u64(cf, p1[i * 8 + 0], p2[i * 8 + 0], &pr[i * 8 + 0]);
		cf = _addcarry_u64(cf, p1[i * 8 + 1], p2[i * 8 + 1], &pr[i * 8 + 1]);
		cf = _addcarry_u64(cf, p1[i * 8 + 2], p2[i * 8 + 2], &pr[i * 8 + 2]);
		cf = _addcarry_u64(cf, p1[i * 8 + 3], p2[i * 8 + 3], &pr[i * 8 + 3]);
		cf = _addcarry_u64(cf, p1[i * 8 + 4], p2[i * 8 + 4], &pr[i * 8 + 4]);
		cf = _addcarry_u64(cf, p1[i * 8 + 5], p2[i * 8 + 5], &pr[i * 8 + 5]);
		cf = _addcarry_u64(cf, p1[i * 8 + 6], p2[i * 8 + 6], &pr[i * 8 + 6]);
		cf = _addcarry_u64(cf, p1[i * 8 + 7], p2[i * 8 + 7], &pr[i * 8 + 7]);
	}
}
