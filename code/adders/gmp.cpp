#include <cstdlib>
#include <cstring>
#include "../include/utility.h"
#include "../include/iface.h"

#ifndef GMP_AVAILABLE //GnuMP is not supported under Visual Studio, hence the stubs
bool gmp_available() noexcept
{
	return false;
}

struct gmp_buffers {};

static void gmp_abort()
{
	std::cerr << "Unexpected call: GMP is not supported.\n";
	std::abort();
}

gmp_buffers* prepare_gmp_adder(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	gmp_abort();
	return nullptr;
}

void run_gmp_adder(gmp_buffers* handle) noexcept
{
	gmp_abort();
}

void finalize_gmp_adder(gmp_buffers* handle) noexcept
{
	gmp_abort();
}

void add_vectors_gmp(void* __restrict, const void* __restric, const void* __restrict, std::size_t) noexcept
{
	gmp_abort();
}
#else
#include <gmp.h>
#include <dlfcn.h>
#include <errno.h>

static void* gmp_lib = nullptr;
#define declare_gmp_dl(fn) using dl_##fn##_t = decltype(fn); static dl_##fn##_t* dl_##fn;
#define dlsym_gmp_dl(fn) verify(!!(dl_##fn = reinterpret_cast<dl_##fn##_t*>(dlsym(gmp_lib, "__g" #fn))))

declare_gmp_dl(mpz_init)
declare_gmp_dl(mpz_clear)
declare_gmp_dl(mpz_import)
declare_gmp_dl(mpz_export)
declare_gmp_dl(mpz_add)
declare_gmp_dl(mpz_tdiv_r_2exp)

static void gmp_free() noexcept
{
	if (!!gmp_lib)
	{
		dlclose(gmp_lib);
		gmp_lib = nullptr;
	}
}

static bool gmp_load() noexcept
{
	if (!!gmp_lib)
		return true;
	gmp_lib = dlopen("libgmp.so", RTLD_LAZY | RTLD_LOCAL);
	if (!gmp_lib)
		return false;
	verify(std::atexit(gmp_free) == 0);
	dlsym_gmp_dl(mpz_init);
	dlsym_gmp_dl(mpz_clear);
	dlsym_gmp_dl(mpz_import);
	dlsym_gmp_dl(mpz_export);
	dlsym_gmp_dl(mpz_add);
	dlsym_gmp_dl(mpz_tdiv_r_2exp);
	return true;
}

bool gmp_available() noexcept
{
	return gmp_load();
}

struct gmp_buffers
{
	mpz_t v1g, v2g, rg;
	void* r;
	const void *v1, *v2;
	std::size_t cb;
};

static void init_gmp_adder(gmp_buffers* __restrict handle, void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	verify(!(cb & 7));
	gmp_load();
	dl_mpz_init(handle->v1g);
	dl_mpz_init(handle->v2g);
	dl_mpz_init(handle->rg);
	dl_mpz_import(handle->v1g, cb, -1, 1, 0, 0, v1);
	dl_mpz_import(handle->v2g, cb, -1, 1, 0, 0, v2);
	handle->r = result;
	handle->v1 = v1;
	handle->v2 = v2;
	handle->cb = cb;
}

gmp_buffers* prepare_gmp_adder(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	gmp_buffers* handle = new gmp_buffers;
	init_gmp_adder(handle, result, v1, v2, cb);
	return handle;
}

void run_gmp_adder(gmp_buffers* handle) noexcept
{
	dl_mpz_add(handle->rg, handle->v1g, handle->v2g);
	dl_mpz_tdiv_r_2exp(handle->rg, handle->rg, handle->cb * 8);
}

void finalize_gmp_adder(gmp_buffers* handle) noexcept
{
	std::size_t count;
	dl_mpz_export(handle->r, &count, -1, 1, 0, 0, handle->rg);
	std::memset(static_cast<char*>(handle->r) + count, 0, handle->cb - count);
	dl_mpz_clear(handle->v1g);
	dl_mpz_clear(handle->v2g);
	dl_mpz_clear(handle->rg);
}

void add_vectors_gmp(void* __restrict result, const void* __restrict v1, const void* __restrict v2, std::size_t cb) noexcept
{
	gmp_buffers bufs;
	init_gmp_adder(&bufs, result, v1, v2, cb);
	run_gmp_adder(&bufs);
	finalize_gmp_adder(&bufs);
}
#endif //!_MSC_VER

